import time
import torch
import numpy as np
import torch.nn as nn
from network import *
from loss import *
from pre_process import *
from lr_schedule import *
from data_list import *
import lr_schedule
from torch.utils.data import DataLoader
from tqdm import tqdm
import pickle
import copy
import os
from query import *



class ActiveLearner:
    def __init__(self, config):
        self.config = config
        self.batch_size = 32
        self.transform_source = None
        self.transform_target = None
        self.test_bs = None

        self.source_path = None  # Source data path
        self.target_path = None  # Target data path
        self.active_path = None  # Active data path (labelled target data)
        self.pool_path = None  # Pool data path (target unlabelled data)
        self.source_u_target_path = None  # Source union Target (labelled) data path
        self.test_path = None  # Path of test data

        self.source_data = None  # Source data as a loader
        self.target_data = None  # Target data as a loader
        self.active_data = None  # Active data as a loader
        self.no_active_data = None  # Unlabelled target data
        self.source_u_target_data = None  # Source union Target (labelled) data as a loader
        self.test_data = None    # Test data as a loader
        self.pool_data = None    # Target data (pool for annotation) as a loader

        self.iter_source = None  # Source data iterator
        self.iter_target = None  # Target data iterator
        self.iter_active = None  # Active data iterator
        self.iter_no_active = None  # No active data iterator
        self.iter_source_u_target = None  # Source union Target (labelled) data iterator

        self.model = None
        self.optimizer = None
        self.pre_training = False

        self.log_accuracy = {}

        self.prepare_data()

        self.current_round = 0

    def prepare_data(self):
        prep_dict = {}
        prep_config = self.config["prep"]
        self.transform_source = image_train(**self.config["prep"]["params"])
        self.transform_target = image_train(**self.config["prep"]["params"])

        prep_dict["test"] = image_test_10crop(**self.config["prep"]["params"])

        dsets = {}
        data_config = self.config["data"]

        # Set batch size
        self.train_bs = self.batch_size
        self.test_bs = self.batch_size

        # Set path
        self.source_path = data_config["source"]["list_path"]
        self.target_path = data_config["target"]["list_path"]
        self.active_path = data_config["active"]["list_path"]
        self.pool_path = self.target_path[:-4] + '_pool.txt'
        self.source_u_target_path = os.path.join(self.config["output_path"], 'source_target.txt')
        self.test_path = data_config["test"]["list_path"]

        # Init .txt files
        with open(self.active_path, 'w') as f:
            pass

        with open(self.source_path, 'r') as f:
            lines_source = f.readlines()

        with open(self.target_path, 'r') as f:
            lines_target = f.readlines()

        with open(self.source_u_target_path, 'w') as f:
            for line in lines_source:
                f.write(line)

        with open(self.pool_path, 'w') as f:
            for line in lines_target:
                f.write(line)

        # Load data
        for d in ['source', 'target', 'source_u_target', 'test', 'no_active']:
            self.load_data(mode=d)
        print('Prepare data --> Success')
        print('source', self.source_path)
        print('target', self.target_path)

    def load_data(self, mode):
        if mode == 'source':
            # Source data
            dsets = ImageList(
                open(self.source_path).readlines(), transform=self.transform_source
            )
            self.source_data = DataLoader(
                dsets,
                batch_size=self.train_bs,
                shuffle=True,
                num_workers=4,
                drop_last=True,
            )

        elif mode == 'target':
            # Target data
            dsets = ImageList(
                open(self.target_path).readlines(), transform=self.transform_target
            )

            self.target_data = DataLoader(
                dsets,
                batch_size=self.train_bs,
                shuffle=True,
                num_workers=4,
                drop_last=True
            )

        elif mode == 'active':
            # Active data
            dsets = ImageList(
                open(self.active_path).readlines(), transform=self.transform_target
            )

            self.active_data = DataLoader(
                dsets,
                batch_size=self.train_bs // 2,
                shuffle=True,
                num_workers=4,
                drop_last=True
            )

        elif mode == 'no_active':
            # Active data
            dsets = ImageList(
                open(self.pool_path).readlines(), transform=self.transform_target
            )

            self.no_active_data = DataLoader(
                dsets,
                batch_size=self.train_bs,
                shuffle=True,
                num_workers=4,
                drop_last=True
            )

        elif mode == 'source_u_target':
            # Source data
            dsets = ImageList(
                open(self.source_u_target_path).readlines(), transform=self.transform_source
            )
            self.source_u_target_data = DataLoader(
                dsets,
                batch_size=self.train_bs,
                shuffle=True,
                num_workers=4,
                drop_last=True,
            )

        elif mode == 'test':
            if self.config['prep']['test_10crop']:
                prep_dict = image_test_10crop(**self.config["prep"]["params"])
                dsets = [
                    ImageList(
                        open(self.test_path).readlines(), transform=prep_dict[i]
                    )
                    for i in range(10)
                ]

                self.test_data = [
                    DataLoader(
                        dset, batch_size=self.test_bs, shuffle=False, num_workers=4
                    )
                    for dset in dsets
                ]
            else:
                prep_dict = image_test(**self.config["prep"]["params"])
                dset = ImageList(
                    open(self.test_path).readlines(), transform=prep_dict
                )

                self.test_data = DataLoader(
                    dset, batch_size=self.test_bs, shuffle=False, num_workers=4
                )

        elif mode == 'annotation':
            if self.config['prep']['test_10crop']:
                prep_dict = image_test_10crop(**self.config["prep"]["params"])
                dsets = [
                    ImageList(
                        open(self.pool_path).readlines(), transform=prep_dict[i]
                    )
                    for i in range(10)]
                self.pool_data = [
                    DataLoader(dset, batch_size=self.test_bs, shuffle=False, num_workers=4)
                    for dset in dsets
                    ]
            else:
                prep_dict = image_test(**self.config["prep"]["params"])
                dset = ImageList(
                    open(self.pool_path).readlines(), transform=prep_dict
                )
                self.pool_data = DataLoader(
                    dset, batch_size=self.test_bs, shuffle=False, num_workers=4
                )

    def get_batch(self, mode):
        if mode == 'source':
            try:
                inputs_source, labels_source = self.iter_source.next()
            except:
                self.iter_source = iter(self.source_data)
                inputs_source, labels_source = self.iter_source.next()
            inputs_source, labels_source = inputs_source.cuda(), labels_source.cuda()
            return inputs_source, labels_source

        elif mode == 'target':
            try:
                inputs_target, labels_active = self.iter_target.next()
                labels_target, active = labels_active[:, 0], labels_active[:, 1].float()
            except:
                self.iter_target = iter(self.target_data)
                inputs_target, labels_active = self.iter_target.next()
                labels_target, active = labels_active[:, 0], labels_active[:, 1].float()
            inputs_target, labels_target, active = inputs_target.cuda(), labels_target.cuda(), active.cuda()
            return inputs_target, labels_target, active

        elif mode == 'active':
            try:
                inputs_active, labels_active = self.iter_active.next()
            except:
                self.iter_active = iter(self.active_data)
                inputs_active, labels_active = self.iter_active.next()
            inputs_active, labels_active = inputs_active.cuda(), labels_active.cuda()
            return inputs_active, labels_active

        elif mode == 'no_active':
            try:
                inputs_no_active, labels_no_active = self.iter_no_active.next()
            except:
                self.iter_no_active = iter(self.no_active_data)
                inputs_no_active, labels_no_active = self.iter_no_active.next()
            labels_no_active = labels_no_active[:, 0]
            inputs_no_active, labels_no_active = inputs_no_active.cuda(), labels_no_active.cuda()
            return inputs_no_active, labels_no_active

        elif mode == 'source_u_target':
            try:
                inputs, labels = self.iter_source_u_target.next()
            except:
                self.iter_source_u_target = iter(self.source_u_target_data)
                inputs, labels = self.iter_source_u_target.next()
            inputs, labels = inputs.cuda(), labels.cuda()
            return inputs, labels

        elif mode == 'source_target':
            if self.config['lab_source_target'] == 'source_u_target':
                return self.get_batch(mode='source'), self.get_batch(mode='source_u_target')
            else:
                inputs_source, labels_source = self.get_batch(mode='source')
                if self.current_round > 0:
                    inputs_active, labels_active = self.get_batch(mode='active')
                    inputs_s_t = torch.cat([inputs_source[:inputs_active.size(0)], inputs_active], dim=0)
                    labels_s_t = torch.cat([labels_source[:inputs_active.size(0)], labels_active], dim=0)
                    return (inputs_source, labels_source), (inputs_s_t, labels_s_t)
                else:
                    return (inputs_source, labels_source), (inputs_source, labels_source)

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            net_config = self.config["network"]
            self.model = net_config["name"](**net_config["params"])
            try:
                print(self.config['pretrained_path'] + '/model')
                self.model.load_state_dict(torch.load(self.config['pretrained_path'] + '/model'))
                print('Pretrained model loaded!')
            except:
                self.pre_training = True
                print('Model not loaded')
            self.model = self.model.cuda()
        else:
            raise NotImplementedError('Network unknown')

    def get_optimizer(self):
        params = self.model.get_parameters()

        optimizer_config = self.config["optimizer"]
        self.optimizer = optimizer_config["type"](
            params, **(optimizer_config["optim_params"])
        )
        param_lr = []
        for param_group in self.optimizer.param_groups:
            param_lr.append(param_group["lr"])

    def lr_scheduler(self, step):
        step += 10000
        optimizer_config = self.config["optimizer"]
        schedule_param = optimizer_config["lr_param"]
        lr_scheduler = lr_schedule.schedule_dict[optimizer_config["lr_type"]]
        self.optimizer = lr_scheduler(self.optimizer, step, **schedule_param)

    def setup(self):
        self.get_model()
        self.get_optimizer()

    def run(self):
        self.setup()

        # Computes accuracy of pretrained model
        accuracy = self.test()
        self.log(0, accuracy)

        for r in range(0, self.config['rounds']):
            print('Annotation round', r)
            if r < self.config['rounds']+1:  # Check if the budget is not expired
                self.annotation()
                self.refresh_loader()  # Refresh the loader after annotation
                self.current_round += 1

            for i in tqdm(range(self.config['iter_round'])):
                self.lr_scheduler((r+1)*i)
                self.train_one_iter()
                if self.time_to_test(i):
                    accuracy = self.test()
                    self.log(i, accuracy)

    def time_to_test(self, i):
        return i % self.config["test_interval"] == self.config["test_interval"] - 1

    def train_one_iter(self):
        pass

    @torch.no_grad()
    def predict(self, features):
        return nn.Softmax(dim=1)(self.model.fc(features))

    @torch.no_grad()
    def test(self):
        self.model.eval()
        start_test = True
        test_10crop = self.config['prep']['test_10crop']
        print('test done with test_10crop', test_10crop)

        if test_10crop:
            iter_test = [iter(self.test_data[i]) for i in range(10)]
            len_test = len(self.test_data[0])
        else:
            iter_test = iter(self.test_data)
            len_test = len(iter_test)

        for _ in tqdm(range(len_test)):
            if test_10crop:
                data = [iter_test[j].next() for j in range(10)]
                inputs = [data[j][0] for j in range(10)]
                labels = data[0][1]
                for j in range(10):
                    inputs[j] = inputs[j].cuda()
            else:
                inputs, labels = iter_test.next()
                inputs = inputs.cuda()

            if test_10crop:
                features_ = []
                outputs_ = []
                for j in range(10):
                    features, _ = self.model(inputs[j])
                    outputs = self.predict(features)
                    features_.append(features)
                    outputs_.append(outputs)
                features = sum(features_) / 10.
                outputs = sum(outputs_) / 10.
            else:
                features, _ = self.model(inputs)
                outputs = self.predict(features)

            if start_test:
                all_features = features.float().cpu()
                all_output = outputs.float().cpu()
                all_label = labels.float()
                start_test = False
            else:
                all_features = torch.cat([all_features, features.cpu()], dim=0)
                all_output = torch.cat((all_output, outputs.float().cpu()), 0)
                all_label = torch.cat((all_label, labels.float()), 0)
        _, predict = torch.max(all_output, 1)

        accuracy = torch.sum(torch.squeeze(predict).float() == all_label).item() / float(all_label.size()[0])
        return accuracy

    def annotation(self):
        self.load_data(mode='annotation')  # Prepare the pool of data
        self.model.eval()

        print('requires evaluate_pool()')
        data_eval = self.evaluate_pool()
        data_eval['budget'] = self.config['budget']
        query_idx = self.query(data_eval)[:self.config['budget']]  # Select samples according the query and the budget
        pool_idx = data_eval['index'].numpy()[query_idx]  # Get annotated samples wrt index

        print('Entropy', Entropy(data_eval['outputs']).mean())

        #  Write the selected samples
        with open(self.target_path, 'r') as f:
            lines_select = f.readlines()

        with open(self.target_path, 'w') as f:
            for line in lines_select:
                line_split = line.split()
                index = eval(line_split[-1])
                if index in pool_idx:
                    line_split[-2] = '1'
                new_line = ' '.join(line_split) + '\n'
                f.write(new_line)

        #  Write the active samples
        with open(self.target_path, 'r') as f:
            lines_target = f.readlines()

        n_active = 0
        for line in lines_target:
            line_split = line.split()
            active = eval(line_split[-2])
            n_active += active

        ratio = int(0.9999*(self.train_bs//2) / n_active) + 1
        print('Multiplication ratio', ratio)
        with open(self.active_path, 'w') as f:
            for line in lines_target:
                line_split = line.split()
                active = eval(line_split[-2])
                if active:
                    new_line = ' '.join(line_split[:2]) + '\n'
                    for _ in range(ratio):  # duplicate samples for faster loading
                        f.write(new_line)

        with open(self.source_path, 'r') as f:
            lines_source = f.readlines()

        with open(self.target_path, 'r') as f:
            lines_target = f.readlines()

        target_active = []
        target_non_active = []

        for line in lines_target:
            line_split = line.split()
            active = eval(line_split[-2])
            if active:
                target_active.append(' '.join(line.split()[:2]) + '\n')
            else:
                target_non_active.append(line)

        with open(self.source_u_target_path, 'w') as f:
            for line in lines_source + target_active:
                f.write(line)

        with open(self.pool_path, 'w') as f:
            for line in target_non_active:
                f.write(line)

    def query(self, data_eval):
        pass

    def log(self, i, accuracy):
        print('Accuracy at step', i, 'is', accuracy, 'for round', self.current_round)

        try:
            self.log_accuracy[self.current_round]
        except:
            self.log_accuracy[self.current_round] = []

        try:
            self.log_accuracy[self.current_round].append((i, accuracy))
        except:
            self.log_accuracy[self.current_round] = [(i, accuracy)]

        log_str = "iter: {:05d}, accuracy classifier: {:.5f}".format(i, accuracy)
        log_str += "    requests: {:02d}".format(self.current_round)
        self.config["out_file"].write(log_str + "\n")
        self.config["out_file"].flush()

        path = self.config["output_path"] + '/log_accuracy'
        with open(path+'.txt', 'w') as f:
            f.write(str(self.log_accuracy))

        with open(path, 'wb') as f:
            pickle.dump(self.log_accuracy, f)

    def refresh_loader(self):
        for d in ['target', 'source_u_target', 'active', 'no_active']:
            self.load_data(d)

    def evaluate_pool(self):
        print('start evaluate pool')

        data_eval = {}

        test_10crop = self.config['prep']['test_10crop']

        if test_10crop:
            iter_pool = [iter(self.pool_data[i]) for i in range(10)]
            len_pool = len(iter_pool[0])
        else:
            iter_pool = iter(self.pool_data)
            len_pool = len(iter_pool)

        if self.config['dset'] == 'domain_net':
            len_pool = 100

        print('Evaluate...')
        for i in tqdm(range(len_pool)):
            if test_10crop:
                data = [iter_pool[j].next() for j in range(10)]
                inputs = [data[j][0] for j in range(10)]
                labels_index = data[0][1]
                for j in range(10):
                    inputs[j] = inputs[j].cuda()
            else:
                inputs, labels_index = iter_pool.next()
                inputs = inputs.cuda()

            labels, active, index = labels_index[:, 0], labels_index[:, 1], labels_index[:, 2]
            self.evaluate_batch_pool(data_eval, inputs, labels, index)
        return data_eval

    def evaluate_batch_pool(self, data_eval, inputs, labels, index):
        self.model.eval()
        test_10crop = self.config['prep']['test_10crop']

        if test_10crop:
            features_, outputs_, badge_ = [], [], []
            for j in range(10):
                features, _ = self.model(inputs[j])
                outputs = self.predict(features)
                features_.append(features.detach())
                outputs_.append(outputs.detach())
                if 'badge' in self.config['query']:
                    badge_.append(get_badge(features, self.model.fc).detach())

            features_ = sum(features_) / 10.
            outputs_ = sum(outputs_) / 10.
            if 'badge' in self.config['query']:
                badge_ = sum(badge_) / 10.
        else:
            features_, _ = self.model(inputs)
            outputs_ = self.predict(features_)
            if 'badge' in self.config['query']:
                badge_ = get_badge(features_, self.model.fc)

        with torch.no_grad():
            try:
                data_eval['index'] = torch.cat([data_eval['index'], index], dim=0)
                data_eval['labels'] = torch.cat([data_eval['labels'], labels], dim=0)

                if 'kmeans' in self.config['query']:
                    data_eval['features'] = torch.cat([data_eval['features'], features_], dim=0)

                data_eval['outputs'] = torch.cat([data_eval['outputs'], outputs_], dim=0)

                if 'badge' in self.config['query']:
                    data_eval['badge'] = torch.cat([data_eval['badge'], badge_], dim=0)
            except:
                data_eval['labels'] = labels
                data_eval['index'] = index

                if 'kmeans' in self.config['query']:
                    data_eval['features'] = features_.detach()
                data_eval['outputs'] = outputs_.detach()

                if 'badge' in self.config['query']:
                    data_eval['badge'] = badge_.detach()
            return data_eval


class OracleAL(ActiveLearner):
    def __init__(self, config):
        super(OracleAL, self).__init__(config)

    def train_one_iter(self):
        self.model.train()
        self.optimizer.zero_grad()
        inputs, labels, active = self.get_batch(mode='target')
        _, outputs = self.model(inputs)
        loss = nn.CrossEntropyLoss()(outputs, labels)
        loss.backward()
        self.optimizer.step()


class NoAdaptationAL(ActiveLearner):
    def __init__(self, config):
        super(NoAdaptationAL, self).__init__(config)

    def train_one_iter(self):
        self.model.train()
        self.optimizer.zero_grad()
        (_, _), (inputs_s_t, labels_s_t) = self.get_batch(mode='source_target')
        _, outputs_s_t = self.model(inputs_s_t)
        loss = nn.CrossEntropyLoss()(outputs_s_t, labels_s_t)
        loss.backward()
        self.optimizer.step()


class DomainAdversarialAL(ActiveLearner):
    def __init__(self, config):
        super(DomainAdversarialAL, self).__init__(config)

        self.ad_net = None

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            super(DomainAdversarialAL, self).get_model(mode='base_network')
        elif mode == 'adversarial_network':
            self.ad_net = AdversarialNetwork(
                self.model.output_num(),
                1024,
                max_iter=self.config['ramp_up_trans']
            )

            try:
                self.ad_net.load_state_dict(torch.load(self.config['pretrained_path'] + '/ad_net'))
                self.ad_net.iter_num = 10000
                print('Adversarial net loaded')
            except:
                self.pre_training = True
                print('Adversarial net not loaded')
            self.ad_net = self.ad_net.cuda()
        else:
            raise NotImplementedError('Network unknown')

    def get_optimizer(self):
        params = self.model.get_parameters() + self.ad_net.get_parameters()

        optimizer_config = self.config["optimizer"]
        self.optimizer = optimizer_config["type"](
            params, **(optimizer_config["optim_params"])
        )

        param_lr = []
        for param_group in self.optimizer.param_groups:
            param_lr.append(param_group["lr"])

    def setup(self):
        self.get_model(mode='base_network')
        self.get_model(mode='adversarial_network')
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.ad_net.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')

        if self.current_round > 0:
            inputs_active, labels_active = self.get_batch(mode='active')
        inputs_target, labels_target, active = self.get_batch(mode='target')

        features_source, outputs_source = self.model(inputs_source)
        if self.current_round > 0:
            _, outputs_active = self.model(inputs_active)
        features_target, outputs_target = self.model(inputs_target)

        # Classifier loss
        classifier_source = nn.CrossEntropyLoss()(outputs_source, labels_source)

        if self.current_round > 0:
            classifier_active = nn.CrossEntropyLoss()(outputs_active, labels_active)
            classifier_loss = 0.5 * (classifier_source + classifier_active)
        else:
            classifier_loss = classifier_source
        # Transfer loss
        features = torch.cat([features_source, features_target], dim=0)
        transfer_loss = DANN(features, self.ad_net, hook=True)

        loss = classifier_loss + transfer_loss
        loss.backward()
        self.optimizer.step()

    @torch.no_grad()
    def evaluate_batch_pool(self, data_eval, inputs, labels, index):
        self.model.eval()
        self.ad_net.eval()
        test_10crop = self.config['prep']['test_10crop']

        if test_10crop:
            features_, outputs_, domains_ = [], [], []
            for j in range(10):
                features, _ = self.model(inputs[j])
                outputs = self.predict(features)
                domains = self.ad_net(features, hook=False)
                features_.append(features)
                outputs_.append(outputs)
                domains_.append(domains)
            outputs_ = sum(outputs_) / 10.
            domains_ = sum(domains_) / 10.
        else:
            features_, _ = self.model(inputs)
            outputs_ = self.predict(features_)
            domains_ = self.ad_net(features_, hook=False)

        try:
            data_eval['index'] = torch.cat([data_eval['index'], index], dim=0)
            data_eval['outputs'] = torch.cat([data_eval['outputs'], outputs_], dim=0)
            data_eval['domains'] = torch.cat([data_eval['domains'], domains_], dim=0)
        except:
            data_eval['index'] = index
            data_eval['outputs'] = outputs_
            data_eval['domains'] = domains_
        return data_eval


class DomainAdversarialTransferAL(ActiveLearner):
    def __init__(self, config):
        super(DomainAdversarialTransferAL, self).__init__(config)

        self.ad_net = None

        if self.config['dset'] == 'domain_net_':
            if 'badge' in self.config['query']:
                self.random_layer = nn.Linear(1024*345 + 345, 4096).cuda()
            elif 'sage' in self.config['query']:
                self.random_layer = nn.Linear(1024*345, 4096).cuda()
            else:
                self.random_layer = None
        else:
            self.random_layer = None

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            super(DomainAdversarialTransferAL, self).get_model(mode='base_network')
        elif mode == 'adversarial_network':
            self.ad_net = AdversarialNetwork(
                self.model.output_num(),
                1024,
                output_dim=self.config["network"]["params"]["class_num"],
                max_iter=self.config['ramp_up_trans']
            )
            try:
                self.ad_net.load_state_dict(torch.load(self.config['pretrained_path'] + '/ad_net'))
                self.ad_net.iter_num = 10000
                print('Adversarial net loaded')
            except:
                self.pre_training = True
                print('Adversarial net not loaded')
            self.ad_net = self.ad_net.cuda()
        else:
            raise NotImplementedError('Network unknown')

    def get_optimizer(self):
        params = self.model.get_parameters() + self.ad_net.get_parameters()
        optimizer_config = self.config["optimizer"]

        self.optimizer = optimizer_config["type"](
            params, **(optimizer_config["optim_params"])
        )

    def setup(self):
        self.get_model(mode='base_network')
        self.get_model(mode='adversarial_network')
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.ad_net.train()

        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')

        if self.current_round > 0:
            inputs_active, labels_active = self.get_batch(mode='active')
        inputs_target, labels_target, active = self.get_batch(mode='target')

        features_source, outputs_source = self.model(inputs_source)

        if self.current_round > 0:
            _, outputs_active = self.model(inputs_active)
        features_target, outputs_target = self.model(inputs_target)

        # Classifier loss
        if self.current_round > 0:
            w = weight_class(labels_active,
                             num_class=self.config["network"]["params"]["class_num"])

        classifier_source = nn.CrossEntropyLoss()(outputs_source, labels_source)

        if self.current_round > 0:
            classifier_active = nn.CrossEntropyLoss()(outputs_active, labels_active)
            classifier_loss = 0.5*(classifier_source + classifier_active)
        else:
            classifier_loss = classifier_source

        # Transfer loss
        features = torch.cat([features_source, features_target], dim=0)
        softmax_out_s = torch.zeros_like(outputs_source)
        softmax_out_s[torch.arange(outputs_source.size(0)), labels_source] = 1.0
        labels_out_t = torch.zeros_like(outputs_target)
        labels_out_t[torch.arange(outputs_target.size(0)), labels_target] = 1.0
        softmax_out_t = active.view(-1, 1) * labels_out_t + (1. - active).view(-1, 1) * nn.Softmax(dim=1)(
            outputs_target)

        softmax_out = torch.cat([softmax_out_s, softmax_out_t], dim=0)

        transfer_loss = TSF([features, softmax_out], self.ad_net)

        # Global loss
        loss = classifier_loss + transfer_loss
        loss.backward()
        self.optimizer.step()

    def evaluate_batch_pool(self, data_eval, inputs, labels, index):
        self.model.eval()
        self.ad_net.eval()
        test_10crop = self.config['prep']['test_10crop']

        if test_10crop:

            for j in range(10):
                outputs_, features_, grads_, badge_ = [], [], [], []
                features, _ = self.model(inputs[j])
                outputs = self.predict(features)
                outputs_.append(outputs.detach())
                features_.append(features.detach())
                if 'sage' in self.config['query']:
                    grads_.append(TSF_grad([features, outputs], self.ad_net).detach())

                if 'badge' in self.config['query']:
                    badge_.append(get_badge(features, self.model.fc).detach())

            with torch.no_grad():
                outputs_ = sum(outputs_) / 10.
                features_ = sum(features_) / 10.
                if 'sage' in self.config['query']:
                    grads_ = sum(grads_) / 10.
                if 'badge' in self.config['query']:
                    badge_ = sum(badge_) / 10.

        else:
            features_, _ = self.model(inputs)
            outputs_ = self.predict(features_)

            if 'sage' in self.config['query']:
                grads_ = TSF_grad([features_, outputs_], self.ad_net).detach()
            if 'badge' in self.config['query']:
                badge_ = get_badge(features_, self.model.fc, self.random_layer).detach()

        with torch.no_grad():
            try:
                data_eval['index'] = torch.cat([data_eval['index'], index], dim=0)
                data_eval['labels'] = torch.cat([data_eval['labels'], labels], dim=0)
                data_eval['outputs'] = torch.cat([data_eval['outputs'], outputs_], dim=0)

                if 'kmeans' in self.config['query']:
                    data_eval['features'] = torch.cat([data_eval['features'], features_], dim=0)

                if 'sage' in self.config['query']:
                    if 'not_proj' in self.config['query']:
                        data_eval['sage_not_proj'] = torch.cat([data_eval['sage'],
                                                                get_sage_not_proj(outputs_,
                                                                                  grads_,
                                                                                  self.random_layer
                                                                                  )], dim=0)
                    else:
                        data_eval['sage'] = torch.cat([data_eval['sage'],
                                                       get_sage(outputs_,
                                                                grads_,
                                                                self.random_layer
                                                                )], dim=0)

                if 'badge' in self.config['query']:
                    data_eval['badge'] = torch.cat([data_eval['badge'], badge_], dim=0)

            except:

                data_eval['index'] = index
                data_eval['labels'] = labels
                data_eval['outputs'] = outputs_

                if 'kmeans' in self.config['query']:
                    data_eval['features'] = features_

                if 'sage' in self.config['query']:
                    if 'not_proj' in self.config['query']:
                        data_eval['sage_not_proj'] = get_sage_not_proj(outputs_, grads_, self.random_layer)
                    else:
                        data_eval['sage'] = get_sage(outputs_, grads_, self.random_layer)

                if 'badge' in self.config['query']:
                    data_eval['badge'] = badge_

            return data_eval


class MiniMaxEntropyAL(ActiveLearner):
    def __init__(self, config):
        super(MiniMaxEntropyAL, self).__init__(config)

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            net_config = self.config["network"]
            self.model = net_config["name"](**net_config["params"])
            self.model.fc = nn.Linear(self.model.output_num(),
                                      self.config["network"]["params"]["class_num"],
                                      bias=False)
            try:
                print(self.config['pretrained_path'] + '/model')
                self.model.load_state_dict(torch.load(self.config['pretrained_path'] + '/model'))
                print('Pretrained model loaded!')
            except:
                self.pre_training = True
                print('Model not loaded')
            self.model = self.model.cuda()
        else:
            raise NotImplementedError('Network unknown')

    def train_one_iter(self):
        self.model.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')
        inputs_active, labels_active = self.get_batch(mode='active')
        inputs_target, labels_target, active = self.get_batch(mode='target')  # MME used only unlabelled samples for transfer loss

        features_source, _ = self.model(inputs_source)
        features_active, _ = self.model(inputs_active)
        features_target, _ = self.model(inputs_target)

        # L2 normalization
        features_source = 1./0.05*l2_normalize(features_source)
        features_active = 1./0.05*l2_normalize(features_active)
        features_target = 1./0.05*l2_normalize(features_target)

        # Classifier loss
        outputs_source = self.model.fc(features_source)
        outputs_active = self.model.fc(features_active)

        classifier_source = nn.CrossEntropyLoss()(outputs_source, labels_source)
        classifier_active = nn.CrossEntropyLoss()(outputs_active, labels_active)
        classifier_loss = 0.5 * (classifier_source + classifier_active)

        # Transfer loss
        transfer_loss = - ((1.-active) * minimax_entropy(features_target, self.model)).sum() / sum(1. - active)

        loss = classifier_loss + 0.1*transfer_loss
        loss.backward()
        self.optimizer.step()

    @torch.no_grad()
    def predict(self, features):
        features = 1. / 0.05 * l2_normalize(features)
        return nn.Softmax(dim=1)(self.model.fc(features))


class DomainAdversarialTransferMME_AL(DomainAdversarialTransferAL):
    def __init__(self, config):
        super(DomainAdversarialTransferMME_AL, self).__init__(config)

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            net_config = self.config["network"]
            self.model = net_config["name"](**net_config["params"])
            self.model.fc = nn.Linear(self.model.output_num(),
                                      self.config["network"]["params"]["class_num"],
                                      bias=False)
            try:
                print(self.config['pretrained_path'] + '/model')
                self.model.load_state_dict(torch.load(self.config['pretrained_path'] + '/model'))
                print('Pretrained model loaded!')
            except:
                self.pre_training = True
                print('Model not loaded')
            self.model = self.model.cuda()
        elif mode == 'adversarial_network':
            super(DomainAdversarialTransferMME_AL, self).get_model('adversarial_network')
        else:
            raise NotImplementedError('Network unknown')

    def setup(self):
        self.get_model(mode='base_network')
        self.get_model(mode='adversarial_network')
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.ad_net.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')
        inputs_active, labels_active = self.get_batch(mode='active')
        inputs_target, labels_target, active = self.get_batch(mode='target')

        features_source, outputs_source = self.model(inputs_source)
        features_active, outputs_active = self.model(inputs_active)
        features_target, outputs_target = self.model(inputs_target)

        # Transfer loss
        features = torch.cat([features_source, features_target], dim=0)
        softmax_out_s = torch.zeros_like(outputs_source)
        softmax_out_s[torch.arange(outputs_source.size(0)), labels_source] = 1.0
        softmax_out_t = self.predict(features_target).detach()

        softmax_out = torch.cat([softmax_out_s, softmax_out_t], dim=0)
        transfer_loss = TSF([features, softmax_out], self.ad_net)


        # Features normalization
        features_source = 1. / 0.05 * l2_normalize(features_source)
        features_active = 1. / 0.05 * l2_normalize(features_active)
        features_target = 1. / 0.05 * l2_normalize(features_target)

        outputs_source = self.model.fc(features_source)
        outputs_active = self.model.fc(features_active)

        # Classifier loss
        classifier_source = nn.CrossEntropyLoss()(outputs_source, labels_source)
        classifier_active = nn.CrossEntropyLoss()(outputs_active, labels_active)
        classifier_loss = 0.5 * (classifier_source + classifier_active)

        # MME loss
        mme_loss = - ((1.-active) * minimax_entropy(features_target, self.model)).sum() / sum(1. - active)

        loss = classifier_loss + self.config['loss']['trade_off']*transfer_loss + 0.1*mme_loss
        loss.backward()
        self.optimizer.step()

    @torch.no_grad()
    def predict(self, features):
        features = 1. / 0.05 * l2_normalize(features)
        return nn.Softmax(dim=1)(self.model.fc(features))

