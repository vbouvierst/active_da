HEADER = "#!/bin/bash\n" \
         "#SBATCH --partition=gpu\n" \
         "#SBATCH --mem=60g\n" \
         "#SBATCH --nodes=1\n" \
         "#SBATCH --cpus-per-task=20\n" \
         "#SBATCH --gres=gpu:1\n" \
         "#SBATCH --output=%j.stdout\n" \
         "#SBATCH --error=%j.stderr\n" \
         "#SBATCH --open-mode=append\n" \
         "#SBATCH --signal=B:SIGUSR1@120\n"

COMMAND = '\npython main.py --dset $d --tag $t --method $m --query $q --budget $b '  \
          '--test_interval 250 --iter_round 2000 --rounds $r ' \
          '--machine fusion --seed '

TASKS = ['R_C', 'R_P', 'P_C', 'C_S', 'S_P', 'R_S', 'P_R']

METHODS = ['mme', 'sage_diverse']

SHOTS = ['shot_1', 'shot_3']

for m in METHODS:
    for t in TASKS:
        for s in SHOTS:
            header = HEADER + "#SBATCH --job-name=" + t[:3] + '_' + m[:2] + '_ssda'
            header += "\n\nmodule load anaconda3/2020.02/gcc-9.2.0\nsource activate DA_env\n\n"
            header += 'cd ..\n'


            if m == 'sage_diverse':
                header += "\nm=tsf_mme_"
                header += "\nd=domain_net_126"
                header += "\nq=sage_diverse"
                header += "\nr=6"
                if s == 'shot_1':
                    header += "\nb=21"
                elif s == 'shot_3':
                    header += "\nb=63"
                header += "\nt=" + str(t) + "\n"

            elif m == 'mme':
                header += "\nm=mme"
                header += "\nd=domain_net_126"
                header += "\nq=" + s
                header += "\nr=1"
                if s == 'shot_1':
                    header += "\nb=126"
                elif s == 'shot_3':
                    header += "\nb=378"
                header += "\nt=" + str(t) + "\n"


            header += '\n'

            path = 'sh/ssda_' + m + '_' + s + '_' + t + '__0123.sh'
            with open(path, 'w') as f:
                f.write(header)
                for seed in ['0', '1', '2', '3']:
                    f.write(
                        COMMAND + seed
                    )

            path = 'sh/ssda_' + m + '_' + s + '_' + t + '__4567.sh'
            with open(path, 'w') as f:
                f.write(header)
                for seed in ['4', '5', '6', '7']:
                    f.write(
                            COMMAND + seed
                    )