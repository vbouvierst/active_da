## Stochastic Adversarial Gradient Embedding for Active Domain Adaptation (Paper 5098)

This repository is the official implementation of: *Stochastic Adversarial Gradient Embedding for Active Domain Adaptation*

### Dependencies

```python
torch==1.4.0
torchvision==0.5.0
Pillow==7.0.0
tqdm==4.42.1
numpy==1.18.1
```

To set the required packages:
```bash
pip install -r requirements.txt
```

### Datasets

We used two datasets:
- [Office31](https://people.eecs.berkeley.edu/~jhoffman/domainadapt/)
- [VisDA 2017](https://github.com/VisionLearningGroup/taskcv-2017-public)

To download them simply run:

```bash
sh set_datasets.sh PATH_TO_STORE
```

Replacing `PATH_TO_STORE` with the path where you whish to store datasets. This will automatically download all of the classification datasets to be stored in `PATH_TO_STORE` and will set a symlink to `data/`, so that the paths for each dataset & task in `data/` points to the correct paths.

/!\ You may have to change the root path of the images if linking does not work.

#### How /data/... is organized!
We split each target into 2 domains (target_train and target_test) with a ratio of 1/2:
- train_target is used for learning and as a pool for annotation.
- test_target is used only at test time.

##### Examples for Amazon - Webcam (Office31)
- amazon_list_local.txt : original dataset, used as source dataset
- webcam_list_local_train.txt : target dataset used during training
- webcam_list_local_test.txt : target dataset used at test time

in pytorch/snapshot/path_of_the_experiment/:
- log.txt : track the accuracy at each round
- active.txt : store target active samples
- target.txt : target dataset, copy of webcam_list_local_test.txt, where a third variable is added (0 or 1) whether the sample is annotated.


### Training
Here are some examples of training commands. One experiment lasts about ∼12 hours on a single NVIDIA V100 GPU with 32GB memory.

- Office31 (Amazon -> Webcam) with SAGE
```
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag A_W 
--ratio_active 0.2 --requests 10
--policy SAGE_diverse
```

- Office31 (Amazon -> Webcam) with AADA (https://openaccess.thecvf.com/content_WACV_2020/papers/Su_Active_Adversarial_Domain_Adaptation_WACV_2020_paper.pdf)
```
python train_image_active_balance.py --seed 0 --method DANN --dset office --tag A_W 
--ratio_active 0.2 --requests 10
--policy entropy --weight_policy True
```

- VisDA (b=10)
```
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset visda --tag visda
--n_active 100 --requests 10
--policy SAGE_diverse
```

The results will be appended to the log files in `snapshot/OUTPUT_DIR`.

### Usage

```
usage: train_image_active_inductive.py [-h] [--seed SEED] [--method {DANN, Y_DAN}]
                [--net {ResNet18,ResNet34,ResNet50,ResNet101,ResNet152}]
                [--dset DATASET]
                [--tag TASK]
                [--ratio_active 0.2]
                [--n_active {10, 100}]
                [--requests 10]
                [--policy {random, entropy, SAGE, SAGE_diverse}]
                [--weight_policy {False, True}]


--seed -> seed of the experiment
--method -> method for computing the transfer loss
--net -> deep network
--dataset -> dataset chosen for the experiments (Office31 or VisDA)
--tag -> Task
--test_interval -> number of SGD iterations between two test phases
--ratio_active -> budget of annotation (for the Office31 dataset)
--n_active -> budget of annotation (for the VisDA dataset)
--requests -> number of requests 
--min_iter_request -> minimal number of SGD iterations between two round of annotations
--max_iter_request -> maximal number of SGD iterations between two round of annotations
--iter_last_request -> number of SGD iterations after the last request
--policy -> sample selection policy
--weight_policy -> weight score by ratio of densities (for AADA)
--projection -> If True, use the positive orthogonal projection
```


## Running AADA 
Most relevant baseline of the paper that we have reproduced: (https://openaccess.thecvf.com/content_WACV_2020/papers/Su_Active_Adversarial_Domain_Adaptation_WACV_2020_paper.pdf)

--method DANN
--policy entropy
--weight_policy True

#### Acknowledgments
This repository borrows heavily from the following repositories, we thank them for providing the implementation for their work.
- Classification code is based on: [CDAN](https://github.com/thuml/CDAN)
- Classification code is baded on: [RUDA] (https://github.com/vbouvier/ruda)