HEADER = "#!/bin/bash\n" \
         "#SBATCH --partition=gpu\n" \
         "#SBATCH --mem=60g\n" \
         "#SBATCH --nodes=1\n" \
         "#SBATCH --cpus-per-task=20\n" \
         "#SBATCH --gres=gpu:1\n" \
         "#SBATCH --output=%j.stdout\n" \
         "#SBATCH --error=%j.stderr\n" \
         "#SBATCH --open-mode=append\n" \
         "#SBATCH --signal=B:SIGUSR1@120\n"

METHODS = ['no_adaptation', 'aada', 'tsf', 'mme', 'tsf_mme', 'tsf_mme_']
TASKS = {
    'office': ['A_W', 'W_A', 'A_D', 'D_A'],
    'visda': ['visda'],
    'domain_net': ['clipart', 'infograph', 'painting', 'quickdraw', 'real', 'sketch'],
    'domain_net_126': ['R_C', 'R_P', 'P_C', 'C_S', 'S_P', 'R_S', 'P_R']
}

SEEDS = ['0', '1', '2', '3', '4', '5', '6', '7']

COMMAND = '\npython main_pretrainer.py --dset $d --tag $t --method $m ' \
          '--iter_pretrain 10000 --test_interval 5000 --machine fusion --seed '

for m in METHODS:
    for d in TASKS.keys():
        for t in TASKS[d]:
            header = HEADER + "#SBATCH --job-name=" + t
            header += "\n\nmodule load anaconda3/2020.02/gcc-9.2.0\nsource activate DA_env\n\n"
            header += 'cd ..\n'

            header += "\nm="+str(m)
            header += "\nd=" + str(d)
            header += "\nt=" + str(t) + "\n"

            path = 'sh/' + m + '_' + t + '.sh'
            with open(path, 'w') as f:
                f.write(header)
                for seed in SEEDS:
                    f.write(
                        COMMAND + seed
                    )