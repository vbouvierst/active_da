import numpy as np
from loss import *
from sklearn.cluster import KMeans
import torch
import torch.nn as nn


def random_query(data_eval):
    index = np.random.permutation(range(len(data_eval['outputs'])))
    return index


def entropy_query(data_eval):
    outputs = data_eval['outputs']
    entropy = Entropy(outputs).cpu().numpy()
    return np.argsort(entropy)[::-1]


def confidence_query(data_eval):
    outputs = data_eval['outputs']
    conf = outputs.max(dim=1)[0].cpu().numpy()
    return np.argsort(conf)


def margin_query(data_eval):
    outputs = data_eval['outputs']
    pred_sorted = np.sort(outputs, axis=1)[:, ::-1]
    margin = pred_sorted[:, 0] - pred_sorted[:, 1]
    return np.argsort(margin)


def k_means_query(data_eval):
    features = data_eval['features']
    budget = data_eval['budget']

    kmeans = KMeans(n_clusters=budget)
    kmeans.fit(features.cpu().numpy())
    cluster_centers = torch.tensor(kmeans.cluster_centers_)
    idx = []
    for cluster_id, cluster_center in enumerate(cluster_centers):
        idx.append(
            int(torch.argmin(euclidean_dist(features.cpu(), cluster_center.unsqueeze(0))))
        )
    return idx


def weight_entropy_query(data_eval):
    domains = data_eval['domains'].cpu().numpy()
    weights = (1.-domains) / (domains + 1e-6)
    outputs = data_eval['outputs']
    entropy = Entropy(outputs).cpu().numpy()
    weights_entropy = weights.reshape(-1)*entropy.reshape(-1)
    return np.argsort(weights_entropy)[::-1]


def random_per_class(data_eval):
    pass


def get_badge(features, last_layer, random_layer=None):
    grad = []
    features = features.detach()

    for b in range(features.size(0)):
        last_layer.zero_grad()  # Clean gradients

        outputs = last_layer(features[b].reshape(1, -1))
        _, preds = torch.max(outputs, dim=1)
        loss = nn.CrossEntropyLoss()(outputs, preds)
        loss.backward()

        grad_weight = last_layer.weight.grad.reshape(1, -1).clone().detach()
        grad_bias = last_layer.bias.grad.reshape(1, -1).clone().detach()

        grad.append(torch.cat([grad_weight, grad_bias], dim=1))

    badge = torch.cat(grad, dim=0)

    if random_layer is not None:
        badge_norm = badge.norm(dim=1, keepdim=True)
        badge_proj = random_layer(badge)
        badge_proj = badge_proj / badge_proj.norm(dim=1, keepdim=True)
        return badge_norm * badge_proj
    else:
        return badge


def badge_query(data_eval):
    badge = data_eval['badge']
    # Compute kmeans ++
    idx_badge, _ = k_means_pp(badge, n_clusters=data_eval['budget'])
    idx_badge = [idx.cpu().numpy() for idx in idx_badge]
    return np.asarray(idx_badge)


def get_sage(outputs, grads, random_layer=None):
    # Get dimensions
    b, d, c = grads.shape

    preds = outputs.reshape(b, 1, c)

    # Compute the mean embedding
    grad_mean = (preds * grads).sum(dim=2)

    # Normalize it
    grad_mean_normalized = \
        (grad_mean / grad_mean.norm(dim=1, keepdim=True)).reshape(b, d, 1)

    # Compute the lambda of the positive projection
    scalar = (grads * grad_mean_normalized).sum(dim=1, keepdim=True)

    # Projection
    grads = grads - scalar.abs() * grad_mean_normalized

    sage = (preds.sqrt() * grads).reshape(grads.size(0), -1)

    if random_layer is not None:
        print('projection')
        sage_norm = sage.norm(dim=1, keepdim=True)
        sage_proj = random_layer(sage)
        sage_proj = sage_proj / sage_proj.norm(dim=1, keepdim=True)
        return sage_norm * sage_proj
    else:
        return sage


def get_sage_not_proj(outputs, grads, random_layer=None):
    # Get dimensions
    b, d, c = grads.shape

    preds = outputs.reshape(b, 1, c)

    sage = (preds.sqrt() * grads).reshape(grads.size(0), -1)

    if random_layer is not None:
        print('projection')
        sage_norm = sage.norm(dim=1, keepdim=True)
        sage_proj = random_layer(sage)
        sage_proj = sage_proj / sage_proj.norm(dim=1, keepdim=True)
        return sage_norm * sage_proj
    else:
        return sage


def sage_norm_query(data_eval):
    sage = data_eval['sage']

    # Compute ||SAGE||
    sage_norm = sage.norm(dim=1).cpu().numpy()

    return np.argsort(sage_norm)[::-1]


def sage_not_proj_query(data_eval):
    sage = data_eval['sage_not_proj']

    # Compute kmeans ++
    idx_sage_diverse, _ = k_means_pp(sage, n_clusters=data_eval['budget'])
    idx_sage_diverse = [idx.cpu().numpy() for idx in idx_sage_diverse]

    return np.asarray(idx_sage_diverse)


def sage_diverse_query(data_eval):
    sage = data_eval['sage']

    # Compute kmeans ++
    idx_sage_diverse, _ = k_means_pp(sage, n_clusters=data_eval['budget'])
    idx_sage_diverse = [idx.cpu().numpy() for idx in idx_sage_diverse]

    return np.asarray(idx_sage_diverse)


def shot_1_query(data_eval):
    labels = data_eval['labels']
    index = data_eval['index']

    index_labels = {}

    for i in range(labels.shape[0]):
        try:
            index_labels[labels[i].cpu().item()].append(index[i].cpu().item())
        except:
            index_labels[labels[i].item()] = [index[i].cpu().item()]

    index_1 = []

    for label in list(index_labels.keys()):
        idx = np.random.choice(index_labels[label], replace=False, size=1)
        index_1 += list(idx)
    return index_1


def shot_3_query(data_eval):
    labels = data_eval['labels']
    index = data_eval['index']

    index_labels = {}

    for i in range(labels.shape[0]):
        try:
            index_labels[labels[i].cpu().item()].append(index[i].cpu().item())
        except:
            index_labels[labels[i].item()] = [index[i].cpu().item()]

    index_3 = []

    for label in list(index_labels.keys()):
        idx = np.random.choice(index_labels[label], replace=False, size=3)
        index_3 += list(idx)
    return index_3
