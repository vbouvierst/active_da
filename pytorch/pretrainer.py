import time
import torch
import numpy as np
import torch.nn as nn
from network import *
from loss import *
from pre_process import *
from lr_schedule import *
from data_list import *
import lr_schedule
from torch.utils.data import DataLoader
from tqdm import tqdm
import pickle
import copy
import os


class PreTrainer:
    def __init__(self, config):
        self.config = config
        self.batch_size = 32
        self.transform_source = None
        self.transform_target = None
        self.test_bs = None

        self.source_path = None  # Source data path
        self.target_path = None  # Target data path
        self.test_path = None  # Path of test data

        self.source_data = None  # Source data as a loader
        self.target_data = None  # Target data as a loader
        self.test_data = None    # Test data as a loader

        self.iter_source = None  # Source data iterator
        self.iter_target = None  # Target data iterator

        self.model = None
        self.optimizer = None
        self.log_accuracy = {}

        self.prepare_data()

    def prepare_data(self):
        prep_dict = {}
        prep_config = self.config["prep"]
        self.transform_source = image_train(**self.config["prep"]["params"])
        self.transform_target = image_train(**self.config["prep"]["params"])

        prep_dict["test"] = image_test_10crop(**self.config["prep"]["params"])

        data_config = self.config["data"]

        # Set batch size
        self.train_bs = 32 #32
        self.test_bs = 32

        # Set path
        self.source_path = data_config["source"]["list_path"]
        self.target_path = data_config["target"]["list_path"]
        self.test_path = data_config["test"]["list_path"]

        with open(self.source_path, 'r') as f:
            lines_source = f.readlines()

        with open(self.target_path, 'r') as f:
            lines_target = f.readlines()

        # Load data
        for d in ['source', 'target', 'test']:
            self.load_data(mode=d)
        print('Prepare data --> Success')

    def load_data(self, mode):
        if mode == 'source':
            # Source data
            dsets = ImageList(
                open(self.source_path).readlines(), transform=self.transform_source
            )
            self.source_data = DataLoader(
                dsets,
                batch_size=self.train_bs,
                shuffle=True,
                num_workers=4,
                drop_last=True,
            )

        elif mode == 'target':
            # Target data
            dsets = ImageList(
                open(self.target_path).readlines(), transform=self.transform_target
            )

            self.target_data = DataLoader(
                dsets,
                batch_size=self.train_bs,
                shuffle=True,
                num_workers=4,
                drop_last=True
            )

        elif mode == 'test':
            if self.config['prep']['test_10crop']:
                prep_dict = image_test_10crop(**self.config["prep"]["params"])

                dsets = [
                    ImageList(
                        open(self.test_path).readlines(), transform=prep_dict[i]
                    )
                    for i in range(10)
                    ]
                self.test_data = [
                    DataLoader(
                        dset, batch_size=self.test_bs, shuffle=False, num_workers=4
                    )
                    for dset in dsets
                    ]

            else:
                prep_dict = image_test(**self.config["prep"]["params"])

                dset = ImageList(
                    open(self.test_path).readlines(), transform=prep_dict
                )

                self.test_data = DataLoader(
                    dset, batch_size=self.test_bs, shuffle=False, num_workers=4
                )

    def get_batch(self, mode):
        if mode == 'source':
            try:
                inputs_source, labels_source = self.iter_source.next()
            except:
                self.iter_source = iter(self.source_data)
                inputs_source, labels_source = self.iter_source.next()
            inputs_source, labels_source = inputs_source.cuda(), labels_source.cuda()
            return inputs_source, labels_source

        elif mode == 'target':
            try:
                inputs_target, labels_active = self.iter_target.next()
                labels_target, active = labels_active[:, 0], labels_active[:, 1].float()
            except:
                self.iter_target = iter(self.target_data)
                inputs_target, labels_active = self.iter_target.next()
                labels_target, active = labels_active[:, 0], labels_active[:, 1].float()
            inputs_target, labels_target, active = inputs_target.cuda(), labels_target.cuda(), active.cuda()
            return inputs_target, labels_target, active

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            net_config = self.config["network"]
            self.model = net_config["name"](**net_config["params"])
            self.model = self.model.cuda()
        else:
            raise NotImplementedError('Network unknown')

    def get_optimizer(self):
        params = self.model.get_parameters()

        optimizer_config = self.config["optimizer"]
        self.optimizer = optimizer_config["type"](
            params, **(optimizer_config["optim_params"])
        )
        param_lr = []
        for param_group in self.optimizer.param_groups:
            param_lr.append(param_group["lr"])

    def lr_scheduler(self, step):
        optimizer_config = self.config["optimizer"]
        schedule_param = optimizer_config["lr_param"]
        lr_scheduler = lr_schedule.schedule_dict[optimizer_config["lr_type"]]
        self.optimizer = lr_scheduler(self.optimizer, step, **schedule_param)

    def setup(self):
        self.get_model()
        self.get_optimizer()

    def run(self):
        self.setup()
        for i in tqdm(range(self.config['iter_pretraining'])):
            self.lr_scheduler(i)
            self.train_one_iter()
            if self.time_to_test(i):
                accuracy = self.test()
                self.log(i, accuracy)
        self.save()

    def time_to_test(self, i):
        return i % self.config["test_interval"] == self.config["test_interval"] - 1

    def train_one_iter(self):
        pass

    @torch.no_grad()
    def predict(self, features):
        return self.model.fc(features)

    @torch.no_grad()
    def test(self):
        self.model.eval()
        start_test = True
        test_10crop = self.config['prep']['test_10crop']
        print('test done with test_10crop', test_10crop)

        if test_10crop:
            iter_test = [iter(self.test_data[i]) for i in range(10)]
            len_test = len(self.test_data[0])
        else:
            iter_test = iter(self.test_data)
            len_test = len(iter_test)

        for _ in tqdm(range(len_test)):
            if test_10crop:
                data = [iter_test[j].next() for j in range(10)]
                inputs = [data[j][0] for j in range(10)]
                labels = data[0][1]
                for j in range(10):
                    inputs[j] = inputs[j].cuda()
            else:
                inputs, labels = iter_test.next()
                inputs = inputs.cuda()

            if test_10crop:
                features_ = []
                outputs_ = []
                for j in range(10):
                    features, _ = self.model(inputs[j])
                    outputs = self.predict(features)
                    features_.append(features)
                    outputs_.append(outputs)
                features = sum(features_) / 10.
                outputs = sum(outputs_) / 10.
            else:
                features, _ = self.model(inputs)
                outputs = self.predict(features)

            if start_test:
                all_features = features.float().cpu()
                all_output = outputs.float().cpu()
                all_label = labels.float()
                start_test = False
            else:
                all_features = torch.cat([all_features, features.cpu()], dim=0)
                all_output = torch.cat((all_output, outputs.float().cpu()), 0)
                all_label = torch.cat((all_label, labels.float()), 0)
        _, predict = torch.max(all_output, 1)

        accuracy = torch.sum(torch.squeeze(predict).float() == all_label).item() / float(all_label.size()[0])
        return accuracy

    def log(self, i, accuracy):
        log_str = "iter: {:05d}, accuracy classifier: {:.5f}".format(i, accuracy)
        print(log_str)
        self.config["out_file"].write(log_str + "\n")
        self.config["out_file"].flush()

    def save(self):
        torch.save(self.model.state_dict(), self.config['output_path'] + '/model')


class NoAdaptationPT(PreTrainer):
    def __init__(self, config):
        super(NoAdaptationPT, self).__init__(config)

    def train_one_iter(self):
        self.model.train()
        self.optimizer.zero_grad()
        inputs, labels = self.get_batch(mode='source')
        _, outputs = self.model(inputs)
        loss = nn.CrossEntropyLoss()(outputs, labels)
        loss.backward()
        self.optimizer.step()


class DomainAdversarialPT(PreTrainer):
    def __init__(self, config):
        super(DomainAdversarialPT, self).__init__(config)

        self.ad_net = None

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            super(DomainAdversarialPT, self).get_model(mode='base_network')
        elif mode == 'adversarial_network':
            self.ad_net = AdversarialNetwork(
                self.model.output_num(),
                1024,
                max_iter=self.config['ramp_up_trans']
            )
            self.ad_net = self.ad_net.cuda()
        else:
            raise NotImplementedError('Network unknown')

    def get_optimizer(self):
        params = self.model.get_parameters() + self.ad_net.get_parameters()

        optimizer_config = self.config["optimizer"]
        self.optimizer = optimizer_config["type"](
            params, **(optimizer_config["optim_params"])
        )

        param_lr = []
        for param_group in self.optimizer.param_groups:
            param_lr.append(param_group["lr"])

    def setup(self):
        self.get_model(mode='base_network')
        self.get_model(mode='adversarial_network')
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.ad_net.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')
        inputs_target, _, _ = self.get_batch(mode='target')

        features_source, outputs_source = self.model(inputs_source)
        features_target, outputs_target = self.model(inputs_target)

        # Classifier loss
        classifier_loss = nn.CrossEntropyLoss()(outputs_source, labels_source)

        # Transfer loss
        features = torch.cat([features_source, features_target], dim=0)
        transfer_loss = DANN(features, self.ad_net, hook=True)

        loss = classifier_loss + transfer_loss
        loss.backward()
        self.optimizer.step()

    def save(self):
        torch.save(self.model.state_dict(), self.config['output_path'] + '/model')
        torch.save(self.ad_net.state_dict(), self.config['output_path'] + '/ad_net')


class DomainAdversarialTransferPT(PreTrainer):
    def __init__(self, config):
        super(DomainAdversarialTransferPT, self).__init__(config)

        self.ad_net = None

    def get_model(self, mode='base_network'):
        if mode == 'base_network':
            super(DomainAdversarialTransferPT, self).get_model(mode='base_network')
        elif mode == 'adversarial_network':
            self.ad_net = AdversarialNetwork(
                self.model.output_num(),
                1024,
                output_dim=self.config["network"]["params"]["class_num"],
                max_iter=self.config['ramp_up_trans']
            )
            self.ad_net = self.ad_net.cuda()
        else:
            raise NotImplementedError('Network unknown')

    def get_optimizer(self):
        params = self.model.get_parameters() + self.ad_net.get_parameters()

        optimizer_config = self.config["optimizer"]
        self.optimizer = optimizer_config["type"](
            params, **(optimizer_config["optim_params"])
        )

        param_lr = []
        for param_group in self.optimizer.param_groups:
            param_lr.append(param_group["lr"])

    def setup(self):
        self.get_model(mode='base_network')
        self.get_model(mode='adversarial_network')
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.ad_net.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')
        inputs_target, labels_target, active = self.get_batch(mode='target')

        features_source, outputs_source = self.model(inputs_source)
        features_target, outputs_target = self.model(inputs_target)

        # Computes the classifier loss
        classifier_loss = nn.CrossEntropyLoss()(outputs_source, labels_source)

        # Computes the transfer loss
        features = torch.cat([features_source, features_target], dim=0)
        softmax_out_s = torch.zeros_like(outputs_source)
        softmax_out_s[torch.arange(outputs_source.size(0)), labels_source] = 1.0
        softmax_out_t = nn.Softmax(dim=1)(outputs_target)

        softmax_out = torch.cat([softmax_out_s, softmax_out_t], dim=0)

        transfer_loss = TSF([features, softmax_out], self.ad_net)

        # Compute global loss
        loss = classifier_loss + transfer_loss
        loss.backward()
        self.optimizer.step()

    def save(self):
        torch.save(self.model.state_dict(), self.config['output_path'] + '/model')
        torch.save(self.ad_net.state_dict(), self.config['output_path'] + '/ad_net')



class MiniMaxEntropyPT(PreTrainer):
    def __init__(self, config):
        super(MiniMaxEntropyPT, self).__init__(config)

    def setup(self):
        self.get_model()
        self.model.fc = nn.Linear(self.model.output_num(),
                                  self.config["network"]["params"]["class_num"],
                                  bias=False).cuda()
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')
        inputs_target, labels_target, active = self.get_batch(mode='target')

        features_source, _ = self.model(inputs_source)
        features_target, _ = self.model(inputs_target)

        features_source = 1./0.05*l2_normalize(features_source)
        features_target = 1./0.05*l2_normalize(features_target)

        outputs_source = self.model.fc(features_source)

        # Classifier loss
        classifier_loss = nn.CrossEntropyLoss()(outputs_source, labels_source)

        # Transfer loss
        transfer_loss = - minimax_entropy(features_target, self.model).mean()

        loss = classifier_loss + 0.1*transfer_loss
        loss.backward()
        self.optimizer.step()

    @torch.no_grad()
    def predict(self, features):
        features = 1. / 0.05 * l2_normalize(features)
        return self.model.fc(features)


class DomainAdversarialTransferMMEPT(DomainAdversarialTransferPT):
    def __init__(self, config):
        super(DomainAdversarialTransferMMEPT, self).__init__(config)

    def setup(self):
        self.get_model(mode='base_network')
        self.get_model(mode='adversarial_network')

        self.model.fc = nn.Linear(self.model.output_num(),
                                  self.config["network"]["params"]["class_num"],
                                  bias=False).cuda()
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.ad_net.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')
        inputs_target, labels_target, active = self.get_batch(mode='target')

        features_source, outputs_source = self.model(inputs_source)
        features_target, outputs_target = self.model(inputs_target)

        # Features normalization
        features_source = 1. / 0.05 * l2_normalize(features_source)
        features_target = 1. / 0.05 * l2_normalize(features_target)

        outputs_source = self.model.fc(features_source)
        outputs_target = self.model.fc(features_target)

        # Classifier loss
        classifier_loss = nn.CrossEntropyLoss()(outputs_source, labels_source)

        # Transfer loss
        features = torch.cat([features_source, features_target], dim=0)
        softmax_out_s = torch.zeros_like(outputs_source)
        softmax_out_s[torch.arange(outputs_source.size(0)), labels_source] = 1.0
        softmax_out_t = nn.Softmax(dim=1)(outputs_target)

        softmax_out = torch.cat([softmax_out_s, softmax_out_t], dim=0)
        transfer_loss = TSF([features, softmax_out], self.ad_net)

        # MME loss
        mme_loss = - minimax_entropy(features_target, self.model).mean()

        loss = classifier_loss + transfer_loss + 0.1*mme_loss
        loss.backward()
        self.optimizer.step()

    @torch.no_grad()
    def predict(self, features):
        features = 1. / 0.05 * l2_normalize(features)
        return self.model.fc(features)


class DomainAdversarialTransferMMEPT_(DomainAdversarialTransferPT):
    def __init__(self, config):
        super(DomainAdversarialTransferMMEPT_, self).__init__(config)

    def setup(self):
        self.get_model(mode='base_network')
        self.get_model(mode='adversarial_network')

        self.model.fc = nn.Linear(self.model.output_num(),
                                  self.config["network"]["params"]["class_num"],
                                  bias=False).cuda()
        self.get_optimizer()

    def train_one_iter(self):
        self.model.train()
        self.ad_net.train()
        self.optimizer.zero_grad()

        inputs_source, labels_source = self.get_batch(mode='source')
        inputs_target, labels_target, active = self.get_batch(mode='target')

        features_source, outputs_source = self.model(inputs_source)
        features_target, outputs_target = self.model(inputs_target)


        # Transfer loss
        features = torch.cat([features_source, features_target], dim=0)
        softmax_out_s = torch.zeros_like(outputs_source)
        softmax_out_s[torch.arange(outputs_source.size(0)), labels_source] = 1.0
        softmax_out_t = self.predict(features_target).detach()

        softmax_out = torch.cat([softmax_out_s, softmax_out_t], dim=0)
        transfer_loss = TSF([features, softmax_out], self.ad_net)


        # Features normalization
        features_source = 1. / 0.05 * l2_normalize(features_source)
        features_target = 1. / 0.05 * l2_normalize(features_target)

        outputs_source = self.model.fc(features_source)

        # Classifier loss
        classifier_loss = nn.CrossEntropyLoss()(outputs_source, labels_source)


        # MME loss
        mme_loss = - minimax_entropy(features_target, self.model).mean()

        loss = classifier_loss + self.config['loss']['trade_off']*transfer_loss + 0.1*mme_loss
        loss.backward()
        self.optimizer.step()

    @torch.no_grad()
    def predict(self, features):
        features = 1. / 0.05 * l2_normalize(features)
        return nn.Softmax(dim=1)(self.model.fc(features))
