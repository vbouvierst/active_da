import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import math
import torch.nn.functional as F
import pdb
from tqdm import tqdm as tqdm


import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import math
import torch.nn.functional as F
from network import grl_hook



CUDA = True # Use gpus


def Entropy(input_):
    bs = input_.size(0)
    epsilon = 1e-5
    entropy = -input_ * torch.log(input_ + epsilon)
    entropy = torch.sum(entropy, dim=1)
    return entropy


def DANN(features, ad_net, hook=True):
    # Domain Adversarial Neural Network loss 
    # From http://proceedings.mlr.press/v37/ganin15.pdf

    ad_out = ad_net(features, hook=hook)
    batch_size = ad_out.size(0) // 2
    dc_target = torch.from_numpy(np.array([[1]] * batch_size + [[0]] * batch_size)).float()
    if CUDA:
        dc_target = dc_target.cuda()

    if CUDA:
        dc_target = dc_target.cuda()

    return nn.BCELoss()(ad_out, dc_target)


def TSF(input_list, ad_net):
    # Transfer loss 
    # From https://arxiv.org/pdf/2006.13629.pdf
    softmax_output = input_list[1].detach()
    feature = input_list[0]

    ad_out = ad_net(feature)
    batch_size = ad_out.size(0) // 2
    dc_target = torch.from_numpy(np.array(np.ones([batch_size, softmax_output.shape[1]]).tolist() +
                                          np.zeros([batch_size, softmax_output.shape[1]]).tolist())).float()

    if CUDA:
        dc_target = dc_target.cuda()

    loss = (softmax_output * nn.BCELoss(reduction='none')(ad_out, dc_target)).mean(dim=0).sum()
    return loss


def l2_normalize(x):
    return x / x.norm(dim=1, keepdim=True)


def minimax_entropy(features, model):
    features.register_hook(grl_hook(1.))
    outputs = model.fc(features)
    pred = nn.Softmax(dim=1)(outputs)
    return Entropy(pred)


def TSF_grad(input_list, ad_net):
    # Computes the grad of transferability loss with respect to representations
    # Eq (6) of the paper.
    output = input_list[1].detach()
    feature = input_list[0]

    ad_out = ad_net(feature)

    batch_size = ad_out.size(0)

    dc_target = torch.from_numpy(
        np.array(
            np.zeros( # zeros
                 [batch_size, output.shape[1]]
            ).tolist())
    ).float().cuda()

    transfer_loss = nn.BCELoss(reduction='none')(ad_out, dc_target).sum(dim=0)

    grad = []
    for c in range(output.shape[1]):
        if c < output.shape[1] - 1:
            grad.append(
                torch.autograd.grad(transfer_loss[c],
                                    feature,
                                    retain_graph=True
                                    )[0].reshape(-1, feature.shape[1], 1)
            )
        else:
            grad.append(
                torch.autograd.grad(
                    transfer_loss[c],
                    feature
                )[0].reshape(-1, feature.shape[1], 1))
    grad = torch.cat(grad, dim=2)
    return grad 


def Y_DAN_grad(input_list, ad_net):
    # Computes the grad of transferability loss with respect to representations
    # Eq (6) of the paper.

    softmax_output = input_list[1].detach()
    feature = input_list[0]

    ad_out = ad_net(feature, hook=False)

    batch_size = ad_out.size(0)

    dc_target = torch.from_numpy(np.array(np.zeros([batch_size, softmax_output.shape[1]]).tolist())).float().cuda()

    transfer_loss = (nn.BCELoss(reduction='none')(ad_out, dc_target)).sum(dim=0)

    grad = []
    for c in range(softmax_output.shape[1]):
        if c < softmax_output.shape[1] - 1:
            grad.append(torch.autograd.grad(transfer_loss[c], feature,
                                            retain_graph=True)[0].reshape(-1, feature.shape[1], 1))
        else:
            grad.append(torch.autograd.grad(transfer_loss[c], feature)[0].reshape(-1, feature.shape[1], 1))

    softmax = softmax_output.reshape([softmax_output.shape[0], 1, softmax_output.shape[1]])
    grad = torch.cat(grad, dim=2)
    return grad


def w_from_ad(config, features, ad_net, eps=1e-2):
    # Computes weight from a DANN network 
    # Eq (4) from https://openaccess.thecvf.com/content_WACV_2020/papers/Su_Active_Adversarial_Domain_Adaptation_WACV_2020_paper.pdf
    if config['method'] == 'DANN':
        ad_out = ad_net(features, hook=False)
        return (1.-ad_out) / (ad_out + eps)
    else:
        return torch.ones_like(features[:, 0])


def to_one_hot(input, size):
    bs = input.shape[0]

    oh = torch.zeros([bs, size]).cuda()

    for i in range(bs):
        oh[i, input[i]] = 1.

    return oh


def euclidean_dist(x, y):
    # x: N x D
    # y: M x D
    n = x.size(0)
    m = y.size(0)
    d = x.size(1)
    assert d == y.size(1)

    x = x.unsqueeze(1).expand(n, m, d)
    y = y.unsqueeze(0).expand(n, m, d)

    return torch.pow(x - y, 2).sum(2)


def k_means_pp(x, n_clusters=20):
    d = x.norm(dim=1)
    _, idx = d.max(dim=0)
    clusters = [x[idx.clone()]]
    idx_list = [idx.clone()]

    print('k-means++ ...')
    for _ in tqdm(range(n_clusters - 1)):
        d_to_clusters = torch.zeros(x.size(0), len(clusters))
        for i, c in enumerate(clusters):
            d_to_clusters[:, i] = (x - c).norm(dim=1)

        # computes minimal distance to cluster
        min_d_to_clusters = d_to_clusters.min(dim=1)[0]
        _, idx = min_d_to_clusters.max(dim=0)
        clusters.append(x[idx.clone()])
        idx_list.append(idx.clone())
    return idx_list, clusters


def weight_class(labels, num_class=31):
    label_counts = torch.tensor([(labels.cpu() == c).float().sum() for c in range(num_class)])
    p_class = label_counts / label_counts.sum()
    w = 1. / (1e-8 + p_class)*(p_class > 0).float() / num_class
    return w.detach().cuda()


def h_distance(vp1, vp2):
    # Compute the h distance between vp = (h, grad)
    # Eq (9) and (10) of the paper.

    v_1, p_1 = vp1[0], vp1[1]
    v_2, p_2 = vp2[0], vp2[1]

    if v_1.dim() == 2:
        v_1 = v_1.reshape(1, v_1.shape[0], v_1.shape[1])
        p_1 = p_1.reshape(1, -1)

    if v_2.dim() == 2:
        v_2 = v_2.reshape(1, v_2.shape[0], v_2.shape[1])
        p_2 = p_2.reshape(1, -1)

    n_1, d, c = v_1.shape
    n_2 = v_2.shape[0]

    v_1 = p_1.sqrt().reshape(n_1, 1, c) * v_1
    v_2 = p_2.sqrt().reshape(n_2, 1, c) * v_2

    v_1, v_2 = v_1.reshape(n_1, c * d), v_2.reshape(n_2, c * d)

    return (v_1 - v_2).norm(dim=1)


def h_kmeans_pp(data, k, projection=True):
    # k-means ++ init from http://ilpubs.stanford.edu:8090/778/1/2006-13.pdf
    # We use here the distance from Eq (9) and (10) of the paper.

    print('stats of grad', data['grads'].mean(), data['grads'].norm(), data['grads'].max())
    print('stats of pred', Entropy(data['g']).mean())

    if projection:
        x = stochastic_gradient_projection(data)
    else:
        x = data['grads']

    p = data['g']

    centroids = [torch.max(h_distance([x, p], [0.*x, 0.*p]), dim=0)[1]]
    distances = (h_distance([x, p], [x[centroids[0]], p[centroids[0]]])).view(-1, 1)
    while len(centroids) < k:
        #update distances
        distances = torch.cat([distances,
                           (h_distance([x, p], [x[centroids[-1]], p[centroids[-1]]])).view(-1, 1)
                           ], dim=1)
        distances_to_centroids, _ = distances.min(dim=1)
        centroids.append(torch.max(distances_to_centroids, dim=0)[1])
    return centroids


def stochastic_gradient_projection(data):
    # Positive orthogonal projection from Eq (8) of the paper.
    softmax = data['g'].reshape(data['g'].shape[0], 1, data['g'].shape[1])
    grad_ref = (softmax * data['grads']).sum(dim=2)
    grad_ref = (grad_ref / grad_ref.norm(dim=1, keepdim=True)).reshape(grad_ref.shape[0], grad_ref.shape[1], 1)
    scalar = (data['grads'] * grad_ref).sum(dim=1, keepdim=True)
    grad_proj = data['grads'] - scalar.abs() * grad_ref
    return grad_proj
