HEADER = "#!/bin/bash\n" \
         "#SBATCH --partition=gpu\n" \
         "#SBATCH --mem=60g\n" \
         "#SBATCH --nodes=1\n" \
         "#SBATCH --cpus-per-task=20\n" \
         "#SBATCH --gres=gpu:1\n" \
         "#SBATCH --output=%j.stdout\n" \
         "#SBATCH --error=%j.stderr\n" \
         "#SBATCH --open-mode=append\n" \
         "#SBATCH --signal=B:SIGUSR1@120\n"

METHODS = ['no_adaptation', 'aada', 'tsf', 'mme', 'tsf_mme_']

QUERIES = {
    'no_adaptation': ['random', 'entropy', 'kmeans'],
    'aada': ['weight_entropy'],
    'tsf': ['random', 'entropy', 'kmeans', 'margin', 'confidence', 'badge', 'sage_norm', 'sage_diverse', 
            'sage_not_proj'],
    'mme': ['random', 'entropy', 'kmeans'],
    'tsf_mme_': ['random', 'entropy', 'kmeans', 'badge', 'sage_norm', 'sage_diverse']
}

TASKS = {
    'office': ['A_W', 'W_A', 'A_D', 'D_A'],
    'visda': ['visda'],
    'domain_net': ['clipart', 'infograph', 'painting', 'quickdraw', 'real', 'sketch'],
    'domain_net_126': ['P_C', 'C_P']
}

BUDGET = {
    'office': [4, 8, 16],
    'visda': [16, 128],
    'domain_net': [128, 256],
    'domain_net_126': [128]
}

SEEDS_0123 = ['0', '1', '2', '3']
SEEDS_4567 = ['4', '5', '6', '7']


COMMAND = '\npython main.py --dset $d --tag $t --method $m --query $q --budget $b '  \
          '--test_interval 250 --iter_round 2000 --rounds 10 ' \
          '--machine fusion --seed '


for m in METHODS:
    for d in TASKS.keys():
        for t in TASKS[d]:
            for q in QUERIES[m]:
                for b in BUDGET[d]:
                    header = HEADER + "#SBATCH --job-name=" + t[:3] + '_' + m[:2] + '_' + q[:2]
                    header += "\n\nmodule load anaconda3/2020.02/gcc-9.2.0\nsource activate DA_env\n\n"
                    header += 'cd ..\n'

                    header += "\nm="+str(m)
                    header += "\nd=" + str(d)
                    header += "\nq=" + str(q)
                    header += "\nb=" + str(b)
                    header += "\nt=" + str(t) + "\n"

                    path = 'sh/' + m + '_' + q + '_' + t + '_' + str(b) + '__0123.sh'
                    with open(path, 'w') as f:
                        f.write(header)
                        for seed in SEEDS_0123:
                            f.write(
                                COMMAND + seed
                            )

                    path = 'sh/' + m + '_' + q + '_' + t + '_' + str(b) + '__4567.sh'
                    with open(path, 'w') as f:
                        f.write(header)
                        for seed in SEEDS_4567:
                            f.write(
                                COMMAND + seed
                            )