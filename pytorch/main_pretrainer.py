import argparse
import os
import os.path as osp

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from network import *
from loss import *
from pre_process import *
from lr_schedule import *
import data_list
from data_list import ImageList
from torch.autograd import Variable
from tqdm import tqdm
import pickle
from active_learner import *
from query import *
from parse_snapshot import LOG_CONFIG
from datetime import datetime
from pretrainer import *


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Active Domain Adaptation')

    parser.add_argument('--method', type=str,
                        default='tsf_mme_',
                        choices=['tsf',
                                 'aada',
                                 'tsf',
                                 'mme',
                                 'tsf_mme',
                                 'tsf_mme_'],
                        help='Method for active domain adaptation')

    parser.add_argument('--net', type=str, default='ResNet50',
                        choices=["ResNet18", "ResNet34", "ResNet50", "ResNet101", "ResNet152"])

    parser.add_argument('--dset', type=str, default='office', help="The dataset or source dataset used")

    parser.add_argument('--tag', type=str, default='A_W', help="Tag of the experiments for saving")

    parser.add_argument('--test_interval', type=int, default=250, help="interval of two continuous test phase")

    parser.add_argument('--iter_pretrain', type=int, default=10000,
                        help="Number of iterations for pretraining")

    parser.add_argument('--lr', type=float, default=0.001, help="learning rate")

    parser.add_argument('--seed', type=int, default=0, help="seed")

    parser.add_argument('--batch_size', type=int, default=32, help="batch_size")

    parser.add_argument('--machine', type=str, default='monstre', help="machine", choices=['monstre', 'fusion'])

    args = parser.parse_args()

    torch.backends.cudnn.benchmark = True
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)

    config = {}
    config['method'] = args.method
    config['iter_pretraining'] = args.iter_pretrain
    config['task'] = args.tag
    config['test_interval'] = args.test_interval
    config['seed'] = args.seed

    config['tag'] = 'PT_' + config['method'] + '_' + args.tag + '_' + str(args.seed)

    if args.dset == 'office':
        if args.tag == 'A_W':
            s_dset_path = '../data/office/amazon_list_local.txt'
            t_dset_path = '../data/office/webcam_list_local.txt'

        elif args.tag == 'W_A':
            s_dset_path = '../data/office/webcam_list_local.txt'
            t_dset_path = '../data/office/amazon_list_local.txt'

        elif args.tag == 'A_D':
            s_dset_path = '../data/office/amazon_list_local.txt'
            t_dset_path = '../data/office/dslr_list_local.txt'

        elif args.tag == 'D_A':
            s_dset_path = '../data/office/dslr_list_local.txt'
            t_dset_path = '../data/office/amazon_list_local.txt'

        elif args.tag == 'D_W':
            s_dset_path = '../data/office/dslr_list_local.txt'
            t_dset_path = '../data/office/webcam_list_local.txt'

        elif args.tag == 'W_D':
            s_dset_path = '../data/office/webcam_list_local.txt'
            t_dset_path = '../data/office/dslr_list_local.txt'

    elif args.dset == 'office-home':
        if args.tag == 'A_C':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Clipart_local.txt'
        elif args.tag == 'A_P':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'
        elif args.tag == 'A_P':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'
        elif args.tag == 'A_R':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Real_World_local.txt'
        elif args.tag == 'C_A':
            s_dset_path = '../data/office-home/Clipart_local.txt'
            t_dset_path = '../data/office-home/Art_local.txt'
        elif args.tag == 'C_P':
            s_dset_path = '../data/office-home/Clipart_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'
        elif args.tag == 'C_R':
            s_dset_path = '../data/office-home/Clipart_local.txt'
            t_dset_path = '../data/office-home/Real_World_local.txt'
        elif args.tag == 'P_A':
            s_dset_path = '../data/office-home/Product_local.txt'
            t_dset_path = '../data/office-home/Art_local.txt'
        elif args.tag == 'P_C':
            s_dset_path = '../data/office-home/Product_local.txt'
            t_dset_path = '../data/office-home/Clipart_local.txt'
        elif args.tag == 'P_R':
            s_dset_path = '../data/office-home/Product_local.txt'
            t_dset_path = '../data/office-home/Real_World_local.txt'
        elif args.tag == 'R_A':
            s_dset_path = '../data/office-home/Real_World_local.txt'
            t_dset_path = '../data/office-home/Art_local.txt'
        elif args.tag == 'R_C':
            s_dset_path = '../data/office-home/Real_World_local.txt'
            t_dset_path = '../data/office-home/Clipart_local.txt'
        elif args.tag == 'R_P':
            s_dset_path = '../data/office-home/Real_World_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'

    elif args.dset == 'visda':
        s_dset_path = '../data/visda-2017/train_list_local.txt'
        t_dset_path = '../data/visda-2017/validation_list_local.txt'

    elif args.dset == 'domain_net':
        s_dset_path = '../data/domain_net/source_combined_' + args.tag + '_train_local.txt'
        t_dset_path = '../data/domain_net/' + args.tag + '_train_local.txt'

    elif args.dset == 'domain_net_126':
        if args.tag == 'R_C':
            s_dset_path = '../data/domain_net/real_126_local.txt'
            t_dset_path = '../data/domain_net/clipart_126_local.txt'
        elif args.tag == 'R_P':
            s_dset_path = '../data/domain_net/real_126_local.txt'
            t_dset_path = '../data/domain_net/painting_126_local.txt'
        elif args.tag == 'P_C':
            s_dset_path = '../data/domain_net/painting_126_local.txt'
            t_dset_path = '../data/domain_net/clipart_126_local.txt'
        elif args.tag == 'C_S':
            s_dset_path = '../data/domain_net/clipart_126_local.txt'
            t_dset_path = '../data/domain_net/sketch_126_local.txt'
        elif args.tag == 'S_P':
            s_dset_path = '../data/domain_net/sketch_126_local.txt'
            t_dset_path = '../data/domain_net/painting_126_local.txt'
        elif args.tag == 'R_S':
            s_dset_path = '../data/domain_net/real_126_local.txt'
            t_dset_path = '../data/domain_net/sketch_126_local.txt'
        elif args.tag == 'P_R':
            s_dset_path = '../data/domain_net/painting_126_local.txt'
            t_dset_path = '../data/domain_net/real_126_local.txt'

    # DomainNet has a train test split
    if args.dset != 'domain_net':
        test_dset_path = t_dset_path[:-4] + '_test.txt'
        t_dset_path = t_dset_path[:-4] + '_train.txt'
    else:
        test_dset_path = t_dset_path[:-15] + 'test_local.txt'

    config["output_path"] = 'pretrained_models/' + config['tag']

    if not osp.exists(config["output_path"]):
        os.system('mkdir -p ' + config["output_path"])

    config["out_file"] = open(osp.join(config["output_path"], "log.txt"), "w")

    target_dset_path = os.path.join(config["output_path"], 'target.txt')

    config['history_path'] = os.path.join(config["output_path"], 'history')

    with open(config['history_path'], 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump([], f)

    if args.machine == 'fusion':
        s_dset_path = s_dset_path[:-4] + '_fusion.txt'
        t_dset_path = t_dset_path[:-4] + '_fusion.txt'
        test_dset_path = test_dset_path[:-4] + '_fusion.txt'

    with open(target_dset_path, 'w') as f_at:
        with open(t_dset_path, 'r') as f:
            index = 0
            for line in f:
                active = '0'
                f_at.write(line[:-1] + ' ' + active + ' ' + str(index) + '\n')
                index += 1

    config['path'] = {'source_path': s_dset_path,
                      'target_path': target_dset_path,
                      'test_path': test_dset_path}

    config["prep"] = {"test_10crop": True, 'params': {"resize_size": 256, "crop_size": 224, 'alexnet': False}}

    if args.dset in ['visda', 'domain_net', 'domain_net_126']:
        config["prep"]["test_10crop"] = False

    config["loss"] = {"trade_off": 1.}
    config["network"] = {"name": ResNetFc, "params": {"resnet_name": args.net,
                                                      "use_bottleneck": True,
                                                      "bottleneck_dim": 256,
                                                      "new_cls": True}}

    if args.dset == 'domain_net':
        config['network']['params']['bottleneck_dim'] = 1024
    elif args.dset == 'domain_net_126':
        config['network']['params']['bottleneck_dim'] = 512

    config["optimizer"] = {"type": optim.SGD, "optim_params": {'lr': args.lr, "momentum": 0.9, \
                                                               "weight_decay": 0.0005, "nesterov": True},
                           "lr_type": "inv", \
                           "lr_param": {"lr": args.lr, "gamma": 0.001, "power": 0.75}}
    config["dataset"] = args.dset
    config["data"] = {"source": {"list_path": s_dset_path,
                                 "batch_size": args.batch_size},
                      "target": {"list_path": target_dset_path,
                                 "batch_size": args.batch_size},
                      "test": {"list_path": test_dset_path,
                               "batch_size": 4}}
    if "office" == config["dataset"]:
        config["loss"] = {"trade_off": 1.}
        if ("amazon" in s_dset_path and "webcam" in t_dset_path) or \
                ("webcam" in s_dset_path and "dslr" in t_dset_path) or \
                ("webcam" in s_dset_path and "amazon" in t_dset_path) or \
                ("dslr" in s_dset_path and "amazon" in t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        elif ("amazon" in s_dset_path and "dslr" in t_dset_path) or \
                ("dslr" in s_dset_path and "webcam" in t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.0003  # optimal parameters
        config["network"]["params"]["class_num"] = 31
    elif config["dataset"] == "image-clef":
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 12
    elif config["dataset"] == "visda":
        config["loss"] = {"trade_off": 0.1}
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 12
    elif config["dataset"] == "office-home":
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 65
    elif config['dataset'] == 'domain_net':
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 345
    elif config['dataset'] == 'domain_net_126':
        config["loss"] = {"trade_off": 0.1}
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 126
    else:
        raise ValueError('Dataset cannot be recognized. Please define your own dataset here.')

    config['ramp_up_trans'] = 10000

    if config['method'] == 'no_adaptation':
        pre_trainer = NoAdaptationPT(config)
    elif config['method'] == 'aada':
        pre_trainer = DomainAdversarialPT(config)
    elif config['method'] == 'tsf':
        pre_trainer = DomainAdversarialTransferPT(config)
    elif config['method'] == 'mme':
        pre_trainer = MiniMaxEntropyPT(config)
    elif config['method'] == 'tsf_mme':
        pre_trainer = DomainAdversarialTransferMMEPT(config)
    elif config['method'] == 'tsf_mme_':
        pre_trainer = DomainAdversarialTransferMMEPT_(config)


    pre_trainer.run()



