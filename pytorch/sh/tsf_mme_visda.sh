#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --mem=60g
#SBATCH --nodes=1
#SBATCH --cpus-per-task=20
#SBATCH --gres=gpu:1
#SBATCH --output=%j.stdout
#SBATCH --error=%j.stderr
#SBATCH --open-mode=append
#SBATCH --signal=B:SIGUSR1@120
#SBATCH --job-name=visda

module load anaconda3/2020.02/gcc-9.2.0
source activate DA_env

cd ..

m=tsf_mme
d=visda
t=visda

python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 0
python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 1
python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 2
python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 3
python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 4
python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 5
python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 6
python main_pretrainer.py --dset $d --tag $t --method $m --iter_pretrain 10000 --test_interval 5000 --machine fusion --seed 7