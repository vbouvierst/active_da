#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --mem=60g
#SBATCH --nodes=1
#SBATCH --cpus-per-task=20
#SBATCH --gres=gpu:1
#SBATCH --output=%j.stdout
#SBATCH --error=%j.stderr
#SBATCH --open-mode=append
#SBATCH --signal=B:SIGUSR1@120
#SBATCH --job-name=D_A_no_km

module load anaconda3/2020.02/gcc-9.2.0
source activate DA_env

cd ..

m=no_adaptation
d=office
q=kmeans
b=8
t=D_A

python main.py --dset $d --tag $t --method $m --query $q --budget $b --test_interval 250 --iter_round 2000 --rounds 10 --machine fusion --seed 4
python main.py --dset $d --tag $t --method $m --query $q --budget $b --test_interval 250 --iter_round 2000 --rounds 10 --machine fusion --seed 5
python main.py --dset $d --tag $t --method $m --query $q --budget $b --test_interval 250 --iter_round 2000 --rounds 10 --machine fusion --seed 6
python main.py --dset $d --tag $t --method $m --query $q --budget $b --test_interval 250 --iter_round 2000 --rounds 10 --machine fusion --seed 7