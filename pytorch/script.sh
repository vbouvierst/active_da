# Office31

##################################################################################################################################
## A-> W
##################################################################################################################################

### SAGE (diverse)

python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse


### Entropy
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy

### Random
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy random


### AADA
python train_image_active_balance.py --seed 0 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 1 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 2 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 3 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 4 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 5 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 6 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 7 --method DANN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True


##################################################################################################################################
## W->A
##################################################################################################################################

python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse


### Entropy
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy

### Random
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy random


### AADA
python train_image_active_balance.py --seed 0 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 1 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 2 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 3 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 4 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 5 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 6 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 7 --method DANN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True



##################################################################################################################################
## A->D
##################################################################################################################################

python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy SAGE_diverse


### Entropy
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy

### Random
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy random


### AADA
python train_image_active_balance.py --seed 0 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 1 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 2 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 3 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 4 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 5 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 6 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 7 --method DANN --dset office --tag A_D --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True



##################################################################################################################################
## D->A
##################################################################################################################################

python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse


### Entropy
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy

### Random
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy random


### AADA
python train_image_active_balance.py --seed 0 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 1 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 2 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 3 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 4 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 5 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 6 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 7 --method DANN --dset office --tag D_A --ratio_active 0.2 --requests 10 --policy entropy --weight_policy True





# VisDA

##################################################################################################################################
## VisDA (b=10)
##################################################################################################################################

## SAGE (diverse)
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse

## Entropy
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy


## Random
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy random


## AADA
python train_image_active_balance.py --seed 0 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 1 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 2 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 3 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 4 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 5 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 6 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 7 --method DANN --dset visda --tag visda --n_active 10 --requests 10 --policy entropy --weight_policy True


# VisDA

##################################################################################################################################
## VisDA (b=100)
##################################################################################################################################

## SAGE (diverse)
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse

## Entropy
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy


## Random
python train_image_active_inductive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random
python train_image_active_inductive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random
python train_image_active_inductive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random
python train_image_active_inductive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random
python train_image_active_inductive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random
python train_image_active_inductive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random
python train_image_active_inductive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random
python train_image_active_inductive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy random


## AADA
python train_image_active_balance.py --seed 0 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 1 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 2 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 3 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 4 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 5 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 6 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True
python train_image_active_balance.py --seed 7 --method DANN --dset visda --tag visda --n_active 100 --requests 10 --policy entropy --weight_policy True



# Ablation (Inductive VS BALANCE VS NAIVE)
## Balance
### AW
python train_image_active_balance.py --seed 0 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 1 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 2 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 3 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 4 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 5 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 6 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 7 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
### WA
python train_image_active_balance.py --seed 0 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 1 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 2 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 3 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 4 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 5 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 6 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 7 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
### VisDA (b=10)
python train_image_active_balance.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
### VisDA (b=100)
python train_image_active_balance.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_balance.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
## Naive
### AW
python train_image_active_naive.py --seed 0 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 1 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 2 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 3 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 4 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 5 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 6 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 7 --method Y_DAN --dset office --tag A_W --ratio_active 0.2 --requests 10 --policy SAGE_diverse
### WA
python train_image_active_naive.py --seed 0 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 1 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 2 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 3 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 4 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 5 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 6 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 7 --method Y_DAN --dset office --tag W_A --ratio_active 0.2 --requests 10 --policy SAGE_diverse
### VisDA (b=10)
python train_image_active_naive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 10 --requests 10 --policy SAGE_diverse
### VisDA (b=100)
python train_image_active_naive.py --seed 0 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 1 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 2 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 3 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 4 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 5 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 6 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse
python train_image_active_naive.py --seed 7 --method Y_DAN --dset visda --tag visda --n_active 100 --requests 10 --policy SAGE_diverse

