import argparse
import os
import pandas as pd
import pickle


LOG_CONFIG = [
    'method',
    'query',
    'lab_source_target',
    'budget',
    'rounds',
    'iter_round',
    'test_interval',
    'task',
    'dataset',
    'seed',
    'tag',
    'pretrained_path'
]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parser for snapshot")
    parser.add_argument("--task", default="W_A", type=str, help="task")
    parser.add_argument("--method", default="prototypes", type=str, help="method")

    args = parser.parse_args()

    task = args.task

    paths = os.listdir('snapshot/')

    results_df = pd.DataFrame(
        {arg: [] for arg in LOG_CONFIG+['accuracy', 'r', 'i']}
    )

    print(results_df)
    idx = 0
    for path in paths:
        if 'AL' in path:
            try:
                with open('snapshot/'+path+'/log_config', 'rb') as f:
                    log_config = pickle.load(f)
                print('log_config')

                with open('snapshot/'+path+'/log_accuracy', 'rb') as f:
                    log_accuracy = pickle.load(f)

                for r in log_accuracy.keys():
                    for i, acc in log_accuracy[r]:
                        print(r, i, acc)
                        results = dict(log_config)
                        results['r'] = r
                        results['i'] = i
                        results['accuracy'] = acc

                        results_df = pd.concat(
                            [results_df,
                             pd.DataFrame(results, index=[idx])], axis=0
                        )
                        idx += 1
            except:
                pass

    results_df.to_csv('snapshot/results.csv', sep=';')
    print(results_df)