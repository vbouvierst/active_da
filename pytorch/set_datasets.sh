#!/bin/bash
DATA_PATH=$1
CUR_PATH=`/bin/pwd`

# Create folders for datasets
cd $DATA_PATH
DATA_PATH=`/bin/pwd`
mkdir -p DA_datasets
cd DA_datasets
mkdir -p office-home
mkdir -p office
mkdir -p image-clef
mkdir -p visda-2017

# Google download
function gdrive_download () {
  CONFIRM=$(wget --quiet --save-cookies cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$1" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
  wget --load-cookies cookies.txt "https://docs.google.com/uc?export=download&confirm=$CONFIRM&id=$1" -O $2
  rm -f cookies.txt
}

# office
cd ../office

OFFH_FILEID="0B4IapRTv9pJ1WGZVd1VDMmhwdlE"
gdrive_download $OFFH_FILEID domain_adaptation_images.tar.gz

mkdir domain_adaptation_images
tar xf domain_adaptation_images.tar.gz -C domain_adaptation_images

# Visda-2017
cd ../visda-2017

wget http://csr.bu.edu/ftp/visda17/clf/train.tar
tar xf train.tar

wget http://csr.bu.edu/ftp/visda17/clf/validation.tar
tar xf validation.tar  

wget http://csr.bu.edu/ftp/visda17/clf/test.tar
tar xf test.tar

wget https://raw.githubusercontent.com/VisionLearningGroup/taskcv-2017-public/master/classification/data/image_list.txt

# Linking
ln -s $DATA_PATH"/DA_datasets" $CUR_PATH"/data"