import argparse
import os
import os.path as osp

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from network import *
from loss import *
from pre_process import *
from torch.utils.data import DataLoader
from lr_schedule import *
import data_list
from data_list import ImageList
from torch.autograd import Variable
import random
import pdb
import math
from tqdm import tqdm
from sklearn.metrics import confusion_matrix
import pickle

CUDA = True

import numpy as np


def score(data, mode):
    # Scoring target data
    if mode == 'random':
        # Random scoring
        return torch.rand(data['g'].shape[0])
    elif mode == 'entropy':
        # Entropy scoring A New Active Labeling Method for Deep Learning, Dan Wang and Yi Shang, IJCNN
        pred = data['g']
        entropy = (- pred * torch.log(pred)).sum(dim=1)
        return entropy
    elif mode == 'SAGE':
        # ||SAGE|| (without diversty)
        softmax = data['g'].reshape(data['g'].shape[0], 1, data['g'].shape[1])
        grad_ref = (softmax * data['grads']).sum(dim=2)
        grad_ref = (grad_ref / grad_ref.norm(dim=1, keepdim=True)).reshape(grad_ref.shape[0], grad_ref.shape[1], 1)
        scalar = (data['grads'] * grad_ref).sum(dim=1, keepdim=True)
        grad_proj = data['grads'] - scalar.abs() * grad_ref
        return (data['g'] * grad_proj.norm(dim=1) ** 2).sum(dim=1).sqrt()


def accuracy(data):
    prob, pred = torch.max(data['g'], 1)
    return (data['labels'].long() == pred).float()


def sort_score(config, data, mode):
    if mode == 'SAGE_diverse':
        args = h_kmeans_pp(data, k=config['n_active_select'], projection=config['projection'])
    else:
        s = score(data, mode)
        s *= (1. - data['actives'].float())

        if config['weight_policy']:
            s *= data['weights']
        args = list(np.argsort(s))[::-1]
    args = [int(a) for a in args]
    active = data['index'][args]
    active = [int(a) for a in active]
    return active


def select_samples(config, data):
    mode = config['policy']
    n_selec = config["n_active_select"]

    active = sort_score(config, data, mode)[:n_selec]
    active_ent = sort_score(config, data, mode='entropy')[:n_selec]

    print('Selected samples are', active)

    print('IoU vs entropy', len(set(active) & set(active_ent)) / len(set(active) | set(active_ent))) # Compare annotated samples wiht ones entropy may have chosen.
    print('purity', purity_score(active, data)) # Purity of selection, see Eq (3) and (4) of the paper
    print('purity entropy', purity_score(active_ent, data))
    return active


def purity_score(active, data_score):
    acc = []
    _, pred = torch.max(data_score['g'], 1)

    for a in active:
        _, i = (data_score['index'] == a).max(dim=0)
        acc.append((pred[i] == data_score['labels'][i]).reshape(-1))
    acc = torch.cat(acc)
    return acc.float().mean()


def scoring_target(config, loader, model, ad_net, test_10crop=True):
    # with torch.no_grad():
    if test_10crop:
        iter_active = [iter(loader['target'][i]) for i in range(10)]
        for i in range(len(loader['target'][0])):
            data = [iter_active[j].next() for j in range(10)]
            inputs = [data[j][0] for j in range(10)]
            labels_index = data[0][1]
            labels, active, index = labels_index[:, 0], labels_index[:, 1], labels_index[:, 2]
            for j in range(10):
                if CUDA:
                    inputs[j] = inputs[j].cuda()
            outputs = []
            features = []
            weights = []
            grads = []

            for j in range(10):
                feature, predict_out = model(inputs[j])
                features.append(feature)
                outputs.append(nn.Softmax(dim=1)(predict_out))
                weights.append(w_from_ad(config, feature, ad_net).squeeze())
                if config['compute_grad']:
                    grads.append(Y_DAN_grad([feature, nn.Softmax(dim=1)(predict_out)], ad_net))

            features = sum(features) / 10.
            outputs = sum(outputs) / 10.
            weights = sum(weights) / 10.
            
            if config['compute_grad']:
                grads = sum(grads) / 10.

            if i == 0:
                all_features = features.detach().cpu()
                all_outputs = outputs.detach().cpu()
                all_index = index.cpu()
                all_labels = labels.detach().cpu()
                all_actives = active.detach().cpu()
                all_weights = weights.detach().cpu()
                if config['compute_grad']:
                    all_grads = grads.detach().cpu()
            else:
                all_features = torch.cat([all_features, features.detach().cpu()], dim=0)
                all_outputs = torch.cat([all_outputs, outputs.detach().cpu()], dim=0)
                all_index = torch.cat([all_index, index.detach().cpu()], dim=0)
                all_labels = torch.cat([all_labels, labels.detach().cpu()], dim=0)
                all_actives = torch.cat([all_actives, active.detach().cpu()], dim=0)
                all_weights = torch.cat([all_weights, weights.detach().cpu()], dim=0)
                if config['compute_grad']:
                    all_grads = torch.cat([all_grads, grads.detach().cpu()], dim=0)

    else:
        iter_active = iter(loader['target'])
        for i in range(len(loader['target'])):
            if i > 2000: # For large datasets, might be interesting to subsample it for active annotation. You can remove this line if necessary.
                break
            inputs, labels_index = iter_active.next()
            labels, active, index = labels_index[:, 0], labels_index[:, 1], labels_index[:, 2]
            inputs = inputs.cuda()
            features, predict_out = model(inputs)
            outputs = nn.Softmax(dim=1)(predict_out)
            weights = w_from_ad(config, features, ad_net).squeeze()
            if config['compute_grad']:
                grads = Y_DAN_grad([features, nn.Softmax(dim=1)(predict_out)], ad_net)

            if i == 0:
                all_features = features.cpu().detach()
                all_outputs = outputs.cpu().detach()
                all_labels = labels.cpu().detach()
                all_index = index.cpu().detach()
                all_actives = active.cpu().detach()
                all_weights = weights.cpu().detach()
                if config['compute_grad']:
                    all_grads = grads.cpu().detach()
            else:
                all_features = torch.cat([all_features, features.cpu().detach()], dim=0)
                all_outputs = torch.cat([all_outputs, outputs.cpu().detach()], dim=0)
                all_labels = torch.cat([all_labels, labels.cpu().detach()], dim=0)
                all_index = torch.cat([all_index, index.cpu().detach()], dim=0)
                all_actives = torch.cat([all_actives, active.cpu().detach()], dim=0)
                all_weights = torch.cat([all_weights, weights.cpu().detach()], dim=0)
                if config['compute_grad']:
                    all_grads = torch.cat([all_grads, grads.cpu().detach()], dim=0)

                    # del inputs, features, outputs_s, outputs_t, weights, grads

    data_score = {'actives': all_actives,
                  'features': all_features,
                  'weights': all_weights,
                  'g': all_outputs,
                  'labels': all_labels,
                  'index': all_index
                  }

    if config['compute_grad']:
        data_score['grads'] = all_grads

    return data_score


def active_samples_selection(config, loader, model, test_10crop=True, ad_net=None):
    data_score = scoring_target(config, loader, model, test_10crop=test_10crop, ad_net=ad_net)

    selected = select_samples(config, data_score)

    with open(config['path']['target_path'], 'r') as f:
        lines_selec = f.readlines()

    with open(config['path']['target_path'], 'w') as f:
        for line in lines_selec:
            line_split = line.split()
            index = eval(line_split[-1])
            if index in selected:
                line_split[-2] = '1'

            new_line = ' '.join(line_split) + '\n'

            if index in selected:
                f.write(new_line)
            else:
                f.write(new_line)

    with open(config['path']['target_path'], 'r') as f:
        lines_selec = f.readlines()

    with open(config['path']['active_path'], 'w') as f:
        for line in lines_selec:
            line_split = line.split()
            active = eval(line_split[-2])
            if active:
                new_line = ' '.join(line_split[:2]) + '\n'
                for _ in range(10):  # duplicate samples for faster loading
                    f.write(new_line)


def image_classification_test(loader, model, test_10crop=True):
    start_test = True
    with torch.no_grad():
        if test_10crop:
            iter_test = [iter(loader['test'][i]) for i in range(10)]
            for i in range(len(loader['test'][0])):
                data = [iter_test[j].next() for j in range(10)]
                inputs = [data[j][0] for j in range(10)]
                labels = data[0][1]
                for j in range(10):
                    if CUDA:
                        inputs[j] = inputs[j].cuda()
                labels = labels

                features = []
                outputs = []
                outputs = []
                for j in range(10):
                    feature, predict_out = model(inputs[j])
                    features.append(feature)
                    outputs.append(nn.Softmax(dim=1)(predict_out))
                outputs = sum(outputs) / 10.
                features = sum(features) / 10.

                if start_test:
                    all_features = features.float().cpu()
                    all_output = outputs.float().cpu()
                    all_label = labels.float()
                    start_test = False
                else:
                    all_features = torch.cat([all_features, features.cpu()], dim=0)
                    all_output = torch.cat((all_output, outputs.float().cpu()), 0)
                    all_label = torch.cat((all_label, labels.float()), 0)
            del iter_test, data, inputs, labels
        else:
            iter_test = iter(loader["test"])
            for i in range(len(loader['test'])):
                data = iter_test.next()
                inputs = data[0]
                labels = data[1]
                if CUDA:
                    inputs = inputs.cuda()
                features, outputs = model(inputs)
                if start_test:
                    all_features = features.float().cpu()
                    all_output = outputs.float().cpu()
                    all_output = outputs.float().cpu()
                    all_label = labels.float()
                    start_test = False
                else:
                    all_features = torch.cat([all_features, features.cpu()], dim=0)
                    all_output = torch.cat((all_output, outputs.float().cpu()), 0)
                    all_label = torch.cat((all_label, labels.float()), 0)

    _, predict = torch.max(all_output, 1)

    accuracy = torch.sum(torch.squeeze(predict).float() == all_label).item() / float(all_label.size()[0])
    del predict, all_output, all_label

    return accuracy


def train(config):
    ## set pre-process
    prep_dict = {}
    prep_config = config["prep"]
    prep_dict["source"] = image_train(**config["prep"]['params'])
    prep_dict["target"] = image_train(**config["prep"]['params'])
    if prep_config["test_10crop"]:
        prep_dict["test"] = image_test_10crop(**config["prep"]['params'])
    else:
        prep_dict["test"] = image_test(**config["prep"]['params'])

    ## prepare data
    dsets = {}
    dset_loaders = {}
    data_config = config["data"]
    train_bs = data_config["source"]["batch_size"]
    test_bs = data_config["test"]["batch_size"]

    dsets["source"] = ImageList(open(data_config["source"]["list_path"]).readlines(), transform=prep_dict["source"])
    dset_loaders["source"] = DataLoader(dsets["source"], batch_size=train_bs, shuffle=True, num_workers=4,
                                        drop_last=True)

    dsets["target"] = ImageList(open(data_config["target"]["list_path"]).readlines(), transform=prep_dict["target"])
    dset_loaders["target"] = DataLoader(dsets["target"], batch_size=train_bs, shuffle=True, num_workers=4,
                                        drop_last=True)

    if prep_config["test_10crop"]:
        for i in range(10):
            dsets["test"] = [ImageList(open(data_config["test"]["list_path"]).readlines(), \
                                       transform=prep_dict["test"][i]) for i in range(10)]
            dset_loaders["test"] = [DataLoader(dset, batch_size=test_bs, \
                                               shuffle=False, num_workers=4) for dset in dsets['test']]

    else:
        dsets["test"] = ImageList(open(data_config["test"]["list_path"]).readlines(), \
                                  transform=prep_dict["test"])
        dset_loaders["test"] = DataLoader(dsets["test"], batch_size=test_bs, \
                                          shuffle=False, num_workers=4)

    class_num = config["network"]["params"]["class_num"]

    ## set base network
    net_config = config["network"]
    base_network = net_config["name"](**net_config["params"])

    if CUDA:
        base_network = base_network.cuda()

    ## add additional network for some methods

    if config['method'] == 'DANN':
        print('Method is DANN')
        ad_net = AdversarialNetwork(base_network.output_num(), 1024, max_iter=config['ramp_up_trans'])
    elif config['method'] == 'Y_DAN':
        ad_net = AdversarialNetwork(base_network.output_num(), 1024,
                                    output_dim=config["network"]["params"]["class_num"],
                                    max_iter=config['ramp_up_trans'])
    else:
        raise ValueError('Method cannot be recognized.')

    if CUDA:
        ad_net = ad_net.cuda()

    parameter_list = base_network.get_parameters() + ad_net.get_parameters()

    ## set optimizer
    optimizer_config = config["optimizer"]
    
    optimizer = optimizer_config["type"](parameter_list, **(optimizer_config["optim_params"]))
    
    optimizer_active = optimizer_config["type"](base_network.get_parameters(), **(optimizer_config["optim_params"]))

    param_lr = []
    for param_group in optimizer.param_groups:
        param_lr.append(param_group["lr"])
    schedule_param = optimizer_config["lr_param"]
    lr_scheduler = schedule_dict[optimizer_config["lr_type"]]

    gpus = config['gpu'].split(',')
    if len(gpus) > 1:
        ad_net = nn.DataParallel(ad_net, device_ids=[int(i) for i in gpus])
        base_network = nn.DataParallel(base_network, device_ids=[int(i) for i in gpus])

    best_acc = 0.0
    number_of_requests = 0
    iter_request = 0
    last_request = False
    count_last_request = 0

    for i in tqdm(range(config["num_iterations"])):
        iter_request += 1
        # Last request for early stopping
        if last_request:
            count_last_request += 1
            if count_last_request > config['iter_last_request']:
                print('Training stop due to max number of iterations reached')
                break

        base_network.train(False)
        ad_net.train(False)

        # Selecting samples
        if ((i % config["max_iter_request"]) == config["max_iter_request"] - 1) & (i > config['min_iter_request'] - 1):
            print('Selecting samples')

            if prep_config["test_10crop"]:
                dsets["target"] = [ImageList(open(data_config["target"]["list_path"]).readlines(), \
                                             transform=prep_dict["test"][i]) for i in range(10)]
                dset_loaders["target"] = [DataLoader(dset, batch_size=test_bs,
                                                     shuffle=False, num_workers=4) for dset in dsets['target']]

            else:
                dsets["target"] = ImageList(open(data_config["target"]["list_path"]).readlines(), \
                                            transform=prep_dict["test"])
                dset_loaders["target"] = DataLoader(dsets["target"], batch_size=test_bs, \
                                                    shuffle=True, num_workers=4)

            if number_of_requests < config['requests']:
                active_samples_selection(config, dset_loaders, base_network,
                                         test_10crop=prep_config["test_10crop"], ad_net=ad_net)
                number_of_requests += 1
            else:
                last_request = True

            # Reload datasets
            dsets["target"] = ImageList(open(data_config["target"]["list_path"]).readlines(),
                                        transform=prep_dict["target"])
            dset_loaders["target"] = DataLoader(dsets["target"], batch_size=train_bs, shuffle=True, num_workers=4,
                                                drop_last=True)

            dsets["active"] = ImageList(open(data_config["active"]["list_path"]).readlines(),
                                        transform=prep_dict["target"])
            dset_loaders["active"] = DataLoader(dsets["active"], batch_size=train_bs, shuffle=True, num_workers=4,
                                                drop_last=True)

        # Testing
        if i > config['start_test']:
            if i % config["test_interval"] == config["test_interval"] - 1:
                print('test10', prep_config["test_10crop"])
                temp_acc = image_classification_test(dset_loaders, base_network, test_10crop=prep_config["test_10crop"])

                log_str = "iter: {:05d}, accuracy classifier: {:.5f}".format(i, temp_acc)
                log_str += "    requests: {:02d}".format(number_of_requests)

                config["out_file"].write(log_str + "\n")
                config["out_file"].flush()
                print(log_str)

                if temp_acc == 1.:
                    break

        loss_params = config["loss"]

        ## train one iter
        base_network.train(True)
        ad_net.train(True)

        optimizer = lr_scheduler(optimizer, i, **schedule_param)
        optimizer.zero_grad()

        try:
            inputs_source, labels_source = iter_source.next()
        except:
            iter_source = iter(dset_loaders["source"])
            inputs_source, labels_source = iter_source.next()
        try:
            inputs_target, labels_active = iter_target.next()
            labels_target, active = labels_active[:, 0], labels_active[:, 1]
        except:
            iter_target = iter(dset_loaders["target"])
            inputs_target, labels_active = iter_target.next()
            labels_target, active = labels_active[:, 0], labels_active[:, 1]

        if CUDA:
            inputs_source, inputs_target = inputs_source.cuda(), inputs_target.cuda()
            labels_source, labels_target = labels_source.cuda(), labels_target.cuda()
            active = active.cuda().float()

        features_source, outputs_source = base_network(inputs_source)
        features_target, outputs_target = base_network(inputs_target)

        features = torch.cat((features_source, features_target), dim=0)

        softmax_out_s = torch.zeros_like(outputs_source)
        softmax_out_s[torch.arange(outputs_source.size(0)), labels_source] = 1.0

        labels_out_t = torch.zeros_like(outputs_target)
        labels_out_t[torch.arange(outputs_target.size(0)), labels_target] = 1.0
        softmax_out_t = active.view(-1, 1) * labels_out_t + (1. - active).view(-1, 1) * nn.Softmax(dim=1)(
            outputs_target)
        softmax_out = torch.cat([softmax_out_s, softmax_out_t], dim=0)

        if config['method'] == 'DANN':
            transfer_loss = DANN(features, ad_net, hook=True)
        elif config['method'] == 'Y_DAN':
            transfer_loss = Y_DAN([features, softmax_out], ad_net)
        else:
            raise ValueError('Method cannot be recognized.')


        classifier_loss = nn.CrossEntropyLoss()(outputs_source, labels_source)
        (classifier_loss + loss_params["trade_off"] * transfer_loss).backward()
        optimizer.step()
    return best_acc

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Active Domain Adaptation')
    parser.add_argument('--method', type=str, default='Y_DAN', choices=['DANN', 'Y_DAN'], help='Transfer method')
    parser.add_argument('--gpu_id', type=str, nargs='?', default='0', help="device id to run")
    parser.add_argument('--net', type=str, default='ResNet50',
                        choices=["ResNet18", "ResNet34", "ResNet50", "ResNet101", "ResNet152"])
    parser.add_argument('--dset', type=str, default='office', help="The dataset or source dataset used")
    parser.add_argument('--tag', type=str, default='A_W', help="Tag of the experiments for saving")
    parser.add_argument('--test_interval', type=int, default=1000, help="interval of two continuous test phase")
    parser.add_argument('--ratio_active', type=float, default=0.2, help="annotation budget of Office31")
    parser.add_argument('--n_active', type=int, default=100, help="annotation budget for VisDA")
    parser.add_argument('--requests', type=int, default=10, help="number of requests")
    parser.add_argument('--min_iter_request', type=int, default=9000,
                        help="Number min of iterations between two requests")
    parser.add_argument('--max_iter_request', type=int, default=5000,
                        help="Number max of iterations between two requests")
    parser.add_argument('--iter_last_request', type=int, default=10000, help="Number of iterations after last request")
    parser.add_argument('--policy', type=str, default='SAGE_diverse', help="sample selection policy",
                        choices=['random', 'entropy', 'SAGE', 'SAGE_diverse'])
    parser.add_argument('--projection', type=bool, default=True, help="Projection")
    parser.add_argument('--weight_policy', type=bool, default=False, help="weight policy")
    parser.add_argument('--lr', type=float, default=0.001, help="learning rate")
    parser.add_argument('--seed', type=int, default=0, help="seed")
    parser.add_argument('--batch_size', type=int, default=32, help="batch_size")
    parser.add_argument('--machine', type=str, default='fusion', help="machine", choices=['monstre', 'fusion'])
    parser.add_argument('--max_iterations', type=int, default=111000, help="Number of iterations")

    args = parser.parse_args()

    #if args.machine != 'fusion':
    #os.environ["CUDA_VISIBLE_DEVICES"] = '3'

    #os.environ['SLURM_STEP_GPUS'] = '3'


    # Fix cuda, torch & numpy random seeds
    torch.backends.cudnn.benchmark = True
    torch.manual_seed(args.seed)
    np.random.seed(args.seed)

    # train config
    config = {}
    config['method'] = args.method

    config['start_test'] = 1
    config["policy"] = args.policy
    config["weight_policy"] = args.weight_policy
    print('Policy is', config['policy'])
    config['requests'] = args.requests
    config['min_iter_request'] = args.min_iter_request
    config['max_iter_request'] = args.max_iter_request
    config['iter_last_request'] = args.iter_last_request
    config["ratio_active"] = args.ratio_active
    config['ramp_up_trans'] = 10000
    config['compute_grad'] = 'SAGE' in config['policy']
    config['projection'] = args.projection

    config['tag'] = 'ActINDUCTIVE_' + str(config['weight_policy']) + '_' \
                    + config['method'] + '_' + config['policy'] + '_' + str(config['ratio_active']) + '_' \
                    + str(config['requests']) + '_' + args.tag + '_' + str(args.seed)

    if args.dset == 'visda':
        config["n_active_select"] = args.n_active
        config['tag'] = 'ActINDUCTIVE_' + str(config['weight_policy']) + '_' \
                        + config['method'] + '_' + config['policy'] + '_' + str(config['n_active_select']) + '_' \
                        + str(config['requests']) + '_' + args.tag + '_' + str(args.seed)
    print(config['tag'])

    if args.dset == 'office':
        config['tag'] += '_standard'
        if args.tag == 'A_W':
            s_dset_path = '../data/office/amazon_list_local.txt'
            t_dset_path = '../data/office/webcam_list_local.txt'

        elif args.tag == 'W_A':
            s_dset_path = '../data/office/webcam_list_local.txt'
            t_dset_path = '../data/office/amazon_list_local.txt'

        elif args.tag == 'A_D':
            s_dset_path = '../data/office/amazon_list_local.txt'
            t_dset_path = '../data/office/dslr_list_local.txt'

        elif args.tag == 'D_A':
            s_dset_path = '../data/office/dslr_list_local.txt'
            t_dset_path = '../data/office/amazon_list_local.txt'

        elif args.tag == 'D_W':
            s_dset_path = '../data/office/dslr_list_local.txt'
            t_dset_path = '../data/office/webcam_list_local.txt'

        elif args.tag == 'W_D':
            s_dset_path = '../data/office/webcam_list_local.txt'
            t_dset_path = '../data/office/dslr_list_local.txt'

    elif args.dset == 'office-home':
        config['tag'] += '_standard'
        if args.tag == 'A_C':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Clipart_local.txt'
        elif args.tag == 'A_P':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'
        elif args.tag == 'A_P':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'
        elif args.tag == 'A_R':
            s_dset_path = '../data/office-home/Art_local.txt'
            t_dset_path = '../data/office-home/Real_World_local.txt'
        elif args.tag == 'C_A':
            s_dset_path = '../data/office-home/Clipart_local.txt'
            t_dset_path = '../data/office-home/Art_local.txt'
        elif args.tag == 'C_P':
            s_dset_path = '../data/office-home/Clipart_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'
        elif args.tag == 'C_R':
            s_dset_path = '../data/office-home/Clipart_local.txt'
            t_dset_path = '../data/office-home/Real_World_local.txt'
        elif args.tag == 'P_A':
            s_dset_path = '../data/office-home/Product_local.txt'
            t_dset_path = '../data/office-home/Art_local.txt'
        elif args.tag == 'P_C':
            s_dset_path = '../data/office-home/Product_local.txt'
            t_dset_path = '../data/office-home/Clipart_local.txt'
        elif args.tag == 'P_R':
            s_dset_path = '../data/office-home/Product_local.txt'
            t_dset_path = '../data/office-home/Real_World_local.txt'
        elif args.tag == 'R_A':
            s_dset_path = '../data/office-home/Real_World_local.txt'
            t_dset_path = '../data/office-home/Art_local.txt'
        elif args.tag == 'R_C':
            s_dset_path = '../data/office-home/Real_World_local.txt'
            t_dset_path = '../data/office-home/Clipart_local.txt'
        elif args.tag == 'R_P':
            s_dset_path = '../data/office-home/Real_World_local.txt'
            t_dset_path = '../data/office-home/Product_local.txt'

    elif args.dset == 'visda':
        config['tag'] += '_standard'
        s_dset_path = '../data/visda-2017/train_list_local.txt'
        t_dset_path = '../data/visda-2017/validation_list_local.txt'

    test_dset_path = t_dset_path[:-4] + '_test.txt'
    t_dset_path = t_dset_path[:-4] + '_train.txt'

    config["output_path"] = 'snapshot/' + config['tag']

    if not osp.exists(config["output_path"]):
        os.system('mkdir -p ' + config["output_path"])
    config["out_file"] = open(osp.join(config["output_path"], "log.txt"), "w")
    config["path_target_prediction"] = osp.join(config["output_path"], "target_prediction")
    if not osp.exists(config["output_path"]):
        os.mkdir(config["output_path"])

    target_dset_path = os.path.join(config["output_path"], 'target.txt')
    active_dset_path = os.path.join(config["output_path"], 'active.txt')

    config['history_path'] = os.path.join(config["output_path"], 'history')

    with open(config['history_path'], 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump([], f)

    if args.machine == 'fusion':
        s_dset_path = s_dset_path[:-4] + '_fusion.txt'
        t_dset_path = t_dset_path[:-4] + '_fusion.txt'
        test_dset_path = test_dset_path[:-4] + '_fusion.txt'

    with open(target_dset_path, 'w') as f_at:
        with open(t_dset_path, 'r') as f:
            index = 0
            for line in f:
                active = '0'
                f_at.write(line[:-1] + ' ' + active + ' ' + str(index) + '\n')
                index += 1

    with open(active_dset_path, 'w') as f:
        pass

    config['path'] = {'source_path': s_dset_path,
                      'target_path': target_dset_path,
                      'test_path': test_dset_path,
                      'active_path': active_dset_path}

    with open(target_dset_path, 'r') as f:
        n_active = len(f.readlines())

    if args.dset != 'visda':
        config["n_active_select"] = int(n_active * config['ratio_active'] / config['requests'])

    config["gpu"] = args.gpu_id
    config["num_iterations"] = args.max_iterations 
    config["num_baseline_iterations"] = 10004
    config["test_interval"] = args.test_interval
    config["output_for_test"] = True

    config["prep"] = {"test_10crop": True, 'params': {"resize_size": 256, "crop_size": 224, 'alexnet': False}}

    if args.dset == 'visda':
        config['prep']['test_10crop'] = False

    config["loss"] = {"trade_off": 1.}
    
    config["network"] = {"name": ResNetFc, "params": {"resnet_name": args.net,
                                                      "use_bottleneck": True,
                                                      "bottleneck_dim": 256,
                                                      "new_cls": True}}

    config["optimizer"] = {"type": optim.SGD, "optim_params": {'lr': args.lr, "momentum": 0.9, \
                                                               "weight_decay": 0.0005, "nesterov": True},
                           "lr_type": "inv", \
                           "lr_param": {"lr": args.lr, "gamma": 0.001, "power": 0.75}}

    config["dataset"] = args.dset
    config["data"] = {"source": {"list_path": s_dset_path,
                                 "batch_size": args.batch_size},
                      "target": {"list_path": target_dset_path,
                                 "batch_size": args.batch_size},
                      "active": {"list_path": active_dset_path,
                                 "batch_size": 2},
                      "test": {"list_path": test_dset_path,
                               "batch_size": 4}}

    if "office" == config["dataset"]:
        if ("amazon" in s_dset_path and "webcam" in t_dset_path) or \
                ("webcam" in s_dset_path and "dslr" in t_dset_path) or \
                ("webcam" in s_dset_path and "amazon" in t_dset_path) or \
                ("dslr" in s_dset_path and "amazon" in t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        elif ("amazon" in s_dset_path and "dslr" in t_dset_path) or \
                ("dslr" in s_dset_path and "webcam" in t_dset_path):
            config["optimizer"]["lr_param"]["lr"] = 0.0003  # optimal parameters
        config["network"]["params"]["class_num"] = 31
    elif config["dataset"] == "image-clef":
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 12
    elif config["dataset"] == "visda":
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 12
        config['loss']["trade_off"] = 1.0
    elif config["dataset"] == "office-home":
        config["optimizer"]["lr_param"]["lr"] = 0.001  # optimal parameters
        config["network"]["params"]["class_num"] = 65
    else:
        raise ValueError('Dataset cannot be recognized. Please define your own dataset here.')
    config["out_file"].write(str(config))
    config["out_file"].flush()

    train(config)
    #train_baseline(config)
