# TODO 

- [X] MME
- [X] check memory with multiple batch (should not lead to batch smaller than 32)

- [X] Add mode source_u_target and source + target
    - [X] check with the variable self.current_round
    - [X] check when the batch size is to small (eventually duplicates samples in active)
- [ ] Run AADA
- [ ] Implement prototypes
    - [ ] prototypes classifier
    - [ ] entropy mixing 
    - [ ] add entropy regularization  with prototypes
- [ ] For experiments
    - [X] add logger
    - [X] add parser
    - [X] set sh files
    - [ ] set experimental setup
- [ ] Implement BADGE
- [X] Implement a .predict() method
- [X] pretraining / loading pretrained models
- [ ] experiment on AW for checking
    - [ ] AADA 
    - [ ] SAGE
    
- [X] check rampup

- [ ] SAGE
 - [ ] track gradient in active learner
 - [ ] compute ortho proj 
 
 