{'method': 'Y_DAN', 'start_test': 1, 'policy': 'random', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': False, 'projection': True, 'tag': 'ActFT_False_Y_DAN_random_10_10_visda_7_standard', 'n_active_select': 10, 'output_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_7_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.06255    target classifier: 0.57395    beta: 0.45506    kappa: 0.03955    rho: 26.78137    requests: 00
iter: 01999, precision source classifier: 0.03621    target classifier: 0.62518    beta: 0.38953    kappa: 0.00950    rho: -11.18636    requests: 00
iter: 02999, precision source classifier: 0.09088    target classifier: 0.64582    beta: 0.39026    kappa: 0.10101    rho: -20.03416    requests: 00
iter: 03999, precision source classifier: 0.12138    target classifier: 0.63659    beta: 0.41429    kappa: 0.10789    rho: -19.19373    requests: 00
iter: 04999, precision source classifier: 0.10608    target classifier: 0.65759    beta: 0.38374    kappa: 0.09676    rho: -10.19885    requests: 00
iter: 05999, precision source classifier: 0.07667    target classifier: 0.67361    beta: 0.35419    kappa: 0.06870    rho: -5.29554    requests: 00
iter: 06999, precision source classifier: 0.08382    target classifier: 0.65523    beta: 0.37699    kappa: 0.06218    rho: -8.53358    requests: 00
iter: 07999, precision source classifier: 0.08726    target classifier: 0.65523    beta: 0.37841    kappa: 0.07087    rho: -3.93250    requests: 00
iter: 08999, precision source classifier: 0.07341    target classifier: 0.65632    beta: 0.37159    kappa: 0.05449    rho: -1.12460    requests: 00
iter: 09999, precision source classifier: 0.08010    target classifier: 0.65759    beta: 0.37291    kappa: 0.06517    rho: 4.84880    requests: 01
iter: 10999, precision source classifier: 0.07811    target classifier: 0.67143    beta: 0.35710    kappa: 0.06816    rho: 3.39036    requests: 01
iter: 11999, precision source classifier: 0.09549    target classifier: 0.67098    beta: 0.36446    kappa: 0.08689    rho: -2.99755    requests: 01
iter: 12999, precision source classifier: 0.12971    target classifier: 0.67822    beta: 0.37046    kappa: 0.13812    rho: 1.74770    requests: 01
iter: 13999, precision source classifier: 0.12844    target classifier: 0.68338    beta: 0.36401    kappa: 0.13125    rho: -3.36316    requests: 01
iter: 14999, precision source classifier: 0.11477    target classifier: 0.68320    beta: 0.35860    kappa: 0.12808    rho: 1.80657    requests: 02
iter: 15999, precision source classifier: 0.11061    target classifier: 0.68800    beta: 0.35153    kappa: 0.11776    rho: 3.81260    requests: 02
iter: 16999, precision source classifier: 0.12337    target classifier: 0.68429    beta: 0.36087    kappa: 0.12473    rho: 2.97505    requests: 02
iter: 17999, precision source classifier: 0.12491    target classifier: 0.67171    beta: 0.37587    kappa: 0.12364    rho: 5.87472    requests: 02
iter: 18999, precision source classifier: 0.10382    target classifier: 0.69071    beta: 0.34585    kappa: 0.10301    rho: 9.50617    requests: 02
iter: 19999, precision source classifier: 0.07938    target classifier: 0.68193    beta: 0.34620    kappa: 0.07839    rho: 10.84905    requests: 03
iter: 20999, precision source classifier: 0.08762    target classifier: 0.69098    beta: 0.33941    kappa: 0.09232    rho: 13.26300    requests: 03
iter: 21999, precision source classifier: 0.08092    target classifier: 0.70167    beta: 0.32534    kappa: 0.08581    rho: 12.00035    requests: 03
iter: 22999, precision source classifier: 0.06200    target classifier: 0.69515    beta: 0.32572    kappa: 0.06825    rho: 13.69288    requests: 03
iter: 23999, precision source classifier: 0.04544    target classifier: 0.70755    beta: 0.30710    kappa: 0.05395    rho: 9.70451    requests: 03
iter: 24999, precision source classifier: 0.05838    target classifier: 0.68718    beta: 0.33292    kappa: 0.08273    rho: 12.19224    requests: 04
iter: 25999, precision source classifier: 0.06209    target classifier: 0.69832    beta: 0.32238    kappa: 0.09957    rho: 12.01005    requests: 04
iter: 26999, precision source classifier: 0.06227    target classifier: 0.70927    beta: 0.31077    kappa: 0.09296    rho: 11.41124    requests: 04
iter: 27999, precision source classifier: 0.07133    target classifier: 0.70999    beta: 0.31302    kappa: 0.10681    rho: 11.42081    requests: 04
iter: 28999, precision source classifier: 0.05449    target classifier: 0.70755    beta: 0.31003    kappa: 0.08047    rho: 9.41450    requests: 04
iter: 29999, precision source classifier: 0.07350    target classifier: 0.70981    beta: 0.31395    kappa: 0.10699    rho: 9.81634    requests: 05
iter: 30999, precision source classifier: 0.07151    target classifier: 0.70103    beta: 0.32272    kappa: 0.11061    rho: 10.46499    requests: 05
iter: 31999, precision source classifier: 0.05992    target classifier: 0.70167    beta: 0.31808    kappa: 0.10101    rho: 9.67905    requests: 05
iter: 32999, precision source classifier: 0.05340    target classifier: 0.71597    beta: 0.30080    kappa: 0.08454    rho: 6.27773    requests: 05
iter: 33999, precision source classifier: 0.04942    target classifier: 0.70637    beta: 0.30962    kappa: 0.07458    rho: 4.25181    requests: 05
iter: 34999, precision source classifier: 0.03883    target classifier: 0.70049    beta: 0.31233    kappa: 0.06273    rho: 4.02071    requests: 06
iter: 35999, precision source classifier: 0.03412    target classifier: 0.71253    beta: 0.29836    kappa: 0.05702    rho: 5.85310    requests: 06
iter: 36999, precision source classifier: 0.02915    target classifier: 0.71325    beta: 0.29608    kappa: 0.04743    rho: 5.36759    requests: 06
iter: 37999, precision source classifier: 0.02842    target classifier: 0.71941    beta: 0.28953    kappa: 0.03983    rho: 2.62312    requests: 06
iter: 38999, precision source classifier: 0.02589    target classifier: 0.71913    beta: 0.28906    kappa: 0.03802    rho: 4.83957    requests: 06
iter: 39999, precision source classifier: 0.02824    target classifier: 0.71026    beta: 0.29888    kappa: 0.04318    rho: 2.95792    requests: 07
iter: 40999, precision source classifier: 0.02960    target classifier: 0.72167    beta: 0.28755    kappa: 0.04471    rho: 2.37275    requests: 07
iter: 41999, precision source classifier: 0.02371    target classifier: 0.71732    beta: 0.29027    kappa: 0.03096    rho: 4.00941    requests: 07
iter: 42999, precision source classifier: 0.03259    target classifier: 0.72375    beta: 0.28629    kappa: 0.04191    rho: 4.20011    requests: 07
iter: 43999, precision source classifier: 0.04526    target classifier: 0.73208    beta: 0.28137    kappa: 0.05358    rho: 4.12867    requests: 07
iter: 44999, precision source classifier: 0.05259    target classifier: 0.72846    beta: 0.28737    kappa: 0.06200    rho: 5.55398    requests: 08
iter: 45999, precision source classifier: 0.06544    target classifier: 0.72927    beta: 0.29044    kappa: 0.07413    rho: 1.71031    requests: 08
iter: 46999, precision source classifier: 0.06517    target classifier: 0.72257    beta: 0.29752    kappa: 0.07323    rho: 1.66248    requests: 08
iter: 47999, precision source classifier: 0.06526    target classifier: 0.72864    beta: 0.29107    kappa: 0.08010    rho: 2.73371    requests: 08
iter: 48999, precision source classifier: 0.06553    target classifier: 0.72040    beta: 0.29995    kappa: 0.07839    rho: 2.30326    requests: 08
iter: 49999, precision source classifier: 0.06300    target classifier: 0.73208    beta: 0.28670    kappa: 0.07621    rho: 1.69355    requests: 09
iter: 50999, precision source classifier: 0.05539    target classifier: 0.72348    beta: 0.29348    kappa: 0.06164    rho: 0.54347    requests: 09
iter: 51999, precision source classifier: 0.05141    target classifier: 0.73190    beta: 0.28339    kappa: 0.05702    rho: 1.47252    requests: 09
iter: 52999, precision source classifier: 0.03937    target classifier: 0.73172    beta: 0.28003    kappa: 0.04417    rho: 1.85632    requests: 09
iter: 53999, precision source classifier: 0.04327    target classifier: 0.73190    beta: 0.28098    kappa: 0.05024    rho: 0.98816    requests: 09
iter: 54999, precision source classifier: 0.03467    target classifier: 0.73425    beta: 0.27604    kappa: 0.03883    rho: 0.92431    requests: 10
iter: 55999, precision source classifier: 0.03277    target classifier: 0.72158    beta: 0.28859    kappa: 0.03738    rho: 0.99378    requests: 10
iter: 56999, precision source classifier: 0.03856    target classifier: 0.72683    beta: 0.28487    kappa: 0.04462    rho: 1.41654    requests: 10
iter: 57999, precision source classifier: 0.03385    target classifier: 0.72538    beta: 0.28498    kappa: 0.04064    rho: 1.50701    requests: 10
iter: 58999, precision source classifier: 0.02887    target classifier: 0.72022    beta: 0.28883    kappa: 0.03720    rho: 1.15037    requests: 10
iter: 59999, precision source classifier: 0.03494    target classifier: 0.72384    beta: 0.28690    kappa: 0.04254    rho: 1.88867    requests: 10
iter: 60999, precision source classifier: 0.03820    target classifier: 0.73172    beta: 0.27969    kappa: 0.05078    rho: 1.88201    requests: 10
iter: 61999, precision source classifier: 0.04535    target classifier: 0.72248    beta: 0.29144    kappa: 0.05603    rho: 1.52775    requests: 10
iter: 62999, precision source classifier: 0.04680    target classifier: 0.72991    beta: 0.28410    kappa: 0.05386    rho: 0.65164    requests: 10
iter: 63999, precision source classifier: 0.04526    target classifier: 0.72239    beta: 0.29151    kappa: 0.04969    rho: 0.88124    requests: 10
iter: 64999, precision source classifier: 0.04716    target classifier: 0.72402    beta: 0.29038    kappa: 0.05005    rho: 0.83788    requests: 10
iter: 65999, precision source classifier: 0.04770    target classifier: 0.71515    beta: 0.29985    kappa: 0.05105    rho: 1.07019    requests: 10
iter: 66999, precision source classifier: 0.04933    target classifier: 0.72058    beta: 0.29466    kappa: 0.04345    rho: 0.89793    requests: 10
iter: 67999, precision source classifier: 0.04861    target classifier: 0.71063    beta: 0.30489    kappa: 0.04435    rho: 1.45542    requests: 10
iter: 68999, precision source classifier: 0.04707    target classifier: 0.72004    beta: 0.29453    kappa: 0.04372    rho: 1.71384    requests: 10
iter: 69999, precision source classifier: 0.04499    target classifier: 0.72113    beta: 0.29275    kappa: 0.03657    rho: 1.19047    requests: 10
iter: 00999, precision source classifier: 0.07214    target classifier: 0.08364
iter: 01999, precision source classifier: 0.18592    target classifier: 0.09748
iter: 02999, precision source classifier: 0.16917    target classifier: 0.06897
iter: 03999, precision source classifier: 0.07730    target classifier: 0.16709
iter: 04999, precision source classifier: 0.06942    target classifier: 0.18718
iter: 05999, precision source classifier: 0.06562    target classifier: 0.18610
iter: 06999, precision source classifier: 0.14853    target classifier: 0.07866
iter: 07999, precision source classifier: 0.06988    target classifier: 0.18202
iter: 08999, precision source classifier: 0.06843    target classifier: 0.17804
iter: 09999, precision source classifier: 0.07639    target classifier: 0.17668
