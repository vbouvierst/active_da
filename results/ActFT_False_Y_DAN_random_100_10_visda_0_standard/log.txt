{'method': 'Y_DAN', 'start_test': 1, 'policy': 'random', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': False, 'projection': True, 'tag': 'ActFT_False_Y_DAN_random_100_10_visda_0_standard', 'n_active_select': 100, 'output_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_0_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.08264    target classifier: 0.60943    beta: 0.42638    kappa: 0.10110    rho: 9.84139    requests: 00
iter: 01999, precision source classifier: 0.15985    target classifier: 0.63622    beta: 0.43366    kappa: 0.19379    rho: 23.76102    requests: 00
iter: 02999, precision source classifier: 0.08020    target classifier: 0.64718    beta: 0.38426    kappa: 0.07096    rho: 21.89921    requests: 00
iter: 03999, precision source classifier: 0.06924    target classifier: 0.68094    beta: 0.34350    kappa: 0.07920    rho: 36.97039    requests: 00
iter: 04999, precision source classifier: 0.03829    target classifier: 0.67524    beta: 0.33838    kappa: 0.04499    rho: 33.31332    requests: 00
iter: 05999, precision source classifier: 0.02227    target classifier: 0.67759    beta: 0.33044    kappa: 0.02724    rho: 25.19948    requests: 00
iter: 06999, precision source classifier: 0.02734    target classifier: 0.66392    beta: 0.34620    kappa: 0.03358    rho: 43.40369    requests: 00
iter: 07999, precision source classifier: 0.03612    target classifier: 0.67279    beta: 0.34015    kappa: 0.03458    rho: 30.77141    requests: 00
iter: 08999, precision source classifier: 0.06951    target classifier: 0.68492    beta: 0.33933    kappa: 0.07250    rho: 26.58433    requests: 00
iter: 09999, precision source classifier: 0.07449    target classifier: 0.67107    beta: 0.35610    kappa: 0.08726    rho: 34.53378    requests: 01
iter: 10999, precision source classifier: 0.08038    target classifier: 0.75235    beta: 0.27009    kappa: 0.08744    rho: 17.67215    requests: 01
iter: 11999, precision source classifier: 0.08997    target classifier: 0.75444    beta: 0.27064    kappa: 0.08653    rho: 10.55600    requests: 01
iter: 12999, precision source classifier: 0.08545    target classifier: 0.75100    beta: 0.27306    kappa: 0.07983    rho: 6.18635    requests: 01
iter: 13999, precision source classifier: 0.06245    target classifier: 0.76765    beta: 0.24863    kappa: 0.05965    rho: 4.03647    requests: 01
iter: 14999, precision source classifier: 0.06725    target classifier: 0.76122    beta: 0.25679    kappa: 0.06345    rho: 3.71596    requests: 02
iter: 15999, precision source classifier: 0.02761    target classifier: 0.75661    beta: 0.25107    kappa: 0.02788    rho: 3.95366    requests: 02
iter: 16999, precision source classifier: 0.03132    target classifier: 0.75127    beta: 0.25754    kappa: 0.02770    rho: 0.18444    requests: 02
iter: 17999, precision source classifier: 0.02046    target classifier: 0.76684    beta: 0.23881    kappa: 0.02290    rho: 1.90533    requests: 02
iter: 18999, precision source classifier: 0.01738    target classifier: 0.76421    beta: 0.24073    kappa: 0.01801    rho: -0.43136    requests: 02
iter: 19999, precision source classifier: 0.06743    target classifier: 0.76041    beta: 0.25771    kappa: 0.06083    rho: -6.63873    requests: 03
iter: 20999, precision source classifier: 0.07938    target classifier: 0.77978    beta: 0.24004    kappa: 0.04462    rho: -11.89557    requests: 03
iter: 21999, precision source classifier: 0.07431    target classifier: 0.78096    beta: 0.23745    kappa: 0.04028    rho: -12.34508    requests: 03
iter: 22999, precision source classifier: 0.08599    target classifier: 0.77299    beta: 0.24919    kappa: 0.04417    rho: -12.39801    requests: 03
iter: 23999, precision source classifier: 0.10491    target classifier: 0.77752    beta: 0.24940    kappa: 0.04815    rho: -16.04297    requests: 03
iter: 24999, precision source classifier: 0.10907    target classifier: 0.78077    beta: 0.24691    kappa: 0.05367    rho: -13.00915    requests: 04
iter: 25999, precision source classifier: 0.10500    target classifier: 0.79055    beta: 0.23488    kappa: 0.07151    rho: -10.75924    requests: 04
iter: 26999, precision source classifier: 0.11034    target classifier: 0.79381    beta: 0.23263    kappa: 0.07277    rho: -8.41087    requests: 04
iter: 27999, precision source classifier: 0.11016    target classifier: 0.79263    beta: 0.23390    kappa: 0.06137    rho: -7.61750    requests: 04
iter: 28999, precision source classifier: 0.10925    target classifier: 0.77969    beta: 0.24818    kappa: 0.06743    rho: -6.97634    requests: 04
iter: 29999, precision source classifier: 0.10898    target classifier: 0.79200    beta: 0.23430    kappa: 0.07178    rho: -1.45668    requests: 05
iter: 30999, precision source classifier: 0.11061    target classifier: 0.78675    beta: 0.24063    kappa: 0.06807    rho: -1.18323    requests: 05
iter: 31999, precision source classifier: 0.10654    target classifier: 0.78141    beta: 0.24550    kappa: 0.05358    rho: -4.68689    requests: 05
iter: 32999, precision source classifier: 0.10572    target classifier: 0.79390    beta: 0.23133    kappa: 0.05693    rho: -2.80439    requests: 05
iter: 33999, precision source classifier: 0.10454    target classifier: 0.78530    beta: 0.24061    kappa: 0.04716    rho: -4.36173    requests: 05
iter: 34999, precision source classifier: 0.10463    target classifier: 0.78213    beta: 0.24417    kappa: 0.05549    rho: -4.68521    requests: 06
iter: 35999, precision source classifier: 0.09631    target classifier: 0.79327    beta: 0.22962    kappa: 0.05014    rho: -2.04246    requests: 06
iter: 36999, precision source classifier: 0.09079    target classifier: 0.80413    beta: 0.21629    kappa: 0.05494    rho: -1.20567    requests: 06
iter: 37999, precision source classifier: 0.09531    target classifier: 0.79462    beta: 0.22787    kappa: 0.05340    rho: -1.69116    requests: 06
iter: 38999, precision source classifier: 0.09296    target classifier: 0.79435    beta: 0.22758    kappa: 0.06544    rho: 1.31128    requests: 06
iter: 39999, precision source classifier: 0.09712    target classifier: 0.80277    beta: 0.21931    kappa: 0.05367    rho: -2.35042    requests: 07
iter: 40999, precision source classifier: 0.10527    target classifier: 0.80033    beta: 0.22403    kappa: 0.07857    rho: -0.39328    requests: 07
iter: 41999, precision source classifier: 0.10319    target classifier: 0.79318    beta: 0.23148    kappa: 0.06291    rho: -1.28491    requests: 07
iter: 42999, precision source classifier: 0.09902    target classifier: 0.80232    beta: 0.22027    kappa: 0.06743    rho: -0.50286    requests: 07
iter: 43999, precision source classifier: 0.09359    target classifier: 0.79625    beta: 0.22564    kappa: 0.06164    rho: 1.07909    requests: 07
iter: 44999, precision source classifier: 0.09033    target classifier: 0.79652    beta: 0.22453    kappa: 0.05974    rho: 0.84912    requests: 08
iter: 45999, precision source classifier: 0.07404    target classifier: 0.79381    beta: 0.22352    kappa: 0.05141    rho: 1.45180    requests: 08
iter: 46999, precision source classifier: 0.07802    target classifier: 0.79028    beta: 0.22831    kappa: 0.04815    rho: 1.87673    requests: 08
iter: 47999, precision source classifier: 0.06798    target classifier: 0.79236    beta: 0.22362    kappa: 0.04182    rho: 2.90113    requests: 08
iter: 48999, precision source classifier: 0.06834    target classifier: 0.79661    beta: 0.21914    kappa: 0.04544    rho: 1.70425    requests: 08
iter: 49999, precision source classifier: 0.05802    target classifier: 0.78693    beta: 0.22701    kappa: 0.04218    rho: 2.66379    requests: 09
iter: 50999, precision source classifier: 0.05811    target classifier: 0.79707    beta: 0.21628    kappa: 0.05159    rho: 2.48295    requests: 09
iter: 51999, precision source classifier: 0.05748    target classifier: 0.80150    beta: 0.21144    kappa: 0.05431    rho: 3.51920    requests: 09
iter: 52999, precision source classifier: 0.06264    target classifier: 0.79788    beta: 0.21646    kappa: 0.05757    rho: 3.56397    requests: 09
iter: 53999, precision source classifier: 0.07449    target classifier: 0.79779    beta: 0.21933    kappa: 0.07024    rho: 3.66564    requests: 09
iter: 54999, precision source classifier: 0.08182    target classifier: 0.79508    beta: 0.22403    kappa: 0.08717    rho: 5.94749    requests: 10
iter: 55999, precision source classifier: 0.06255    target classifier: 0.80558    beta: 0.20824    kappa: 0.07015    rho: 3.59645    requests: 10
iter: 56999, precision source classifier: 0.06743    target classifier: 0.80404    beta: 0.21098    kappa: 0.07757    rho: 4.06114    requests: 10
iter: 57999, precision source classifier: 0.07006    target classifier: 0.80349    beta: 0.21216    kappa: 0.07893    rho: 2.91572    requests: 10
iter: 58999, precision source classifier: 0.07793    target classifier: 0.80340    beta: 0.21407    kappa: 0.08744    rho: 4.68605    requests: 10
iter: 59999, precision source classifier: 0.08228    target classifier: 0.81146    beta: 0.20631    kappa: 0.08074    rho: 2.47852    requests: 10
iter: 60999, precision source classifier: 0.07413    target classifier: 0.80838    beta: 0.20782    kappa: 0.07685    rho: 3.44848    requests: 10
iter: 61999, precision source classifier: 0.06209    target classifier: 0.81318    beta: 0.20004    kappa: 0.06236    rho: 1.94614    requests: 10
iter: 62999, precision source classifier: 0.07078    target classifier: 0.80431    beta: 0.21145    kappa: 0.06752    rho: 0.73557    requests: 10
iter: 63999, precision source classifier: 0.06979    target classifier: 0.80865    beta: 0.20656    kappa: 0.05992    rho: 0.90539    requests: 10
iter: 64999, precision source classifier: 0.06472    target classifier: 0.80476    beta: 0.20959    kappa: 0.05467    rho: 1.33439    requests: 10
iter: 65999, precision source classifier: 0.06101    target classifier: 0.80838    beta: 0.20491    kappa: 0.04915    rho: 0.98417    requests: 10
iter: 66999, precision source classifier: 0.06119    target classifier: 0.81273    beta: 0.20033    kappa: 0.04671    rho: 2.08255    requests: 10
iter: 67999, precision source classifier: 0.06671    target classifier: 0.80096    beta: 0.21411    kappa: 0.04471    rho: 2.42283    requests: 10
iter: 68999, precision source classifier: 0.07721    target classifier: 0.80503    beta: 0.21213    kappa: 0.05485    rho: 0.84934    requests: 10
iter: 69999, precision source classifier: 0.08056    target classifier: 0.80449    beta: 0.21350    kappa: 0.05014    rho: 0.97364    requests: 10
iter: 00999, precision source classifier: 0.04562    target classifier: 0.10337
iter: 01999, precision source classifier: 0.05567    target classifier: 0.19795
iter: 02999, precision source classifier: 0.04263    target classifier: 0.07449
iter: 03999, precision source classifier: 0.15261    target classifier: 0.17333
iter: 04999, precision source classifier: 0.07341    target classifier: 0.18492
iter: 05999, precision source classifier: 0.06481    target classifier: 0.18954
iter: 06999, precision source classifier: 0.18573    target classifier: 0.18121
iter: 07999, precision source classifier: 0.12301    target classifier: 0.18782
iter: 08999, precision source classifier: 0.16247    target classifier: 0.17940
iter: 09999, precision source classifier: 0.15478    target classifier: 0.18791
