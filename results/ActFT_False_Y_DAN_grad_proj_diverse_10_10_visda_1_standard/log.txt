{'method': 'Y_DAN', 'start_test': 1, 'policy': 'grad_proj_diverse', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': True, 'projection': True, 'tag': 'ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard', 'n_active_select': 10, 'output_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_10_10_visda_1_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.11713    target classifier: 0.61559    beta: 0.43605    kappa: 0.10174    rho: 18.97315    requests: 00
iter: 01999, precision source classifier: 0.07920    target classifier: 0.64084    beta: 0.39071    kappa: 0.08934    rho: 9.52617    requests: 00
iter: 02999, precision source classifier: 0.10201    target classifier: 0.64238    beta: 0.39892    kappa: 0.09848    rho: 9.25778    requests: 00
iter: 03999, precision source classifier: 0.06200    target classifier: 0.66609    beta: 0.35666    kappa: 0.05141    rho: -2.48594    requests: 00
iter: 04999, precision source classifier: 0.03602    target classifier: 0.68239    beta: 0.33018    kappa: 0.01159    rho: -7.34388    requests: 00
iter: 05999, precision source classifier: 0.03485    target classifier: 0.68311    beta: 0.32903    kappa: 0.00615    rho: -3.65685    requests: 00
iter: 06999, precision source classifier: 0.04417    target classifier: 0.66953    beta: 0.34642    kappa: 0.02335    rho: -6.90683    requests: 00
iter: 07999, precision source classifier: 0.04870    target classifier: 0.65858    beta: 0.35957    kappa: 0.03331    rho: -5.85683    requests: 00
iter: 08999, precision source classifier: 0.04091    target classifier: 0.66935    beta: 0.34544    kappa: 0.01702    rho: -3.97557    requests: 00
iter: 09999, precision source classifier: 0.03584    target classifier: 0.67759    beta: 0.33509    kappa: 0.01322    rho: 0.38034    requests: 01
iter: 10999, precision source classifier: 0.02969    target classifier: 0.69768    beta: 0.31228    kappa: 0.00679    rho: -1.20619    requests: 01
iter: 11999, precision source classifier: 0.02779    target classifier: 0.69614    beta: 0.31325    kappa: 0.00760    rho: -0.49840    requests: 01
iter: 12999, precision source classifier: 0.02869    target classifier: 0.70221    beta: 0.30730    kappa: 0.01312    rho: 1.40166    requests: 01
iter: 13999, precision source classifier: 0.02752    target classifier: 0.69605    beta: 0.31325    kappa: 0.01557    rho: -1.82064    requests: 01
iter: 14999, precision source classifier: 0.02562    target classifier: 0.69841    beta: 0.31023    kappa: 0.01602    rho: -1.74625    requests: 02
iter: 15999, precision source classifier: 0.02453    target classifier: 0.71126    beta: 0.29672    kappa: 0.02435    rho: 4.51294    requests: 02
iter: 16999, precision source classifier: 0.03412    target classifier: 0.71407    beta: 0.29676    kappa: 0.03295    rho: 5.00727    requests: 02
iter: 17999, precision source classifier: 0.02643    target classifier: 0.71099    beta: 0.29758    kappa: 0.02851    rho: 2.00429    requests: 02
iter: 18999, precision source classifier: 0.02661    target classifier: 0.71687    beta: 0.29160    kappa: 0.02444    rho: 4.96478    requests: 02
iter: 19999, precision source classifier: 0.03068    target classifier: 0.71244    beta: 0.29739    kappa: 0.02489    rho: 0.66525    requests: 03
iter: 20999, precision source classifier: 0.04625    target classifier: 0.70990    beta: 0.30490    kappa: 0.04173    rho: 0.05952    requests: 03
iter: 21999, precision source classifier: 0.05784    target classifier: 0.70719    beta: 0.31152    kappa: 0.05340    rho: -1.50210    requests: 03
iter: 22999, precision source classifier: 0.06879    target classifier: 0.72402    beta: 0.29712    kappa: 0.06617    rho: -1.68643    requests: 03
iter: 23999, precision source classifier: 0.08264    target classifier: 0.71235    beta: 0.31431    kappa: 0.07467    rho: -1.92511    requests: 03
iter: 24999, precision source classifier: 0.08680    target classifier: 0.72366    beta: 0.30337    kappa: 0.07848    rho: 1.18368    requests: 04
iter: 25999, precision source classifier: 0.08861    target classifier: 0.72167    beta: 0.30615    kappa: 0.08110    rho: 5.60992    requests: 04
iter: 26999, precision source classifier: 0.08870    target classifier: 0.72402    beta: 0.30360    kappa: 0.08237    rho: 7.32996    requests: 04
iter: 27999, precision source classifier: 0.09033    target classifier: 0.71805    beta: 0.31071    kappa: 0.07902    rho: 7.77473    requests: 04
iter: 28999, precision source classifier: 0.08535    target classifier: 0.73479    beta: 0.29073    kappa: 0.07866    rho: 7.05317    requests: 04
iter: 29999, precision source classifier: 0.09160    target classifier: 0.73226    beta: 0.29551    kappa: 0.08210    rho: 5.52540    requests: 05
iter: 30999, precision source classifier: 0.08807    target classifier: 0.73325    beta: 0.29328    kappa: 0.08354    rho: 9.10627    requests: 05
iter: 31999, precision source classifier: 0.09314    target classifier: 0.72900    beta: 0.29960    kappa: 0.08010    rho: 6.91871    requests: 05
iter: 32999, precision source classifier: 0.09513    target classifier: 0.73959    beta: 0.28857    kappa: 0.08707    rho: 9.47294    requests: 05
iter: 33999, precision source classifier: 0.09658    target classifier: 0.73036    beta: 0.29924    kappa: 0.08762    rho: 10.43340    requests: 05
iter: 34999, precision source classifier: 0.11124    target classifier: 0.73027    beta: 0.30428    kappa: 0.09757    rho: 12.21596    requests: 06
iter: 35999, precision source classifier: 0.10726    target classifier: 0.74203    beta: 0.28975    kappa: 0.09948    rho: 8.94252    requests: 06
iter: 36999, precision source classifier: 0.10997    target classifier: 0.73678    beta: 0.29653    kappa: 0.10065    rho: 9.36680    requests: 06
iter: 37999, precision source classifier: 0.11885    target classifier: 0.73516    beta: 0.30136    kappa: 0.10346    rho: 9.65403    requests: 06
iter: 38999, precision source classifier: 0.12500    target classifier: 0.73072    beta: 0.30854    kappa: 0.10626    rho: 6.27404    requests: 06
iter: 39999, precision source classifier: 0.13957    target classifier: 0.73742    beta: 0.30598    kappa: 0.12437    rho: 8.04754    requests: 07
iter: 40999, precision source classifier: 0.14021    target classifier: 0.73923    beta: 0.30410    kappa: 0.12527    rho: 6.45108    requests: 07
iter: 41999, precision source classifier: 0.13070    target classifier: 0.74185    beta: 0.29777    kappa: 0.12210    rho: 6.81689    requests: 07
iter: 42999, precision source classifier: 0.14274    target classifier: 0.73751    beta: 0.30701    kappa: 0.12735    rho: 7.46486    requests: 07
iter: 43999, precision source classifier: 0.15804    target classifier: 0.73977    beta: 0.30989    kappa: 0.14446    rho: 7.71139    requests: 07
iter: 44999, precision source classifier: 0.15958    target classifier: 0.73914    beta: 0.31121    kappa: 0.14356    rho: 7.51884    requests: 08
iter: 45999, precision source classifier: 0.15550    target classifier: 0.73959    beta: 0.30918    kappa: 0.14093    rho: 7.93130    requests: 08
iter: 46999, precision source classifier: 0.16139    target classifier: 0.74348    beta: 0.30671    kappa: 0.15080    rho: 8.72609    requests: 08
iter: 47999, precision source classifier: 0.15170    target classifier: 0.74194    beta: 0.30502    kappa: 0.13840    rho: 7.57045    requests: 08
iter: 48999, precision source classifier: 0.15424    target classifier: 0.74421    beta: 0.30326    kappa: 0.14093    rho: 6.25829    requests: 08
iter: 49999, precision source classifier: 0.15677    target classifier: 0.74385    beta: 0.30460    kappa: 0.14826    rho: 6.98648    requests: 09
iter: 50999, precision source classifier: 0.16093    target classifier: 0.74937    beta: 0.29954    kappa: 0.15206    rho: 7.60175    requests: 09
iter: 51999, precision source classifier: 0.15921    target classifier: 0.74493    beta: 0.30420    kappa: 0.14663    rho: 8.93362    requests: 09
iter: 52999, precision source classifier: 0.14808    target classifier: 0.74620    beta: 0.29874    kappa: 0.13604    rho: 7.97216    requests: 09
iter: 53999, precision source classifier: 0.14256    target classifier: 0.74520    beta: 0.29798    kappa: 0.13007    rho: 7.56573    requests: 09
iter: 54999, precision source classifier: 0.13052    target classifier: 0.74547    beta: 0.29355    kappa: 0.11767    rho: 6.37143    requests: 10
iter: 55999, precision source classifier: 0.13577    target classifier: 0.74231    beta: 0.29899    kappa: 0.11097    rho: 7.33285    requests: 10
iter: 56999, precision source classifier: 0.13369    target classifier: 0.74113    beta: 0.29963    kappa: 0.10889    rho: 8.12618    requests: 10
iter: 57999, precision source classifier: 0.12681    target classifier: 0.74846    beta: 0.28888    kappa: 0.10663    rho: 5.78902    requests: 10
iter: 58999, precision source classifier: 0.11948    target classifier: 0.75009    beta: 0.28463    kappa: 0.10644    rho: 6.09750    requests: 10
iter: 59999, precision source classifier: 0.11577    target classifier: 0.74104    beta: 0.29366    kappa: 0.10192    rho: 6.35570    requests: 10
iter: 60999, precision source classifier: 0.10708    target classifier: 0.74176    beta: 0.29000    kappa: 0.08988    rho: 4.28690    requests: 10
iter: 61999, precision source classifier: 0.10400    target classifier: 0.74638    beta: 0.28386    kappa: 0.09015    rho: 4.57204    requests: 10
iter: 62999, precision source classifier: 0.10373    target classifier: 0.75163    beta: 0.27792    kappa: 0.08707    rho: 2.87913    requests: 10
iter: 63999, precision source classifier: 0.09984    target classifier: 0.75127    beta: 0.27712    kappa: 0.08861    rho: 4.84572    requests: 10
iter: 64999, precision source classifier: 0.09658    target classifier: 0.74692    beta: 0.28093    kappa: 0.08753    rho: 3.42712    requests: 10
iter: 65999, precision source classifier: 0.10255    target classifier: 0.74819    beta: 0.28139    kappa: 0.09169    rho: 4.69728    requests: 10
iter: 66999, precision source classifier: 0.10708    target classifier: 0.75208    beta: 0.27846    kappa: 0.09776    rho: 3.56217    requests: 10
iter: 67999, precision source classifier: 0.10672    target classifier: 0.74620    beta: 0.28492    kappa: 0.09432    rho: 4.17530    requests: 10
iter: 68999, precision source classifier: 0.10536    target classifier: 0.74276    beta: 0.28833    kappa: 0.09712    rho: 4.91433    requests: 10
iter: 69999, precision source classifier: 0.11613    target classifier: 0.74330    beta: 0.29123    kappa: 0.10282    rho: 3.26140    requests: 10
iter: 00999, precision source classifier: 0.02770    target classifier: 0.06816
iter: 01999, precision source classifier: 0.08300    target classifier: 0.04164
iter: 02999, precision source classifier: 0.08726    target classifier: 0.07449
iter: 03999, precision source classifier: 0.08698    target classifier: 0.03548
iter: 04999, precision source classifier: 0.08391    target classifier: 0.06200
iter: 05999, precision source classifier: 0.08382    target classifier: 0.03892
iter: 06999, precision source classifier: 0.08418    target classifier: 0.06879
iter: 07999, precision source classifier: 0.08427    target classifier: 0.07277
iter: 08999, precision source classifier: 0.08454    target classifier: 0.07332
iter: 09999, precision source classifier: 0.08526    target classifier: 0.08671
