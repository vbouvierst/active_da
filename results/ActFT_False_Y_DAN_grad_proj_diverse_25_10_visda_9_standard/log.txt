{'method': 'Y_DAN', 'start_test': 1, 'policy': 'grad_proj_diverse', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': True, 'projection': True, 'tag': 'ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard', 'n_active_select': 25, 'output_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_25_10_visda_9_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.13740    target classifier: 0.60101    beta: 0.46316    kappa: 0.25253    rho: 17.12747    requests: 00
iter: 01999, precision source classifier: 0.09386    target classifier: 0.66039    beta: 0.37548    kappa: 0.02706    rho: 9.48513    requests: 00
iter: 02999, precision source classifier: 0.10807    target classifier: 0.65451    beta: 0.38804    kappa: 0.05947    rho: -1.86204    requests: 00
iter: 03999, precision source classifier: 0.10138    target classifier: 0.65514    beta: 0.38445    kappa: 0.05974    rho: -15.55931    requests: 00
iter: 04999, precision source classifier: 0.10472    target classifier: 0.67940    beta: 0.35882    kappa: 0.07531    rho: -19.55348    requests: 00
iter: 05999, precision source classifier: 0.08717    target classifier: 0.69008    beta: 0.34024    kappa: 0.08898    rho: -4.32468    requests: 00
iter: 06999, precision source classifier: 0.07350    target classifier: 0.68130    beta: 0.34469    kappa: 0.07522    rho: -12.29683    requests: 00
iter: 07999, precision source classifier: 0.06834    target classifier: 0.68039    beta: 0.34375    kappa: 0.06725    rho: -12.03080    requests: 00
iter: 08999, precision source classifier: 0.06861    target classifier: 0.67813    beta: 0.34628    kappa: 0.06716    rho: -18.89992    requests: 00
iter: 09999, precision source classifier: 0.06689    target classifier: 0.66781    beta: 0.35669    kappa: 0.06110    rho: -18.77644    requests: 01
iter: 10999, precision source classifier: 0.06598    target classifier: 0.72719    beta: 0.29284    kappa: 0.06617    rho: -8.64481    requests: 01
iter: 11999, precision source classifier: 0.06671    target classifier: 0.72638    beta: 0.29394    kappa: 0.07332    rho: -3.28387    requests: 01
iter: 12999, precision source classifier: 0.07350    target classifier: 0.71597    beta: 0.30731    kappa: 0.07540    rho: -5.75264    requests: 01
iter: 13999, precision source classifier: 0.07259    target classifier: 0.72004    beta: 0.30263    kappa: 0.07449    rho: -0.48744    requests: 01
iter: 14999, precision source classifier: 0.06961    target classifier: 0.71126    beta: 0.31108    kappa: 0.07078    rho: -5.12624    requests: 02
iter: 15999, precision source classifier: 0.06716    target classifier: 0.72837    beta: 0.29195    kappa: 0.05856    rho: -3.79114    requests: 02
iter: 16999, precision source classifier: 0.07748    target classifier: 0.73172    beta: 0.29158    kappa: 0.06200    rho: -0.96319    requests: 02
iter: 17999, precision source classifier: 0.06843    target classifier: 0.73063    beta: 0.28992    kappa: 0.05232    rho: 0.96588    requests: 02
iter: 18999, precision source classifier: 0.05322    target classifier: 0.74339    beta: 0.27180    kappa: 0.03340    rho: -4.87953    requests: 02
iter: 19999, precision source classifier: 0.03693    target classifier: 0.74566    beta: 0.26486    kappa: 0.01493    rho: -1.38987    requests: 03
iter: 20999, precision source classifier: 0.03449    target classifier: 0.75235    beta: 0.25726    kappa: 0.01421    rho: -3.54076    requests: 03
iter: 21999, precision source classifier: 0.03621    target classifier: 0.73561    beta: 0.27508    kappa: 0.01412    rho: -3.73455    requests: 03
iter: 22999, precision source classifier: 0.03738    target classifier: 0.73706    beta: 0.27391    kappa: 0.01340    rho: 1.97163    requests: 03
iter: 23999, precision source classifier: 0.04164    target classifier: 0.73624    beta: 0.27597    kappa: 0.01756    rho: 1.41140    requests: 03
iter: 24999, precision source classifier: 0.05078    target classifier: 0.74149    beta: 0.27310    kappa: 0.02896    rho: 2.77713    requests: 04
iter: 25999, precision source classifier: 0.08834    target classifier: 0.72791    beta: 0.29922    kappa: 0.07395    rho: -1.80208    requests: 04
iter: 26999, precision source classifier: 0.12944    target classifier: 0.74213    beta: 0.29702    kappa: 0.12862    rho: 4.37776    requests: 04
iter: 27999, precision source classifier: 0.14718    target classifier: 0.73226    beta: 0.31475    kappa: 0.17017    rho: 8.50942    requests: 04
iter: 28999, precision source classifier: 0.14781    target classifier: 0.73832    beta: 0.30788    kappa: 0.17560    rho: 5.48606    requests: 04
iter: 29999, precision source classifier: 0.14256    target classifier: 0.73235    beta: 0.31295    kappa: 0.17632    rho: 6.67981    requests: 05
iter: 30999, precision source classifier: 0.12210    target classifier: 0.75054    beta: 0.28497    kappa: 0.15822    rho: 7.36056    requests: 05
iter: 31999, precision source classifier: 0.12600    target classifier: 0.73742    beta: 0.30123    kappa: 0.16772    rho: 5.61613    requests: 05
iter: 32999, precision source classifier: 0.12907    target classifier: 0.75091    beta: 0.28683    kappa: 0.16537    rho: 7.12634    requests: 05
iter: 33999, precision source classifier: 0.13822    target classifier: 0.74928    beta: 0.29176    kappa: 0.16673    rho: 4.40215    requests: 05
iter: 34999, precision source classifier: 0.13939    target classifier: 0.74448    beta: 0.29772    kappa: 0.16999    rho: 6.64150    requests: 06
iter: 35999, precision source classifier: 0.13631    target classifier: 0.75018    beta: 0.29007    kappa: 0.17225    rho: 8.00111    requests: 06
iter: 36999, precision source classifier: 0.13369    target classifier: 0.75244    beta: 0.28658    kappa: 0.17786    rho: 13.20338    requests: 06
iter: 37999, precision source classifier: 0.13577    target classifier: 0.75416    beta: 0.28528    kappa: 0.16474    rho: 8.39837    requests: 06
iter: 38999, precision source classifier: 0.13831    target classifier: 0.74448    beta: 0.29735    kappa: 0.18356    rho: 8.68569    requests: 06
iter: 39999, precision source classifier: 0.14247    target classifier: 0.74991    beta: 0.29247    kappa: 0.17424    rho: 6.83198    requests: 07
iter: 40999, precision source classifier: 0.12971    target classifier: 0.77317    beta: 0.26148    kappa: 0.15243    rho: 8.22110    requests: 07
iter: 41999, precision source classifier: 0.12735    target classifier: 0.76720    beta: 0.26762    kappa: 0.15369    rho: 7.58227    requests: 07
iter: 42999, precision source classifier: 0.12518    target classifier: 0.77009    beta: 0.26365    kappa: 0.14908    rho: 7.15519    requests: 07
iter: 43999, precision source classifier: 0.12391    target classifier: 0.76159    beta: 0.27297    kappa: 0.14537    rho: 6.65729    requests: 07
iter: 44999, precision source classifier: 0.10934    target classifier: 0.76675    beta: 0.26272    kappa: 0.12355    rho: 6.62707    requests: 08
iter: 45999, precision source classifier: 0.09640    target classifier: 0.76729    beta: 0.25836    kappa: 0.10925    rho: 6.58674    requests: 08
iter: 46999, precision source classifier: 0.09622    target classifier: 0.76675    beta: 0.25891    kappa: 0.10726    rho: 6.18712    requests: 08
iter: 47999, precision source classifier: 0.09413    target classifier: 0.76901    beta: 0.25582    kappa: 0.10699    rho: 6.61760    requests: 08
iter: 48999, precision source classifier: 0.07811    target classifier: 0.76095    beta: 0.26011    kappa: 0.07983    rho: 4.29775    requests: 08
iter: 49999, precision source classifier: 0.07549    target classifier: 0.76846    beta: 0.25125    kappa: 0.07368    rho: 5.79949    requests: 09
iter: 50999, precision source classifier: 0.06924    target classifier: 0.76819    beta: 0.24986    kappa: 0.06119    rho: 4.35809    requests: 09
iter: 51999, precision source classifier: 0.07214    target classifier: 0.77145    beta: 0.24713    kappa: 0.06191    rho: 4.00634    requests: 09
iter: 52999, precision source classifier: 0.07304    target classifier: 0.76783    beta: 0.25127    kappa: 0.06345    rho: 2.98526    requests: 09
iter: 53999, precision source classifier: 0.07585    target classifier: 0.76928    beta: 0.25047    kappa: 0.06562    rho: 2.38225    requests: 09
iter: 54999, precision source classifier: 0.08155    target classifier: 0.76982    beta: 0.25143    kappa: 0.07187    rho: 1.52660    requests: 10
iter: 55999, precision source classifier: 0.07160    target classifier: 0.77435    beta: 0.24387    kappa: 0.06227    rho: 0.85206    requests: 10
iter: 56999, precision source classifier: 0.06245    target classifier: 0.76611    beta: 0.25027    kappa: 0.05539    rho: -1.18175    requests: 10
iter: 57999, precision source classifier: 0.06200    target classifier: 0.76430    beta: 0.25208    kappa: 0.05521    rho: -1.00591    requests: 10
iter: 58999, precision source classifier: 0.05947    target classifier: 0.76195    beta: 0.25390    kappa: 0.05567    rho: -2.51961    requests: 10
iter: 59999, precision source classifier: 0.05322    target classifier: 0.75833    beta: 0.25604    kappa: 0.05567    rho: -0.59698    requests: 10
iter: 60999, precision source classifier: 0.04372    target classifier: 0.75570    beta: 0.25624    kappa: 0.04598    rho: -0.12022    requests: 10
iter: 61999, precision source classifier: 0.05096    target classifier: 0.76539    beta: 0.24800    kappa: 0.04987    rho: -2.54113    requests: 10
iter: 62999, precision source classifier: 0.04671    target classifier: 0.76548    beta: 0.24680    kappa: 0.04843    rho: -2.66295    requests: 10
iter: 63999, precision source classifier: 0.04444    target classifier: 0.76322    beta: 0.24858    kappa: 0.05404    rho: 0.23376    requests: 10
iter: 64999, precision source classifier: 0.04173    target classifier: 0.77018    beta: 0.24061    kappa: 0.04136    rho: -1.67293    requests: 10
iter: 65999, precision source classifier: 0.04046    target classifier: 0.76720    beta: 0.24341    kappa: 0.04155    rho: -2.06591    requests: 10
iter: 66999, precision source classifier: 0.04118    target classifier: 0.76991    beta: 0.24076    kappa: 0.04399    rho: -1.97810    requests: 10
iter: 67999, precision source classifier: 0.04173    target classifier: 0.76756    beta: 0.24335    kappa: 0.05078    rho: 0.28997    requests: 10
iter: 68999, precision source classifier: 0.04290    target classifier: 0.76539    beta: 0.24592    kappa: 0.04743    rho: -0.49766    requests: 10
iter: 69999, precision source classifier: 0.04535    target classifier: 0.77272    beta: 0.23887    kappa: 0.04336    rho: -2.49047    requests: 10
iter: 00999, precision source classifier: 0.07069    target classifier: 0.07449
iter: 01999, precision source classifier: 0.07051    target classifier: 0.07006
iter: 02999, precision source classifier: 0.17958    target classifier: 0.03168
iter: 03999, precision source classifier: 0.09042    target classifier: 0.08029
iter: 04999, precision source classifier: 0.06571    target classifier: 0.07214
iter: 05999, precision source classifier: 0.09151    target classifier: 0.04082
iter: 06999, precision source classifier: 0.09187    target classifier: 0.03286
iter: 07999, precision source classifier: 0.06499    target classifier: 0.09441
iter: 08999, precision source classifier: 0.06861    target classifier: 0.06933
iter: 09999, precision source classifier: 0.08671    target classifier: 0.04951
