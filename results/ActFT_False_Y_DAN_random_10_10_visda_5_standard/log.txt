{'method': 'Y_DAN', 'start_test': 1, 'policy': 'random', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': False, 'projection': True, 'tag': 'ActFT_False_Y_DAN_random_10_10_visda_5_standard', 'n_active_select': 10, 'output_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_10_10_visda_5_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.07060    target classifier: 0.62228    beta: 0.40705    kappa: 0.02887    rho: -8.86586    requests: 00
iter: 01999, precision source classifier: 0.04933    target classifier: 0.64718    beta: 0.37179    kappa: 0.06417    rho: 26.99167    requests: 00
iter: 02999, precision source classifier: 0.11477    target classifier: 0.66845    beta: 0.37525    kappa: 0.19442    rho: 15.39014    requests: 00
iter: 03999, precision source classifier: 0.10255    target classifier: 0.65541    beta: 0.38465    kappa: 0.18456    rho: 23.90193    requests: 00
iter: 04999, precision source classifier: 0.10310    target classifier: 0.65876    beta: 0.38115    kappa: 0.17659    rho: 25.54445    requests: 00
iter: 05999, precision source classifier: 0.09441    target classifier: 0.67089    beta: 0.36412    kappa: 0.15668    rho: 35.74461    requests: 00
iter: 06999, precision source classifier: 0.09341    target classifier: 0.65514    beta: 0.38107    kappa: 0.15921    rho: 27.56980    requests: 00
iter: 07999, precision source classifier: 0.10482    target classifier: 0.65749    beta: 0.38330    kappa: 0.18791    rho: 28.40613    requests: 00
iter: 08999, precision source classifier: 0.10192    target classifier: 0.65976    beta: 0.37955    kappa: 0.18392    rho: 24.86894    requests: 00
iter: 09999, precision source classifier: 0.09938    target classifier: 0.67044    beta: 0.36663    kappa: 0.17116    rho: 27.71032    requests: 01
iter: 10999, precision source classifier: 0.09232    target classifier: 0.70664    beta: 0.32394    kappa: 0.14482    rho: 19.94094    requests: 01
iter: 11999, precision source classifier: 0.07558    target classifier: 0.71189    beta: 0.31241    kappa: 0.12572    rho: 18.95328    requests: 01
iter: 12999, precision source classifier: 0.06743    target classifier: 0.70936    beta: 0.31239    kappa: 0.10599    rho: 13.27563    requests: 01
iter: 13999, precision source classifier: 0.06562    target classifier: 0.71533    beta: 0.30540    kappa: 0.10445    rho: 12.20358    requests: 01
iter: 14999, precision source classifier: 0.04444    target classifier: 0.70854    beta: 0.30574    kappa: 0.07151    rho: 12.64058    requests: 02
iter: 15999, precision source classifier: 0.05413    target classifier: 0.70565    beta: 0.31192    kappa: 0.06879    rho: 11.94754    requests: 02
iter: 16999, precision source classifier: 0.08689    target classifier: 0.70329    beta: 0.32568    kappa: 0.07522    rho: 8.86177    requests: 02
iter: 17999, precision source classifier: 0.12781    target classifier: 0.70167    beta: 0.34280    kappa: 0.12084    rho: 6.40756    requests: 02
iter: 18999, precision source classifier: 0.13613    target classifier: 0.70873    beta: 0.33794    kappa: 0.12953    rho: 7.94757    requests: 02
iter: 19999, precision source classifier: 0.13432    target classifier: 0.70221    beta: 0.34476    kappa: 0.12120    rho: -5.97327    requests: 03
iter: 20999, precision source classifier: 0.10762    target classifier: 0.71524    beta: 0.31986    kappa: 0.08481    rho: -7.58261    requests: 03
iter: 21999, precision source classifier: 0.07630    target classifier: 0.70854    beta: 0.31627    kappa: 0.04843    rho: -5.97447    requests: 03
iter: 22999, precision source classifier: 0.06589    target classifier: 0.71434    beta: 0.30656    kappa: 0.04082    rho: -8.86722    requests: 03
iter: 23999, precision source classifier: 0.06155    target classifier: 0.71162    beta: 0.30803    kappa: 0.03738    rho: -15.94305    requests: 03
iter: 24999, precision source classifier: 0.05902    target classifier: 0.71769    beta: 0.30076    kappa: 0.04046    rho: -16.60833    requests: 04
iter: 25999, precision source classifier: 0.06264    target classifier: 0.70701    beta: 0.31331    kappa: 0.03946    rho: -11.13045    requests: 04
iter: 26999, precision source classifier: 0.06798    target classifier: 0.70854    beta: 0.31345    kappa: 0.04290    rho: -17.15684    requests: 04
iter: 27999, precision source classifier: 0.06499    target classifier: 0.71579    beta: 0.30471    kappa: 0.04462    rho: -16.57074    requests: 04
iter: 28999, precision source classifier: 0.06897    target classifier: 0.71968    beta: 0.30184    kappa: 0.04852    rho: -14.23150    requests: 04
iter: 29999, precision source classifier: 0.06580    target classifier: 0.71443    beta: 0.30643    kappa: 0.04426    rho: -17.91116    requests: 05
iter: 30999, precision source classifier: 0.07114    target classifier: 0.70809    beta: 0.31500    kappa: 0.05005    rho: -15.48449    requests: 05
iter: 31999, precision source classifier: 0.07087    target classifier: 0.71479    beta: 0.30771    kappa: 0.05693    rho: -9.23086    requests: 05
iter: 32999, precision source classifier: 0.07042    target classifier: 0.71932    beta: 0.30270    kappa: 0.05567    rho: -7.19781    requests: 05
iter: 33999, precision source classifier: 0.07169    target classifier: 0.71805    beta: 0.30447    kappa: 0.05196    rho: -11.84297    requests: 05
iter: 34999, precision source classifier: 0.07133    target classifier: 0.71678    beta: 0.30572    kappa: 0.05358    rho: -7.97393    requests: 06
iter: 35999, precision source classifier: 0.06273    target classifier: 0.72755    beta: 0.29144    kappa: 0.03965    rho: -5.73115    requests: 06
iter: 36999, precision source classifier: 0.06961    target classifier: 0.72113    beta: 0.30049    kappa: 0.04381    rho: -6.33430    requests: 06
iter: 37999, precision source classifier: 0.07006    target classifier: 0.71832    beta: 0.30365    kappa: 0.04100    rho: -7.88916    requests: 06
iter: 38999, precision source classifier: 0.06417    target classifier: 0.72556    beta: 0.29401    kappa: 0.03720    rho: -4.53474    requests: 06
iter: 39999, precision source classifier: 0.06209    target classifier: 0.71017    beta: 0.30975    kappa: 0.03123    rho: -5.63355    requests: 07
iter: 40999, precision source classifier: 0.07015    target classifier: 0.71588    beta: 0.30630    kappa: 0.03657    rho: -5.53964    requests: 07
iter: 41999, precision source classifier: 0.07295    target classifier: 0.71615    beta: 0.30694    kappa: 0.03702    rho: -4.74137    requests: 07
iter: 42999, precision source classifier: 0.08273    target classifier: 0.70619    beta: 0.32105    kappa: 0.04091    rho: -3.77849    requests: 07
iter: 43999, precision source classifier: 0.08164    target classifier: 0.70719    beta: 0.31959    kappa: 0.04281    rho: -2.72721    requests: 07
iter: 44999, precision source classifier: 0.09160    target classifier: 0.72212    beta: 0.30666    kappa: 0.04345    rho: -1.83194    requests: 08
iter: 45999, precision source classifier: 0.08798    target classifier: 0.71597    beta: 0.31219    kappa: 0.03802    rho: -1.66095    requests: 08
iter: 46999, precision source classifier: 0.09088    target classifier: 0.71280    beta: 0.31666    kappa: 0.03910    rho: -0.92429    requests: 08
iter: 47999, precision source classifier: 0.10047    target classifier: 0.71506    beta: 0.31752    kappa: 0.04833    rho: -0.19691    requests: 08
iter: 48999, precision source classifier: 0.10536    target classifier: 0.72158    beta: 0.31198    kappa: 0.05150    rho: -0.70864    requests: 08
iter: 49999, precision source classifier: 0.10147    target classifier: 0.70909    beta: 0.32452    kappa: 0.04471    rho: 0.48718    requests: 09
iter: 50999, precision source classifier: 0.09613    target classifier: 0.71850    beta: 0.31220    kappa: 0.04462    rho: 1.11256    requests: 09
iter: 51999, precision source classifier: 0.08662    target classifier: 0.71370    beta: 0.31420    kappa: 0.04046    rho: 1.65614    requests: 09
iter: 52999, precision source classifier: 0.08255    target classifier: 0.71325    beta: 0.31330    kappa: 0.04118    rho: 3.37306    requests: 09
iter: 53999, precision source classifier: 0.06445    target classifier: 0.71778    beta: 0.30241    kappa: 0.03295    rho: 3.93044    requests: 09
iter: 54999, precision source classifier: 0.05820    target classifier: 0.70954    beta: 0.30914    kappa: 0.02906    rho: 1.94965    requests: 10
iter: 55999, precision source classifier: 0.04870    target classifier: 0.72484    beta: 0.28999    kappa: 0.02218    rho: 2.77377    requests: 10
iter: 56999, precision source classifier: 0.04146    target classifier: 0.72212    beta: 0.29064    kappa: 0.02064    rho: 2.16156    requests: 10
iter: 57999, precision source classifier: 0.03783    target classifier: 0.71660    beta: 0.29528    kappa: 0.01865    rho: 1.27184    requests: 10
iter: 58999, precision source classifier: 0.04109    target classifier: 0.72701    beta: 0.28543    kappa: 0.02317    rho: 2.38548    requests: 10
iter: 59999, precision source classifier: 0.04308    target classifier: 0.72067    beta: 0.29264    kappa: 0.03005    rho: 1.67947    requests: 10
iter: 60999, precision source classifier: 0.04589    target classifier: 0.71904    beta: 0.29521    kappa: 0.03575    rho: 0.44722    requests: 10
iter: 61999, precision source classifier: 0.05811    target classifier: 0.72927    beta: 0.28819    kappa: 0.04607    rho: -1.46470    requests: 10
iter: 62999, precision source classifier: 0.07848    target classifier: 0.72239    beta: 0.30200    kappa: 0.06942    rho: -1.16036    requests: 10
iter: 63999, precision source classifier: 0.08119    target classifier: 0.72140    beta: 0.30398    kappa: 0.07295    rho: -2.74765    requests: 10
iter: 64999, precision source classifier: 0.09070    target classifier: 0.72357    beta: 0.30477    kappa: 0.08083    rho: -3.39325    requests: 10
iter: 65999, precision source classifier: 0.09667    target classifier: 0.72538    beta: 0.30478    kappa: 0.08508    rho: -4.94937    requests: 10
iter: 66999, precision source classifier: 0.08861    target classifier: 0.72230    beta: 0.30546    kappa: 0.07712    rho: -3.16058    requests: 10
iter: 67999, precision source classifier: 0.08943    target classifier: 0.72502    beta: 0.30275    kappa: 0.07486    rho: -4.01967    requests: 10
iter: 68999, precision source classifier: 0.08753    target classifier: 0.72131    beta: 0.30619    kappa: 0.07286    rho: -4.03243    requests: 10
iter: 69999, precision source classifier: 0.08771    target classifier: 0.72665    beta: 0.30040    kappa: 0.07304    rho: -3.70593    requests: 10
iter: 00999, precision source classifier: 0.08246    target classifier: 0.10988
iter: 01999, precision source classifier: 0.07259    target classifier: 0.10337
iter: 02999, precision source classifier: 0.10101    target classifier: 0.12663
iter: 03999, precision source classifier: 0.06562    target classifier: 0.09115
iter: 04999, precision source classifier: 0.09839    target classifier: 0.18610
iter: 05999, precision source classifier: 0.10273    target classifier: 0.17017
iter: 06999, precision source classifier: 0.05838    target classifier: 0.11703
iter: 07999, precision source classifier: 0.10047    target classifier: 0.11604
iter: 08999, precision source classifier: 0.05938    target classifier: 0.09794
iter: 09999, precision source classifier: 0.07160    target classifier: 0.16971
