{'method': 'Y_DAN', 'start_test': 1, 'policy': 'grad_proj_diverse', 'weight_policy': False, 'requests': 10, 'min_iter_request': 4000, 'max_iter_request': 2000, 'iter_last_request': 2500, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': True, 'projection': True, 'tag': 'ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard', 'output_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard/history', 'path': {'source_path': '../data/office-home/Art_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard/target.txt', 'test_path': '../data/office-home/Clipart_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard/active.txt'}, 'n_active_select': 42, 'gpu': '0', 'num_iterations': 26000, 'num_baseline_iterations': 10004, 'test_interval': 500, 'output_for_test': True, 'prep': {'test_10crop': True, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 65}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'office-home', 'data': {'source': {'list_path': '../data/office-home/Art_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_C_0_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/office-home/Clipart_local_test_fusion.txt', 'batch_size': 4}}}iter: 00499, precision source classifier: 0.01486    target classifier: 0.41757    beta: 0.59164    kappa: 0.01757    rho: 0.01663    requests: 00
iter: 00999, precision source classifier: 0.01396    target classifier: 0.42342    beta: 0.58516    kappa: 0.02523    rho: 0.02113    requests: 00
iter: 01499, precision source classifier: 0.01351    target classifier: 0.45045    beta: 0.55753    kappa: 0.01171    rho: 0.01359    requests: 00
iter: 01999, precision source classifier: 0.01081    target classifier: 0.47568    beta: 0.53053    kappa: 0.01306    rho: 0.01313    requests: 00
iter: 02499, precision source classifier: 0.00991    target classifier: 0.46667    beta: 0.53914    kappa: 0.01261    rho: 0.01293    requests: 00
iter: 02999, precision source classifier: 0.01216    target classifier: 0.48243    beta: 0.52442    kappa: 0.00856    rho: 0.01176    requests: 00
iter: 03499, precision source classifier: 0.01577    target classifier: 0.48333    beta: 0.52542    kappa: 0.01351    rho: 0.01428    requests: 00
iter: 03999, precision source classifier: 0.01306    target classifier: 0.49459    beta: 0.51259    kappa: 0.00946    rho: 0.01169    requests: 00
iter: 04499, precision source classifier: 0.01036    target classifier: 0.48604    beta: 0.51983    kappa: 0.00946    rho: 0.01159    requests: 00
iter: 04999, precision source classifier: 0.00856    target classifier: 0.48468    beta: 0.52025    kappa: 0.00360    rho: 0.00866    requests: 00
iter: 05499, precision source classifier: 0.00586    target classifier: 0.49189    beta: 0.51159    kappa: 0.00450    rho: 0.01010    requests: 00
iter: 05999, precision source classifier: 0.00541    target classifier: 0.49910    beta: 0.50412    kappa: 0.00586    rho: 0.00989    requests: 01
iter: 06499, precision source classifier: 0.01306    target classifier: 0.52252    beta: 0.48432    kappa: 0.00766    rho: 0.01183    requests: 01
iter: 06999, precision source classifier: 0.01396    target classifier: 0.52252    beta: 0.48476    kappa: 0.01216    rho: 0.01296    requests: 01
iter: 07499, precision source classifier: 0.01982    target classifier: 0.51892    beta: 0.49133    kappa: 0.01622    rho: 0.01581    requests: 01
iter: 07999, precision source classifier: 0.01577    target classifier: 0.51982    beta: 0.48839    kappa: 0.02252    rho: 0.02036    requests: 02
iter: 08499, precision source classifier: 0.01396    target classifier: 0.53063    beta: 0.47655    kappa: 0.03649    rho: 0.02636    requests: 02
iter: 08999, precision source classifier: 0.01171    target classifier: 0.53784    beta: 0.46818    kappa: 0.03378    rho: 0.02827    requests: 02
iter: 09499, precision source classifier: 0.00991    target classifier: 0.52432    beta: 0.48096    kappa: 0.03604    rho: 0.02956    requests: 02
iter: 09999, precision source classifier: 0.01171    target classifier: 0.53694    beta: 0.46909    kappa: 0.03333    rho: 0.02795    requests: 03
iter: 10499, precision source classifier: 0.01036    target classifier: 0.55901    beta: 0.44617    kappa: 0.03018    rho: 0.02729    requests: 03
iter: 10999, precision source classifier: 0.00991    target classifier: 0.54955    beta: 0.45551    kappa: 0.04009    rho: 0.03204    requests: 03
iter: 11499, precision source classifier: 0.00991    target classifier: 0.55811    beta: 0.44687    kappa: 0.03514    rho: 0.03052    requests: 03
iter: 11999, precision source classifier: 0.01126    target classifier: 0.55586    beta: 0.44976    kappa: 0.02973    rho: 0.02754    requests: 04
iter: 12499, precision source classifier: 0.01216    target classifier: 0.56216    beta: 0.44379    kappa: 0.02027    rho: 0.02160    requests: 04
iter: 12999, precision source classifier: 0.01577    target classifier: 0.55766    beta: 0.44999    kappa: 0.02883    rho: 0.02501    requests: 04
iter: 13499, precision source classifier: 0.01622    target classifier: 0.56216    beta: 0.44562    kappa: 0.02838    rho: 0.02404    requests: 04
iter: 13999, precision source classifier: 0.01441    target classifier: 0.55901    beta: 0.44800    kappa: 0.02793    rho: 0.02467    requests: 05
iter: 14499, precision source classifier: 0.01441    target classifier: 0.56441    beta: 0.44252    kappa: 0.03018    rho: 0.02249    requests: 05
iter: 14999, precision source classifier: 0.01622    target classifier: 0.55856    beta: 0.44928    kappa: 0.02973    rho: 0.02271    requests: 05
iter: 15499, precision source classifier: 0.01667    target classifier: 0.56802    beta: 0.43987    kappa: 0.02342    rho: 0.02050    requests: 05
iter: 15999, precision source classifier: 0.01712    target classifier: 0.57162    beta: 0.43641    kappa: 0.02297    rho: 0.02049    requests: 06
iter: 16499, precision source classifier: 0.01937    target classifier: 0.58378    beta: 0.42502    kappa: 0.02477    rho: 0.01861    requests: 06
iter: 16999, precision source classifier: 0.01847    target classifier: 0.57748    beta: 0.43105    kappa: 0.02342    rho: 0.01882    requests: 06
iter: 17499, precision source classifier: 0.01847    target classifier: 0.57658    beta: 0.43197    kappa: 0.02027    rho: 0.01808    requests: 06
iter: 17999, precision source classifier: 0.01577    target classifier: 0.58198    beta: 0.42530    kappa: 0.02072    rho: 0.01832    requests: 07
iter: 18499, precision source classifier: 0.01982    target classifier: 0.58874    beta: 0.42017    kappa: 0.01802    rho: 0.01826    requests: 07
iter: 18999, precision source classifier: 0.01982    target classifier: 0.58874    beta: 0.42017    kappa: 0.02342    rho: 0.02068    requests: 07
iter: 19499, precision source classifier: 0.02117    target classifier: 0.58829    beta: 0.42121    kappa: 0.02207    rho: 0.01873    requests: 07
iter: 19999, precision source classifier: 0.02342    target classifier: 0.58108    beta: 0.42955    kappa: 0.02523    rho: 0.01986    requests: 08
iter: 20499, precision source classifier: 0.02703    target classifier: 0.58694    beta: 0.42513    kappa: 0.02523    rho: 0.01950    requests: 08
iter: 20999, precision source classifier: 0.02342    target classifier: 0.58378    beta: 0.42679    kappa: 0.02613    rho: 0.01959    requests: 08
iter: 21499, precision source classifier: 0.02252    target classifier: 0.58694    beta: 0.42317    kappa: 0.02252    rho: 0.01924    requests: 08
iter: 21999, precision source classifier: 0.02252    target classifier: 0.59550    beta: 0.41442    kappa: 0.02883    rho: 0.02059    requests: 09
iter: 22499, precision source classifier: 0.02387    target classifier: 0.59234    beta: 0.41822    kappa: 0.02477    rho: 0.01949    requests: 09
iter: 22999, precision source classifier: 0.02477    target classifier: 0.60270    beta: 0.40800    kappa: 0.02973    rho: 0.02038    requests: 09
iter: 23499, precision source classifier: 0.02613    target classifier: 0.60495    beta: 0.40625    kappa: 0.03378    rho: 0.02096    requests: 09
iter: 23999, precision source classifier: 0.02703    target classifier: 0.60000    beta: 0.41172    kappa: 0.03649    rho: 0.02271    requests: 10
iter: 24499, precision source classifier: 0.03108    target classifier: 0.61622    beta: 0.39672    kappa: 0.03423    rho: 0.02014    requests: 10
iter: 24999, precision source classifier: 0.02973    target classifier: 0.61261    beta: 0.39988    kappa: 0.03333    rho: 0.02067    requests: 10
iter: 25499, precision source classifier: 0.03153    target classifier: 0.60541    beta: 0.40805    kappa: 0.03649    rho: 0.02204    requests: 10
iter: 25999, precision source classifier: 0.02748    target classifier: 0.61396    beta: 0.39756    kappa: 0.02883    rho: 0.01983    requests: 10
