{'method': 'Y_DAN', 'start_test': 1, 'policy': 'random', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': False, 'projection': True, 'tag': 'ActFT_False_Y_DAN_random_25_10_visda_1_standard', 'n_active_select': 25, 'output_path': 'snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_25_10_visda_1_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.13505    target classifier: 0.62102    beta: 0.43880    kappa: 0.12889    rho: 12.74205    requests: 00
iter: 01999, precision source classifier: 0.10011    target classifier: 0.64337    beta: 0.39697    kappa: 0.13695    rho: 24.65753    requests: 00
iter: 02999, precision source classifier: 0.09070    target classifier: 0.63179    beta: 0.40559    kappa: 0.04182    rho: 11.33636    requests: 00
iter: 03999, precision source classifier: 0.10119    target classifier: 0.65559    beta: 0.38387    kappa: 0.03286    rho: 8.59235    requests: 00
iter: 04999, precision source classifier: 0.09848    target classifier: 0.68157    beta: 0.35393    kappa: 0.04788    rho: 13.66210    requests: 00
iter: 05999, precision source classifier: 0.09033    target classifier: 0.67795    beta: 0.35474    kappa: 0.02408    rho: -11.81799    requests: 00
iter: 06999, precision source classifier: 0.09395    target classifier: 0.66247    beta: 0.37322    kappa: 0.02046    rho: -13.00210    requests: 00
iter: 07999, precision source classifier: 0.08662    target classifier: 0.66926    beta: 0.36280    kappa: 0.02951    rho: -12.87459    requests: 00
iter: 08999, precision source classifier: 0.07639    target classifier: 0.67252    beta: 0.35526    kappa: 0.03014    rho: -11.21607    requests: 00
iter: 09999, precision source classifier: 0.07667    target classifier: 0.67542    beta: 0.35224    kappa: 0.03521    rho: -11.77814    requests: 01
iter: 10999, precision source classifier: 0.08463    target classifier: 0.71669    beta: 0.31026    kappa: 0.04399    rho: 2.63147    requests: 01
iter: 11999, precision source classifier: 0.08861    target classifier: 0.71352    beta: 0.31508    kappa: 0.03965    rho: 0.90143    requests: 01
iter: 12999, precision source classifier: 0.08554    target classifier: 0.71651    beta: 0.31076    kappa: 0.03838    rho: -7.36876    requests: 01
iter: 13999, precision source classifier: 0.08182    target classifier: 0.69913    beta: 0.32841    kappa: 0.03358    rho: -4.80805    requests: 01
iter: 14999, precision source classifier: 0.07829    target classifier: 0.71705    beta: 0.30773    kappa: 0.03883    rho: -0.84888    requests: 02
iter: 15999, precision source classifier: 0.07015    target classifier: 0.71533    beta: 0.30689    kappa: 0.03657    rho: -0.92697    requests: 02
iter: 16999, precision source classifier: 0.07087    target classifier: 0.70737    beta: 0.31569    kappa: 0.04806    rho: -2.91739    requests: 02
iter: 17999, precision source classifier: 0.08173    target classifier: 0.69841    beta: 0.32917    kappa: 0.06273    rho: 0.54247    requests: 02
iter: 18999, precision source classifier: 0.07368    target classifier: 0.71171    beta: 0.31196    kappa: 0.05494    rho: -3.52426    requests: 02
iter: 19999, precision source classifier: 0.07169    target classifier: 0.70148    beta: 0.32230    kappa: 0.06092    rho: -5.71765    requests: 03
iter: 20999, precision source classifier: 0.07105    target classifier: 0.71189    beta: 0.31089    kappa: 0.06825    rho: -7.36647    requests: 03
iter: 21999, precision source classifier: 0.06825    target classifier: 0.70864    beta: 0.31344    kappa: 0.06644    rho: -8.71725    requests: 03
iter: 22999, precision source classifier: 0.06789    target classifier: 0.71877    beta: 0.30246    kappa: 0.06734    rho: -5.07349    requests: 03
iter: 23999, precision source classifier: 0.06843    target classifier: 0.71425    beta: 0.30749    kappa: 0.06128    rho: -7.66475    requests: 03
iter: 24999, precision source classifier: 0.07069    target classifier: 0.70411    beta: 0.31913    kappa: 0.06544    rho: -1.46512    requests: 04
iter: 25999, precision source classifier: 0.07368    target classifier: 0.71705    beta: 0.30620    kappa: 0.06861    rho: -1.93477    requests: 04
iter: 26999, precision source classifier: 0.07051    target classifier: 0.71379    beta: 0.30866    kappa: 0.06924    rho: 0.79344    requests: 04
iter: 27999, precision source classifier: 0.07603    target classifier: 0.72113    beta: 0.30258    kappa: 0.07223    rho: -1.99042    requests: 04
iter: 28999, precision source classifier: 0.07748    target classifier: 0.72484    beta: 0.29903    kappa: 0.07657    rho: 2.99857    requests: 04
iter: 29999, precision source classifier: 0.07911    target classifier: 0.71316    beta: 0.31223    kappa: 0.07992    rho: 3.94275    requests: 05
iter: 30999, precision source classifier: 0.08771    target classifier: 0.71388    beta: 0.31437    kappa: 0.08427    rho: 3.44456    requests: 05
iter: 31999, precision source classifier: 0.08662    target classifier: 0.71597    beta: 0.31172    kappa: 0.09024    rho: 4.21207    requests: 05
iter: 32999, precision source classifier: 0.08599    target classifier: 0.71986    beta: 0.30725    kappa: 0.08771    rho: 4.56609    requests: 05
iter: 33999, precision source classifier: 0.09287    target classifier: 0.71895    beta: 0.31058    kappa: 0.10310    rho: 5.97250    requests: 05
iter: 34999, precision source classifier: 0.09640    target classifier: 0.71262    beta: 0.31879    kappa: 0.10319    rho: 1.57577    requests: 06
iter: 35999, precision source classifier: 0.09377    target classifier: 0.71325    beta: 0.31717    kappa: 0.10047    rho: 3.10060    requests: 06
iter: 36999, precision source classifier: 0.08717    target classifier: 0.71253    beta: 0.31567    kappa: 0.09839    rho: 3.35395    requests: 06
iter: 37999, precision source classifier: 0.08472    target classifier: 0.70447    beta: 0.32362    kappa: 0.09730    rho: 1.93638    requests: 06
iter: 38999, precision source classifier: 0.08436    target classifier: 0.71416    beta: 0.31293    kappa: 0.09848    rho: 1.94243    requests: 06
iter: 39999, precision source classifier: 0.08554    target classifier: 0.71551    beta: 0.31185    kappa: 0.09205    rho: 0.50558    requests: 07
iter: 40999, precision source classifier: 0.08318    target classifier: 0.72629    beta: 0.29931    kappa: 0.09368    rho: 2.27210    requests: 07
iter: 41999, precision source classifier: 0.08535    target classifier: 0.72321    beta: 0.30338    kappa: 0.09341    rho: 1.02867    requests: 07
iter: 42999, precision source classifier: 0.08617    target classifier: 0.73796    beta: 0.28753    kappa: 0.09712    rho: 0.70121    requests: 07
iter: 43999, precision source classifier: 0.09015    target classifier: 0.72945    beta: 0.29813    kappa: 0.10681    rho: 1.76290    requests: 07
iter: 44999, precision source classifier: 0.09504    target classifier: 0.72954    beta: 0.29963    kappa: 0.10654    rho: 1.06221    requests: 08
iter: 45999, precision source classifier: 0.11522    target classifier: 0.72502    beta: 0.31157    kappa: 0.11595    rho: 1.19476    requests: 08
iter: 46999, precision source classifier: 0.11722    target classifier: 0.73027    beta: 0.30633    kappa: 0.12419    rho: 1.98770    requests: 08
iter: 47999, precision source classifier: 0.12437    target classifier: 0.73353    beta: 0.30511    kappa: 0.12799    rho: 2.52858    requests: 08
iter: 48999, precision source classifier: 0.12998    target classifier: 0.73416    beta: 0.30635    kappa: 0.12934    rho: 2.67070    requests: 08
iter: 49999, precision source classifier: 0.15034    target classifier: 0.72212    beta: 0.32784    kappa: 0.14645    rho: 2.41115    requests: 09
iter: 50999, precision source classifier: 0.14220    target classifier: 0.74620    beta: 0.29669    kappa: 0.15215    rho: 4.23489    requests: 09
iter: 51999, precision source classifier: 0.12925    target classifier: 0.74113    beta: 0.29810    kappa: 0.13333    rho: 4.33041    requests: 09
iter: 52999, precision source classifier: 0.12799    target classifier: 0.73307    beta: 0.30690    kappa: 0.12989    rho: 5.06958    requests: 09
iter: 53999, precision source classifier: 0.11966    target classifier: 0.73787    beta: 0.29856    kappa: 0.12925    rho: 3.97371    requests: 09
iter: 54999, precision source classifier: 0.11522    target classifier: 0.73995    beta: 0.29471    kappa: 0.12129    rho: 4.64945    requests: 10
iter: 55999, precision source classifier: 0.09622    target classifier: 0.74276    beta: 0.28542    kappa: 0.10599    rho: 4.28310    requests: 10
iter: 56999, precision source classifier: 0.09350    target classifier: 0.73896    beta: 0.28875    kappa: 0.10472    rho: 2.10919    requests: 10
iter: 57999, precision source classifier: 0.09785    target classifier: 0.73941    beta: 0.28964    kappa: 0.10735    rho: 1.46905    requests: 10
iter: 58999, precision source classifier: 0.09911    target classifier: 0.74792    beta: 0.28061    kappa: 0.11278    rho: 1.65746    requests: 10
iter: 59999, precision source classifier: 0.10074    target classifier: 0.73977    beta: 0.29017    kappa: 0.11206    rho: 1.82414    requests: 10
iter: 60999, precision source classifier: 0.09812    target classifier: 0.74041    beta: 0.28862    kappa: 0.10563    rho: 0.88214    requests: 10
iter: 61999, precision source classifier: 0.10092    target classifier: 0.74620    beta: 0.28309    kappa: 0.10626    rho: 0.89573    requests: 10
iter: 62999, precision source classifier: 0.10726    target classifier: 0.73805    beta: 0.29421    kappa: 0.11640    rho: 0.26549    requests: 10
iter: 63999, precision source classifier: 0.10654    target classifier: 0.73995    beta: 0.29185    kappa: 0.11025    rho: -0.60737    requests: 10
iter: 64999, precision source classifier: 0.10264    target classifier: 0.73443    beta: 0.29673    kappa: 0.09875    rho: -0.80391    requests: 10
iter: 65999, precision source classifier: 0.09848    target classifier: 0.73787    beta: 0.29155    kappa: 0.09459    rho: -2.61032    requests: 10
iter: 66999, precision source classifier: 0.09567    target classifier: 0.73172    beta: 0.29744    kappa: 0.09296    rho: -1.55463    requests: 10
iter: 67999, precision source classifier: 0.08726    target classifier: 0.73181    beta: 0.29460    kappa: 0.07694    rho: -3.05211    requests: 10
iter: 68999, precision source classifier: 0.08373    target classifier: 0.72982    beta: 0.29564    kappa: 0.06481    rho: -3.08013    requests: 10
iter: 69999, precision source classifier: 0.09124    target classifier: 0.73253    beta: 0.29510    kappa: 0.06870    rho: -3.23190    requests: 10
iter: 00999, precision source classifier: 0.08916    target classifier: 0.09278
iter: 01999, precision source classifier: 0.08255    target classifier: 0.15288
iter: 02999, precision source classifier: 0.09622    target classifier: 0.16917
iter: 03999, precision source classifier: 0.11531    target classifier: 0.14428
iter: 04999, precision source classifier: 0.09531    target classifier: 0.12174
iter: 05999, precision source classifier: 0.09776    target classifier: 0.17352
iter: 06999, precision source classifier: 0.08888    target classifier: 0.16781
iter: 07999, precision source classifier: 0.10038    target classifier: 0.15125
iter: 08999, precision source classifier: 0.10382    target classifier: 0.08825
iter: 09999, precision source classifier: 0.09757    target classifier: 0.17044
