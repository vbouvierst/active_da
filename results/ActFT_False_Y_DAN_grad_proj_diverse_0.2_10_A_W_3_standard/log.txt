{'method': 'Y_DAN', 'start_test': 1, 'policy': 'grad_proj_diverse', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': True, 'projection': True, 'tag': 'ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard', 'output_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard/history', 'path': {'source_path': '../data/office/amazon_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard/target.txt', 'test_path': '../data/office/webcam_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard/active.txt'}, 'n_active_select': 8, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': True, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 31}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'office', 'data': {'source': {'list_path': '../data/office/amazon_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_grad_proj_diverse_0.2_10_A_W_3_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/office/webcam_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.02551    target classifier: 0.83929    beta: 0.16578    kappa: 0.02041    rho: 0.02852    requests: 00
iter: 01999, precision source classifier: 0.03827    target classifier: 0.88520    beta: 0.12028    kappa: 0.06122    rho: 0.05755    requests: 00
iter: 02999, precision source classifier: 0.00510    target classifier: 0.90816    beta: 0.09322    kappa: 0.00765    rho: 0.02487    requests: 00
iter: 03999, precision source classifier: 0.01531    target classifier: 0.91071    beta: 0.09160    kappa: 0.01531    rho: 0.02299    requests: 00
iter: 04999, precision source classifier: 0.03571    target classifier: 0.92602    beta: 0.07768    kappa: 0.03571    rho: 0.02435    requests: 00
iter: 05999, precision source classifier: 0.03316    target classifier: 0.91327    beta: 0.09065    kappa: 0.03316    rho: 0.02110    requests: 00
iter: 06999, precision source classifier: 0.03571    target classifier: 0.91071    beta: 0.09353    kappa: 0.03571    rho: 0.02216    requests: 00
iter: 07999, precision source classifier: 0.03316    target classifier: 0.91327    beta: 0.09065    kappa: 0.03571    rho: 0.02242    requests: 00
iter: 08999, precision source classifier: 0.01786    target classifier: 0.92347    beta: 0.07886    kappa: 0.02041    rho: 0.02056    requests: 00
iter: 09999, precision source classifier: 0.01276    target classifier: 0.92602    beta: 0.07587    kappa: 0.01786    rho: 0.02254    requests: 01
iter: 10999, precision source classifier: 0.02551    target classifier: 0.93367    beta: 0.06902    kappa: 0.02551    rho: 0.02254    requests: 01
iter: 11999, precision source classifier: 0.01786    target classifier: 0.94133    beta: 0.06070    kappa: 0.02296    rho: 0.02224    requests: 01
iter: 12999, precision source classifier: 0.02551    target classifier: 0.93878    beta: 0.06379    kappa: 0.02806    rho: 0.02536    requests: 01
iter: 13999, precision source classifier: 0.02551    target classifier: 0.94133    beta: 0.06117    kappa: 0.02551    rho: 0.02554    requests: 01
iter: 14999, precision source classifier: 0.02041    target classifier: 0.93878    beta: 0.06346    kappa: 0.02041    rho: 0.02619    requests: 02
iter: 15999, precision source classifier: 0.01786    target classifier: 0.94133    beta: 0.06070    kappa: 0.01786    rho: 0.02477    requests: 02
iter: 16999, precision source classifier: 0.02041    target classifier: 0.93878    beta: 0.06346    kappa: 0.02041    rho: 0.02694    requests: 02
iter: 17999, precision source classifier: 0.01531    target classifier: 0.94388    beta: 0.05795    kappa: 0.01531    rho: 0.02735    requests: 02
iter: 18999, precision source classifier: 0.01786    target classifier: 0.94643    beta: 0.05551    kappa: 0.01531    rho: 0.02827    requests: 02
iter: 19999, precision source classifier: 0.01531    target classifier: 0.94388    beta: 0.05795    kappa: 0.01276    rho: 0.03035    requests: 03
iter: 20999, precision source classifier: 0.02551    target classifier: 0.94643    beta: 0.05594    kappa: 0.02551    rho: 0.03372    requests: 03
iter: 21999, precision source classifier: 0.04592    target classifier: 0.94643    beta: 0.05714    kappa: 0.04592    rho: 0.03613    requests: 03
iter: 22999, precision source classifier: 0.04082    target classifier: 0.96173    beta: 0.04089    kappa: 0.04337    rho: 0.03676    requests: 03
iter: 23999, precision source classifier: 0.05867    target classifier: 0.94898    beta: 0.05520    kappa: 0.06122    rho: 0.03935    requests: 03
iter: 24999, precision source classifier: 0.05612    target classifier: 0.94388    beta: 0.06045    kappa: 0.06122    rho: 0.04130    requests: 04
iter: 25999, precision source classifier: 0.05612    target classifier: 0.96173    beta: 0.04156    kappa: 0.06633    rho: 0.04247    requests: 04
iter: 26999, precision source classifier: 0.05612    target classifier: 0.96173    beta: 0.04156    kappa: 0.05612    rho: 0.04250    requests: 04
iter: 27999, precision source classifier: 0.05102    target classifier: 0.95663    beta: 0.04670    kappa: 0.05867    rho: 0.04237    requests: 04
iter: 28999, precision source classifier: 0.04847    target classifier: 0.96173    beta: 0.04122    kappa: 0.05357    rho: 0.04306    requests: 04
iter: 29999, precision source classifier: 0.04337    target classifier: 0.95918    beta: 0.04367    kappa: 0.04592    rho: 0.04107    requests: 05
iter: 30999, precision source classifier: 0.04592    target classifier: 0.97194    beta: 0.03043    kappa: 0.04847    rho: 0.04234    requests: 05
iter: 31999, precision source classifier: 0.04337    target classifier: 0.97704    beta: 0.02502    kappa: 0.04337    rho: 0.04208    requests: 05
iter: 32999, precision source classifier: 0.04337    target classifier: 0.96939    beta: 0.03301    kappa: 0.04337    rho: 0.04255    requests: 05
iter: 33999, precision source classifier: 0.04082    target classifier: 0.98214    beta: 0.01964    kappa: 0.04082    rho: 0.04105    requests: 05
iter: 34999, precision source classifier: 0.04337    target classifier: 0.97449    beta: 0.02768    kappa: 0.04337    rho: 0.04251    requests: 06
iter: 35999, precision source classifier: 0.04337    target classifier: 0.97449    beta: 0.02768    kappa: 0.04082    rho: 0.03932    requests: 06
iter: 36999, precision source classifier: 0.03827    target classifier: 0.97704    beta: 0.02489    kappa: 0.03827    rho: 0.03998    requests: 06
iter: 37999, precision source classifier: 0.04337    target classifier: 0.98214    beta: 0.01969    kappa: 0.04082    rho: 0.04113    requests: 06
iter: 38999, precision source classifier: 0.04082    target classifier: 0.97449    beta: 0.02761    kappa: 0.03827    rho: 0.04021    requests: 06
iter: 39999, precision source classifier: 0.05612    target classifier: 0.97704    beta: 0.02536    kappa: 0.05357    rho: 0.04020    requests: 07
iter: 40999, precision source classifier: 0.03827    target classifier: 0.97194    beta: 0.03019    kappa: 0.03571    rho: 0.04062    requests: 07
iter: 41999, precision source classifier: 0.03827    target classifier: 0.97449    beta: 0.02754    kappa: 0.03571    rho: 0.03825    requests: 07
iter: 42999, precision source classifier: 0.04592    target classifier: 0.97449    beta: 0.02776    kappa: 0.04592    rho: 0.03795    requests: 07
iter: 43999, precision source classifier: 0.03316    target classifier: 0.97449    beta: 0.02739    kappa: 0.03316    rho: 0.03789    requests: 07
iter: 44999, precision source classifier: 0.03571    target classifier: 0.97449    beta: 0.02746    kappa: 0.03316    rho: 0.03698    requests: 08
iter: 45999, precision source classifier: 0.04082    target classifier: 0.97959    beta: 0.02230    kappa: 0.04082    rho: 0.03715    requests: 08
iter: 46999, precision source classifier: 0.03827    target classifier: 0.97449    beta: 0.02754    kappa: 0.03571    rho: 0.03750    requests: 08
iter: 47999, precision source classifier: 0.03571    target classifier: 0.97449    beta: 0.02746    kappa: 0.03316    rho: 0.03775    requests: 08
iter: 48999, precision source classifier: 0.03316    target classifier: 0.97449    beta: 0.02739    kappa: 0.03061    rho: 0.03734    requests: 08
iter: 49999, precision source classifier: 0.03316    target classifier: 0.98214    beta: 0.01948    kappa: 0.03316    rho: 0.03758    requests: 09
iter: 50999, precision source classifier: 0.02296    target classifier: 0.97959    beta: 0.02189    kappa: 0.02296    rho: 0.03646    requests: 09
iter: 51999, precision source classifier: 0.01786    target classifier: 0.97959    beta: 0.02178    kappa: 0.01531    rho: 0.03673    requests: 09
iter: 52999, precision source classifier: 0.02296    target classifier: 0.97449    beta: 0.02711    kappa: 0.02041    rho: 0.03703    requests: 09
iter: 53999, precision source classifier: 0.02296    target classifier: 0.97194    beta: 0.02971    kappa: 0.02041    rho: 0.03699    requests: 09
iter: 54999, precision source classifier: 0.03316    target classifier: 0.97449    beta: 0.02739    kappa: 0.03316    rho: 0.03857    requests: 10
iter: 55999, precision source classifier: 0.03061    target classifier: 0.98469    beta: 0.01680    kappa: 0.03061    rho: 0.03898    requests: 10
iter: 56999, precision source classifier: 0.03061    target classifier: 0.97959    beta: 0.02206    kappa: 0.03061    rho: 0.03879    requests: 10
iter: 57999, precision source classifier: 0.04082    target classifier: 0.97959    beta: 0.02230    kappa: 0.03827    rho: 0.03906    requests: 10
iter: 58999, precision source classifier: 0.03827    target classifier: 0.98214    beta: 0.01959    kappa: 0.03827    rho: 0.03852    requests: 10
iter: 59999, precision source classifier: 0.03061    target classifier: 0.98214    beta: 0.01943    kappa: 0.03061    rho: 0.03858    requests: 10
iter: 60999, precision source classifier: 0.03316    target classifier: 0.97959    beta: 0.02212    kappa: 0.03316    rho: 0.03720    requests: 10
iter: 61999, precision source classifier: 0.03061    target classifier: 0.98214    beta: 0.01943    kappa: 0.03316    rho: 0.03843    requests: 10
iter: 62999, precision source classifier: 0.02041    target classifier: 0.97704    beta: 0.02443    kappa: 0.02296    rho: 0.03765    requests: 10
iter: 63999, precision source classifier: 0.02296    target classifier: 0.97194    beta: 0.02971    kappa: 0.02551    rho: 0.03672    requests: 10
iter: 64999, precision source classifier: 0.03061    target classifier: 0.96684    beta: 0.03521    kappa: 0.03061    rho: 0.03656    requests: 10
iter: 65999, precision source classifier: 0.02806    target classifier: 0.96939    beta: 0.03249    kappa: 0.03061    rho: 0.03643    requests: 10
iter: 66999, precision source classifier: 0.02041    target classifier: 0.97959    beta: 0.02183    kappa: 0.02296    rho: 0.03556    requests: 10
iter: 67999, precision source classifier: 0.02551    target classifier: 0.98214    beta: 0.01933    kappa: 0.02551    rho: 0.03531    requests: 10
iter: 68999, precision source classifier: 0.02041    target classifier: 0.98214    beta: 0.01923    kappa: 0.02041    rho: 0.03577    requests: 10
iter: 69999, precision source classifier: 0.02041    target classifier: 0.97959    beta: 0.02183    kappa: 0.02041    rho: 0.03559    requests: 10
iter: 00999, precision source classifier: 0.03827    target classifier: 0.59694
iter: 01999, precision source classifier: 0.03316    target classifier: 0.60459
iter: 02999, precision source classifier: 0.03827    target classifier: 0.61224
iter: 03999, precision source classifier: 0.03061    target classifier: 0.61480
iter: 04999, precision source classifier: 0.03061    target classifier: 0.62755
iter: 05999, precision source classifier: 0.02806    target classifier: 0.62245
iter: 06999, precision source classifier: 0.02806    target classifier: 0.62500
iter: 07999, precision source classifier: 0.02806    target classifier: 0.62755
iter: 08999, precision source classifier: 0.02806    target classifier: 0.62500
iter: 09999, precision source classifier: 0.02806    target classifier: 0.63265
