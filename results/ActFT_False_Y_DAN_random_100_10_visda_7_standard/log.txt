{'method': 'Y_DAN', 'start_test': 1, 'policy': 'random', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': False, 'projection': True, 'tag': 'ActFT_False_Y_DAN_random_100_10_visda_7_standard', 'n_active_select': 100, 'output_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_random_100_10_visda_7_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.03286    target classifier: 0.58608    beta: 0.42857    kappa: 0.08626    rho: 37.86202    requests: 00
iter: 01999, precision source classifier: 0.03358    target classifier: 0.64292    beta: 0.37014    kappa: 0.00262    rho: -22.80566    requests: 00
iter: 02999, precision source classifier: 0.05992    target classifier: 0.63622    beta: 0.38761    kappa: 0.04327    rho: -8.26906    requests: 00
iter: 03999, precision source classifier: 0.06182    target classifier: 0.63143    beta: 0.39351    kappa: 0.08970    rho: -3.76394    requests: 00
iter: 04999, precision source classifier: 0.06037    target classifier: 0.66564    beta: 0.35653    kappa: 0.05404    rho: 10.47303    requests: 00
iter: 05999, precision source classifier: 0.05856    target classifier: 0.66854    beta: 0.35277    kappa: 0.05892    rho: 14.76020    requests: 00
iter: 06999, precision source classifier: 0.06689    target classifier: 0.65903    beta: 0.36609    kappa: 0.06508    rho: 19.45360    requests: 00
iter: 07999, precision source classifier: 0.06571    target classifier: 0.65460    beta: 0.37037    kappa: 0.07476    rho: 22.43876    requests: 00
iter: 08999, precision source classifier: 0.06598    target classifier: 0.66003    beta: 0.36467    kappa: 0.06897    rho: 16.16128    requests: 00
iter: 09999, precision source classifier: 0.09305    target classifier: 0.66501    beta: 0.37006    kappa: 0.10454    rho: 13.80111    requests: 01
iter: 10999, precision source classifier: 0.10409    target classifier: 0.72393    beta: 0.30891    kappa: 0.10798    rho: -6.08110    requests: 01
iter: 11999, precision source classifier: 0.08499    target classifier: 0.73534    beta: 0.29002    kappa: 0.08391    rho: -2.81139    requests: 01
iter: 12999, precision source classifier: 0.10219    target classifier: 0.73850    beta: 0.29205    kappa: 0.10672    rho: -0.22671    requests: 01
iter: 13999, precision source classifier: 0.11767    target classifier: 0.73959    beta: 0.29594    kappa: 0.14356    rho: -4.44938    requests: 01
iter: 14999, precision source classifier: 0.11568    target classifier: 0.73914    beta: 0.29578    kappa: 0.13803    rho: -3.31220    requests: 02
iter: 15999, precision source classifier: 0.09993    target classifier: 0.75299    beta: 0.27524    kappa: 0.10273    rho: -7.83972    requests: 02
iter: 16999, precision source classifier: 0.11568    target classifier: 0.75489    beta: 0.27799    kappa: 0.12437    rho: -2.90963    requests: 02
iter: 17999, precision source classifier: 0.10391    target classifier: 0.75987    beta: 0.26880    kappa: 0.10663    rho: 1.22233    requests: 02
iter: 18999, precision source classifier: 0.10319    target classifier: 0.76349    beta: 0.26455    kappa: 0.09857    rho: 0.80698    requests: 02
iter: 19999, precision source classifier: 0.10726    target classifier: 0.76041    beta: 0.26920    kappa: 0.09911    rho: 1.58112    requests: 03
iter: 20999, precision source classifier: 0.11061    target classifier: 0.77199    beta: 0.25720    kappa: 0.09097    rho: 1.30783    requests: 03
iter: 21999, precision source classifier: 0.09929    target classifier: 0.77046    beta: 0.25568    kappa: 0.08391    rho: 2.66924    requests: 03
iter: 22999, precision source classifier: 0.09051    target classifier: 0.76575    beta: 0.25838    kappa: 0.08155    rho: 3.28947    requests: 03
iter: 23999, precision source classifier: 0.08807    target classifier: 0.76665    beta: 0.25670    kappa: 0.08427    rho: 3.21838    requests: 03
iter: 24999, precision source classifier: 0.05730    target classifier: 0.76177    beta: 0.25350    kappa: 0.06463    rho: 1.67473    requests: 04
iter: 25999, precision source classifier: 0.07024    target classifier: 0.78041    beta: 0.23700    kappa: 0.07811    rho: -1.10007    requests: 04
iter: 26999, precision source classifier: 0.07775    target classifier: 0.77625    beta: 0.24343    kappa: 0.09033    rho: -0.29064    requests: 04
iter: 27999, precision source classifier: 0.06571    target classifier: 0.77462    beta: 0.24204    kappa: 0.07196    rho: -2.45074    requests: 04
iter: 28999, precision source classifier: 0.07712    target classifier: 0.77227    beta: 0.24758    kappa: 0.09251    rho: -1.63633    requests: 04
iter: 29999, precision source classifier: 0.06816    target classifier: 0.77543    beta: 0.24180    kappa: 0.08092    rho: -2.29461    requests: 05
iter: 30999, precision source classifier: 0.07069    target classifier: 0.76195    beta: 0.25696    kappa: 0.07359    rho: -6.35082    requests: 05
iter: 31999, precision source classifier: 0.08273    target classifier: 0.76557    beta: 0.25639    kappa: 0.08816    rho: -4.61447    requests: 05
iter: 32999, precision source classifier: 0.07332    target classifier: 0.76819    beta: 0.25095    kappa: 0.07811    rho: -5.02762    requests: 05
iter: 33999, precision source classifier: 0.06363    target classifier: 0.76213    beta: 0.25483    kappa: 0.06227    rho: -6.40959    requests: 05
iter: 34999, precision source classifier: 0.08029    target classifier: 0.76819    beta: 0.25285    kappa: 0.08237    rho: -5.35262    requests: 06
iter: 35999, precision source classifier: 0.09178    target classifier: 0.77326    beta: 0.25048    kappa: 0.09015    rho: -7.69322    requests: 06
iter: 36999, precision source classifier: 0.10029    target classifier: 0.76783    beta: 0.25887    kappa: 0.09712    rho: -8.65779    requests: 06
iter: 37999, precision source classifier: 0.10083    target classifier: 0.77489    beta: 0.25119    kappa: 0.09649    rho: -7.35891    requests: 06
iter: 38999, precision source classifier: 0.11767    target classifier: 0.78041    beta: 0.24972    kappa: 0.11685    rho: -6.47333    requests: 06
iter: 39999, precision source classifier: 0.10301    target classifier: 0.76919    beta: 0.25814    kappa: 0.09531    rho: -6.78078    requests: 07
iter: 40999, precision source classifier: 0.11106    target classifier: 0.78041    beta: 0.24787    kappa: 0.11025    rho: -4.27849    requests: 07
iter: 41999, precision source classifier: 0.11984    target classifier: 0.77371    beta: 0.25794    kappa: 0.12609    rho: -3.28793    requests: 07
iter: 42999, precision source classifier: 0.11939    target classifier: 0.78041    beta: 0.25021    kappa: 0.13478    rho: -1.26370    requests: 07
iter: 43999, precision source classifier: 0.11713    target classifier: 0.78693    beta: 0.24220    kappa: 0.12934    rho: -0.93574    requests: 07
iter: 44999, precision source classifier: 0.11676    target classifier: 0.78141    beta: 0.24834    kappa: 0.12618    rho: -0.53424    requests: 08
iter: 45999, precision source classifier: 0.11432    target classifier: 0.78847    beta: 0.23969    kappa: 0.12328    rho: -0.17425    requests: 08
iter: 46999, precision source classifier: 0.12129    target classifier: 0.78548    beta: 0.24499    kappa: 0.12473    rho: -0.14551    requests: 08
iter: 47999, precision source classifier: 0.10744    target classifier: 0.77480    beta: 0.25314    kappa: 0.12889    rho: 2.84208    requests: 08
iter: 48999, precision source classifier: 0.09622    target classifier: 0.78892    beta: 0.23440    kappa: 0.11830    rho: 3.19890    requests: 08
iter: 49999, precision source classifier: 0.08789    target classifier: 0.78313    beta: 0.23860    kappa: 0.10436    rho: 2.66332    requests: 09
iter: 50999, precision source classifier: 0.07703    target classifier: 0.78974    beta: 0.22865    kappa: 0.08735    rho: 2.22993    requests: 09
iter: 51999, precision source classifier: 0.06517    target classifier: 0.78458    beta: 0.23126    kappa: 0.07187    rho: 3.61106    requests: 09
iter: 52999, precision source classifier: 0.05539    target classifier: 0.78965    beta: 0.22351    kappa: 0.06083    rho: 2.38170    requests: 09
iter: 53999, precision source classifier: 0.03557    target classifier: 0.78050    beta: 0.22839    kappa: 0.04028    rho: 4.62228    requests: 09
iter: 54999, precision source classifier: 0.02209    target classifier: 0.78992    beta: 0.21563    kappa: 0.02833    rho: 3.47081    requests: 10
iter: 55999, precision source classifier: 0.01919    target classifier: 0.78575    beta: 0.21923    kappa: 0.02679    rho: 3.85391    requests: 10
iter: 56999, precision source classifier: 0.02254    target classifier: 0.79200    beta: 0.21360    kappa: 0.02942    rho: 2.72632    requests: 10
iter: 57999, precision source classifier: 0.02516    target classifier: 0.79435    beta: 0.21176    kappa: 0.03612    rho: 3.52680    requests: 10
iter: 58999, precision source classifier: 0.02842    target classifier: 0.78304    beta: 0.22411    kappa: 0.03946    rho: 3.64236    requests: 10
iter: 59999, precision source classifier: 0.02824    target classifier: 0.79037    beta: 0.21653    kappa: 0.03783    rho: 2.31696    requests: 10
iter: 60999, precision source classifier: 0.02399    target classifier: 0.78286    beta: 0.22328    kappa: 0.03403    rho: 2.42881    requests: 10
iter: 61999, precision source classifier: 0.02661    target classifier: 0.79155    beta: 0.21496    kappa: 0.03313    rho: 0.89784    requests: 10
iter: 62999, precision source classifier: 0.02724    target classifier: 0.78937    beta: 0.21733    kappa: 0.03802    rho: 1.01427    requests: 10
iter: 63999, precision source classifier: 0.02381    target classifier: 0.79363    beta: 0.21221    kappa: 0.03403    rho: 0.64367    requests: 10
iter: 64999, precision source classifier: 0.02779    target classifier: 0.79290    beta: 0.21382    kappa: 0.03829    rho: 1.29254    requests: 10
iter: 65999, precision source classifier: 0.03458    target classifier: 0.79191    beta: 0.21636    kappa: 0.04562    rho: 1.73684    requests: 10
iter: 66999, precision source classifier: 0.03530    target classifier: 0.78811    beta: 0.22046    kappa: 0.04453    rho: 2.18379    requests: 10
iter: 67999, precision source classifier: 0.04499    target classifier: 0.78593    beta: 0.22496    kappa: 0.04987    rho: 0.04598    requests: 10
iter: 68999, precision source classifier: 0.05449    target classifier: 0.79037    beta: 0.22253    kappa: 0.05992    rho: 0.13285    requests: 10
iter: 69999, precision source classifier: 0.05775    target classifier: 0.79354    beta: 0.21994    kappa: 0.05802    rho: -0.76215    requests: 10
iter: 00999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 01999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 02999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 03999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 04999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 05999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 06999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 07999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 08999, precision source classifier: 0.06562    target classifier: 0.06562
iter: 09999, precision source classifier: 0.06562    target classifier: 0.06562
