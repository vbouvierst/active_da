{'method': 'Y_DAN', 'start_test': 1, 'policy': 'entropy', 'weight_policy': False, 'requests': 10, 'min_iter_request': 9000, 'max_iter_request': 5000, 'iter_last_request': 10000, 'ratio_active': 0.2, 'ramp_up_trans': 10000, 'compute_grad': False, 'projection': True, 'tag': 'ActFT_False_Y_DAN_entropy_100_10_visda_2_standard', 'n_active_select': 100, 'output_path': 'snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard', 'out_file': <_io.TextIOWrapper name='snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard/log.txt' mode='w' encoding='UTF-8'>, 'path_target_prediction': 'snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard/target_prediction', 'history_path': 'snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard/history', 'path': {'source_path': '../data/visda-2017/train_list_local_fusion.txt', 'target_path': 'snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard/target.txt', 'test_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'active_path': 'snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard/active.txt'}, 'gpu': '0', 'num_iterations': 111000, 'num_baseline_iterations': 10004, 'test_interval': 1000, 'output_for_test': True, 'prep': {'test_10crop': False, 'params': {'resize_size': 256, 'crop_size': 224, 'alexnet': False}}, 'loss': {'trade_off': 1.0, 'random': True, 'random_dim': 1024}, 'network': {'name': <class 'network.ResNetFc'>, 'params': {'resnet_name': 'ResNet50', 'use_bottleneck': True, 'bottleneck_dim': 256, 'new_cls': True, 'two_classifiers': True, 'class_num': 12}}, 'optimizer': {'type': <class 'torch.optim.sgd.SGD'>, 'optim_params': {'lr': 0.001, 'momentum': 0.9, 'weight_decay': 0.0005, 'nesterov': True}, 'lr_type': 'inv', 'lr_param': {'lr': 0.001, 'gamma': 0.001, 'power': 0.75}}, 'dataset': 'visda', 'data': {'source': {'list_path': '../data/visda-2017/train_list_local_fusion.txt', 'batch_size': 32}, 'target': {'list_path': 'snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard/target.txt', 'batch_size': 32}, 'active': {'list_path': 'snapshot/ActFT_False_Y_DAN_entropy_100_10_visda_2_standard/active.txt', 'batch_size': 2}, 'test': {'list_path': '../data/visda-2017/validation_list_local_test_fusion.txt', 'batch_size': 4}}}iter: 00999, precision source classifier: 0.09776    target classifier: 0.61043    beta: 0.43241    kappa: 0.07848    rho: 3.95240    requests: 00
iter: 01999, precision source classifier: 0.09567    target classifier: 0.64048    beta: 0.39822    kappa: 0.15261    rho: 1.35031    requests: 00
iter: 02999, precision source classifier: 0.07386    target classifier: 0.65496    beta: 0.37323    kappa: 0.13369    rho: -4.34212    requests: 00
iter: 03999, precision source classifier: 0.05069    target classifier: 0.65559    beta: 0.36347    kappa: 0.11631    rho: 9.47738    requests: 00
iter: 04999, precision source classifier: 0.05286    target classifier: 0.65641    beta: 0.36344    kappa: 0.08834    rho: 1.59149    requests: 00
iter: 05999, precision source classifier: 0.05196    target classifier: 0.68601    beta: 0.33191    kappa: 0.06436    rho: -1.38227    requests: 00
iter: 06999, precision source classifier: 0.06236    target classifier: 0.66990    beta: 0.35275    kappa: 0.06951    rho: 8.81515    requests: 00
iter: 07999, precision source classifier: 0.06698    target classifier: 0.66428    beta: 0.36050    kappa: 0.06535    rho: 6.56469    requests: 00
iter: 08999, precision source classifier: 0.05232    target classifier: 0.67098    beta: 0.34787    kappa: 0.05992    rho: 2.31208    requests: 00
iter: 09999, precision source classifier: 0.05005    target classifier: 0.67677    beta: 0.34095    kappa: 0.05938    rho: 14.71579    requests: 01
iter: 10999, precision source classifier: 0.05711    target classifier: 0.72049    beta: 0.29718    kappa: 0.07422    rho: -0.00444    requests: 01
iter: 11999, precision source classifier: 0.05223    target classifier: 0.70909    beta: 0.30767    kappa: 0.06852    rho: -0.42668    requests: 01
iter: 12999, precision source classifier: 0.05911    target classifier: 0.71877    beta: 0.29964    kappa: 0.05974    rho: 6.57346    requests: 01
iter: 13999, precision source classifier: 0.07069    target classifier: 0.72285    beta: 0.29899    kappa: 0.08680    rho: 2.48408    requests: 01
iter: 14999, precision source classifier: 0.08671    target classifier: 0.71741    beta: 0.31017    kappa: 0.09178    rho: 4.96130    requests: 02
iter: 15999, precision source classifier: 0.11722    target classifier: 0.73959    beta: 0.29578    kappa: 0.11903    rho: 7.35488    requests: 02
iter: 16999, precision source classifier: 0.11242    target classifier: 0.74041    beta: 0.29327    kappa: 0.12192    rho: 9.66831    requests: 02
iter: 17999, precision source classifier: 0.11206    target classifier: 0.73298    beta: 0.30150    kappa: 0.11993    rho: 14.71135    requests: 02
iter: 18999, precision source classifier: 0.13695    target classifier: 0.73144    beta: 0.31197    kappa: 0.15062    rho: 21.16962    requests: 02
iter: 19999, precision source classifier: 0.08979    target classifier: 0.72384    beta: 0.30417    kappa: 0.09214    rho: 24.49998    requests: 03
iter: 20999, precision source classifier: 0.13921    target classifier: 0.74556    beta: 0.29640    kappa: 0.14663    rho: 23.91618    requests: 03
iter: 21999, precision source classifier: 0.16365    target classifier: 0.75036    beta: 0.29932    kappa: 0.17795    rho: 24.48322    requests: 03
iter: 22999, precision source classifier: 0.17035    target classifier: 0.74665    beta: 0.30620    kappa: 0.20040    rho: 32.43432    requests: 03
iter: 23999, precision source classifier: 0.17053    target classifier: 0.73733    beta: 0.31750    kappa: 0.19660    rho: 28.53625    requests: 03
iter: 24999, precision source classifier: 0.17306    target classifier: 0.74593    beta: 0.30808    kappa: 0.18655    rho: 27.09741    requests: 04
iter: 25999, precision source classifier: 0.17171    target classifier: 0.75959    beta: 0.29110    kappa: 0.19515    rho: 28.39845    requests: 04
iter: 26999, precision source classifier: 0.17895    target classifier: 0.75606    beta: 0.29796    kappa: 0.21959    rho: 32.32607    requests: 04
iter: 27999, precision source classifier: 0.18103    target classifier: 0.75217    beta: 0.30346    kappa: 0.19750    rho: 28.57165    requests: 04
iter: 28999, precision source classifier: 0.17922    target classifier: 0.75199    beta: 0.30301    kappa: 0.19714    rho: 25.05080    requests: 04
iter: 29999, precision source classifier: 0.18076    target classifier: 0.75262    beta: 0.30281    kappa: 0.19814    rho: 24.56218    requests: 05
iter: 30999, precision source classifier: 0.18049    target classifier: 0.74955    beta: 0.30646    kappa: 0.22149    rho: 27.47876    requests: 05
iter: 31999, precision source classifier: 0.17913    target classifier: 0.74928    beta: 0.30628    kappa: 0.20999    rho: 24.45323    requests: 05
iter: 32999, precision source classifier: 0.17650    target classifier: 0.75244    beta: 0.30146    kappa: 0.23090    rho: 25.80418    requests: 05
iter: 33999, precision source classifier: 0.17125    target classifier: 0.75027    beta: 0.30217    kappa: 0.20212    rho: 22.73470    requests: 05
iter: 34999, precision source classifier: 0.15930    target classifier: 0.74937    beta: 0.29896    kappa: 0.20094    rho: 20.09662    requests: 06
iter: 35999, precision source classifier: 0.15713    target classifier: 0.74846    beta: 0.29926    kappa: 0.20266    rho: 21.99021    requests: 06
iter: 36999, precision source classifier: 0.15378    target classifier: 0.75434    beta: 0.29114    kappa: 0.17877    rho: 20.46949    requests: 06
iter: 37999, precision source classifier: 0.14193    target classifier: 0.75615    beta: 0.28501    kappa: 0.16130    rho: 17.95564    requests: 06
iter: 38999, precision source classifier: 0.12328    target classifier: 0.75253    beta: 0.28308    kappa: 0.14727    rho: 15.06833    requests: 06
iter: 39999, precision source classifier: 0.09920    target classifier: 0.75226    beta: 0.27582    kappa: 0.10744    rho: 11.71700    requests: 07
iter: 40999, precision source classifier: 0.07621    target classifier: 0.76421    beta: 0.25605    kappa: 0.08599    rho: 12.48514    requests: 07
iter: 41999, precision source classifier: 0.09133    target classifier: 0.76901    beta: 0.25503    kappa: 0.10165    rho: 13.79027    requests: 07
iter: 42999, precision source classifier: 0.09079    target classifier: 0.76484    beta: 0.25945    kappa: 0.09893    rho: 11.04581    requests: 07
iter: 43999, precision source classifier: 0.08472    target classifier: 0.76195    beta: 0.26089    kappa: 0.09395    rho: 9.24631    requests: 07
iter: 44999, precision source classifier: 0.09350    target classifier: 0.76484    beta: 0.26023    kappa: 0.10753    rho: 11.77038    requests: 08
iter: 45999, precision source classifier: 0.09260    target classifier: 0.76104    beta: 0.26415    kappa: 0.10210    rho: 5.99469    requests: 08
iter: 46999, precision source classifier: 0.08834    target classifier: 0.75661    beta: 0.26778    kappa: 0.10047    rho: 10.38697    requests: 08
iter: 47999, precision source classifier: 0.08581    target classifier: 0.75742    beta: 0.26615    kappa: 0.09404    rho: 7.97683    requests: 08
iter: 48999, precision source classifier: 0.08192    target classifier: 0.76684    beta: 0.25478    kappa: 0.08391    rho: 9.84706    requests: 08
iter: 49999, precision source classifier: 0.07395    target classifier: 0.75715    beta: 0.26304    kappa: 0.07730    rho: 9.31621    requests: 09
iter: 50999, precision source classifier: 0.07793    target classifier: 0.76865    beta: 0.25172    kappa: 0.08427    rho: 9.12816    requests: 09
iter: 51999, precision source classifier: 0.07721    target classifier: 0.76140    beta: 0.25936    kappa: 0.08228    rho: 11.73689    requests: 09
iter: 52999, precision source classifier: 0.07033    target classifier: 0.76340    beta: 0.25530    kappa: 0.07395    rho: 11.29568    requests: 09
iter: 53999, precision source classifier: 0.07268    target classifier: 0.76457    beta: 0.25468    kappa: 0.07567    rho: 10.61454    requests: 09
iter: 54999, precision source classifier: 0.06508    target classifier: 0.76457    beta: 0.25261    kappa: 0.06390    rho: 10.30713    requests: 10
iter: 55999, precision source classifier: 0.07458    target classifier: 0.77534    beta: 0.24358    kappa: 0.06997    rho: 8.07809    requests: 10
iter: 56999, precision source classifier: 0.08418    target classifier: 0.77254    beta: 0.24919    kappa: 0.08074    rho: 9.34894    requests: 10
iter: 57999, precision source classifier: 0.10156    target classifier: 0.77181    beta: 0.25481    kappa: 0.09187    rho: 7.67844    requests: 10
iter: 58999, precision source classifier: 0.12038    target classifier: 0.77706    beta: 0.25429    kappa: 0.11188    rho: 7.32998    requests: 10
iter: 59999, precision source classifier: 0.10825    target classifier: 0.77281    beta: 0.25561    kappa: 0.10590    rho: 6.25588    requests: 10
iter: 60999, precision source classifier: 0.11151    target classifier: 0.77236    beta: 0.25705    kappa: 0.11097    rho: 4.81912    requests: 10
iter: 61999, precision source classifier: 0.11151    target classifier: 0.77507    beta: 0.25400    kappa: 0.10672    rho: 3.54402    requests: 10
iter: 62999, precision source classifier: 0.11622    target classifier: 0.77344    beta: 0.25719    kappa: 0.11360    rho: 3.82036    requests: 10
iter: 63999, precision source classifier: 0.11314    target classifier: 0.77046    beta: 0.25966    kappa: 0.11160    rho: 2.58919    requests: 10
iter: 64999, precision source classifier: 0.11197    target classifier: 0.77181    beta: 0.25779    kappa: 0.11405    rho: 2.92681    requests: 10
iter: 65999, precision source classifier: 0.10690    target classifier: 0.77444    beta: 0.25340    kappa: 0.11396    rho: 1.86321    requests: 10
iter: 66999, precision source classifier: 0.10545    target classifier: 0.77552    beta: 0.25177    kappa: 0.11758    rho: 1.68762    requests: 10
iter: 67999, precision source classifier: 0.10798    target classifier: 0.77109    beta: 0.25745    kappa: 0.12020    rho: 1.76879    requests: 10
iter: 68999, precision source classifier: 0.10599    target classifier: 0.77371    beta: 0.25395    kappa: 0.11541    rho: 1.60074    requests: 10
iter: 69999, precision source classifier: 0.10672    target classifier: 0.77218    beta: 0.25587    kappa: 0.11531    rho: 0.14004    requests: 10
iter: 00999, precision source classifier: 0.07929    target classifier: 0.17786
iter: 01999, precision source classifier: 0.08264    target classifier: 0.18736
iter: 02999, precision source classifier: 0.08110    target classifier: 0.17904
iter: 03999, precision source classifier: 0.08327    target classifier: 0.18700
iter: 04999, precision source classifier: 0.08409    target classifier: 0.18755
iter: 05999, precision source classifier: 0.08327    target classifier: 0.10047
iter: 06999, precision source classifier: 0.08418    target classifier: 0.18745
iter: 07999, precision source classifier: 0.08273    target classifier: 0.18745
iter: 08999, precision source classifier: 0.08354    target classifier: 0.18619
iter: 09999, precision source classifier: 0.08192    target classifier: 0.19316
