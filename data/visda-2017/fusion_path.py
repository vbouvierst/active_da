datasets = ['train_list_local.txt', 'validation_list_local.txt', 'test_list_local.txt']

def get_fusion_name(file):
    new_file = file[:-4] + '_fusion.txt'
    return new_file

for dataset in datasets:
    with open(dataset, 'r') as f:
        with open(get_fusion_name(dataset), 'w') as new_f:
            for line in f:
                new_f.write('/gpfs/workdir/bouvierv' + line[2:])