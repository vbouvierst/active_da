import numpy as np
import pandas as pd

datasets = ['clipart_train_local.txt', 'clipart_test_local.txt',
            'infograph_train_local.txt', 'infograph_test_local.txt',
            'painting_train_local.txt', 'painting_test_local.txt',
            'quickdraw_train_local.txt', 'quickdraw_test_local.txt',
            'real_train_local.txt', 'real_test_local.txt',
            'sketch_train_local.txt', 'sketch_test_local.txt']


labels_tracker = {}

for dataset in datasets:
    with open(dataset, 'r') as f:
        for line in f:
            label = line.split()[1]
            try:
                labels_tracker[label] += 1
            except:
                labels_tracker[label] = 1

print(labels_tracker)
