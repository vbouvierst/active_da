import numpy as np


datasets = ['clipart_126_local.txt',
            'painting_126_local.txt',
            'real_126_local.txt',
            'sketch_126_local.txt'
            ]


def get_local_name(file):
    new_file = file[:-4] + '_local.txt'
    return new_file


def get_dataset_name(dataset):
    name = dataset[:-4]

    name_train = name + '_train.txt'
    name_test = name + '_test.txt'

    return name_train, name_test


def get_fusion_name(file):
    new_file = file[:-4] + '_fusion.txt'
    return new_file


def path_per_labels(dataset):
    name = dataset[:-4]
    name_train = name + '_train.txt'

    with open(name_train, 'r') as f:
        dic = {}
        lines = f.readlines()
        for l in lines:
            label = l.split()[1]
            try:
                dic[label].append(l)
            except:
                dic[label] = [l]
        return dic


for dataset in datasets:
    path_train, path_test = get_dataset_name(dataset)

    paths = [path_train, path_test]

    c = 0
    with open(dataset, 'r') as f:
        with open(path_train, 'w') as f_train:
                with open(path_test, 'w') as f_test:
                    for line in f:
                        if c % 2 == 0:
                            f_train.write(line[:-1] + '\n')
                        else:
                            f_test.write(line)
                        c += 1

    dic = path_per_labels(dataset)

    lines_1 = []
    lines_3 = []

    for label in list(dic.keys()):
        lines = dic[label]
        line_1 = list(np.random.choice(lines, 1, replace=False))
        line_3 = list(np.random.choice(lines, 3, replace=False))

        lines_1 += line_1
        lines_3 += line_3

    print(len(lines_1), len(lines_3))

    for dataset in paths + [dataset]:
        with open(dataset, 'r') as f:
            with open(get_fusion_name(dataset), 'w') as new_f:
                for line in f:
                    new_f.write('/gpfs/workdir/bouvierv/data/domain_net/' + line[19:])
