datasets = ['clipart_train.txt', 'clipart_test.txt',
            'infograph_train.txt', 'infograph_test.txt',
            'painting_train.txt', 'painting_test.txt',
            'quickdraw_train.txt', 'quickdraw_test.txt',
            'real_train.txt', 'real_test.txt',
            'sketch_train.txt', 'sketch_test.txt'
            ]



def get_local_name(file):
    new_file = file[:-4] + '_local.txt'
    return new_file

def get_fusion_name(file):
    new_file = file[:-4] + '_fusion.txt'
    return new_file

for dataset in datasets:
    with open(dataset, 'r') as f:
        with open(get_local_name(dataset), 'w') as new_f:
            for line in f:
                new_f.write('../data/domain_net/' + line)


datasets = ['clipart_train_local.txt', 'clipart_test_local.txt',
            'infograph_train_local.txt', 'infograph_test_local.txt',
            'painting_train_local.txt', 'painting_test_local.txt',
            'quickdraw_train_local.txt', 'quickdraw_test_local.txt',
            'real_train_local.txt', 'real_test_local.txt',
            'sketch_train_local.txt', 'sketch_test_local.txt'
            ]


for dataset in datasets:
    with open(dataset, 'r') as f:
        with open(get_fusion_name(dataset), 'w') as new_f:
            for line in f:
                new_f.write('/gpfs/workdir/bouvierv/data/domain_net/' + line[19:])