datasets = ['clipart', 'infograph', 'painting', 'quickdraw', 'real', 'sketch']

for target in datasets:
    path_sources_combined = 'source_combined_' + target + '_train_local.txt'

    print('Target is ', target)
    with open(path_sources_combined, 'w') as f_source:
        for source in datasets:
            if source != target:
                print('Write source is', source)
                path_source = source + '_train_local.txt'
                with open(path_source, 'r') as f:
                    lines = f.readlines()
                    for line in lines:
                        f_source.write(line)


def get_fusion_name(file):
    new_file = file[:-4] + '_fusion.txt'
    return new_file


for target in datasets:
    path_sources_combined = 'source_combined_' + target + '_train_local.txt'
    with open(path_sources_combined, 'r') as f:
        with open(get_fusion_name(path_sources_combined), 'w') as new_f:
            for line in f:
                new_f.write('/gpfs/workdir/bouvierv/data/domain_net/' + line[19:])



