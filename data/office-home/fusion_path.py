def get_filename(file):
  return file[:-4] + '_fusion.txt'

datasets = ['Art_local.txt', 'Clipart_local.txt', 'Real_World_local.txt', 'Product_local.txt']

for dataset in datasets:
  with open(dataset, 'r') as f:
    with open(get_filename(dataset), 'w') as new_f:
      for line in f:
        new_f.write('/gpfs/workdir/bouvierv' + line[2:])