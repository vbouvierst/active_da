import numpy as np

SEED = 42

np.random.seed(SEED)

datasets = ['Art_local.txt', 'Product_local.txt', 'Clipart_local.txt', 'Real_World_local.txt']


def get_label_from_line(line):
    return line.split()[0], eval(line.split()[-1])


def get_dataset_name(dataset):
    name = dataset[:-4]

    name_train = name + '_train.txt'
    name_test = name + '_test.txt'

    return name_train, name_test


def get_fusion_name(file):
    new_file = file[:-4] + '_fusion.txt'
    return new_file

for dataset in datasets:
    path_train, path_test = get_dataset_name(dataset)

    paths = [path_train, path_test]

    with open(dataset, 'r') as f:
        with open(path_train, 'w') as f_train:
                with open(path_test, 'w') as f_test:
                    for line in f:
                        if np.random.rand() > 0.5:
                            f_train.write(line[:-1] + '\n')
                        else:
                            f_test.write(line)
                        #f_train.write(line[:-1] + '\n')
                        #f_test.write(line)

    for dataset in paths:
        with open(dataset, 'r') as f:
            with open(get_fusion_name(dataset), 'w') as new_f:
                for line in f:
                    new_f.write('/gpfs/workdir/bouvierv' + line[2:])