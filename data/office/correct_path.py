paths = ['amazon_10_list.txt', 'webcam_10_list.txt', 'dslr_10_list.txt']

for path in paths:

    with open(path, 'r') as f:
        lines = f.readlines()

    with open(path[:-4] + '_local.txt', 'w') as f:
        for line in lines:
            print(line)
            f.write('../data/' + line[34:])